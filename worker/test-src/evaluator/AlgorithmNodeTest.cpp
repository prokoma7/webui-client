#include "testing.hpp"
#include <environment/Environment.h>
#include <list>
#include <string>
#include "graph/nodes/AbstractNode.hpp"
#include "graph/nodes/AlgorithmNode.hpp"
#include "graph/nodes/BoolNode.hpp"
#include "graph/nodes/IntNode.hpp"
#include "utils/alt.hpp"

class AlgorithmTestNodeMock : public AbstractNode {
public:
    AlgorithmTestNodeMock()
        : AbstractNode("", ext::vector<std::string>())
    {
    }

    explicit AlgorithmTestNodeMock(const std::string& value)
        : AbstractNode("", ext::vector<std::string>())
    {
        this->setParam(std::make_shared<abstraction::ValueHolder<std::string>>(std::string(value), true), 0);
    }

    void evaluate(abstraction::TemporariesHolder& environment) override
    {
        passResultToChilds(params[0]);
    }

    bool isOutput() const override
    {
        return false;
    }

    bool isInput() const override
    {
        return false;
    }
};

TEST_CASE("AlgorithmNode evaluate")
{
    std::string automaton = "NFA 0 1\n>0 0|1 1\n<1 - -\n";
    auto node = std::make_shared<AlgorithmNode>("1", "string::Parse", ext::vector<std::string> {"automaton::Automaton"});
    auto mockInputNode = std::make_shared<AlgorithmTestNodeMock>(automaton);
    auto mockOutputNode = std::make_shared<AlgorithmTestNodeMock>();

    mockInputNode->createEdge(node, 0);
    node->createEdge(mockOutputNode, 0);

    cli::Environment environment;
    mockInputNode->evaluate(environment);
    node->evaluate(environment);


    REQUIRE(abstractValueToString(mockOutputNode->getParams().find(0)->second) == "(NFA states = {0, 1} inputAlphabet = {0, 1} initialState = 0 finalStates = {1} transitions = {((0, 0), 0), ((0, 0), 1), ((0, 1), 1)})\n");
}

TEST_CASE("AlgorithmNode multiple params evaluate")
{
    auto inputInt = std::make_shared<IntNode>("", 4, ext::vector<std::string>());
    auto inputBool = std::make_shared<BoolNode>("", false, ext::vector<std::string>());
    auto node = std::make_shared<AlgorithmNode>("", "string::generate::RandomStringFactory", ext::vector<std::string>());
    auto mockOutputNode = std::make_shared<AlgorithmTestNodeMock>();

    inputInt->createEdge(node, 0);
    inputInt->createEdge(node, 1);
    inputBool->createEdge(node, 2);
    inputBool->createEdge(node, 3);

    cli::Environment environment;
    inputInt->evaluate(environment);
    inputBool->evaluate(environment);
    node->evaluate(environment);
    mockOutputNode->evaluate(environment);

    REQUIRE(mockOutputNode->getParams().size() == 1);
}
