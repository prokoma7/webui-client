#include <abstraction/ValueHolder.hpp>
#include <ext/sstream>
#include <registry/Registry.h>
#include "alt.hpp"

std::string abstractValueToString(const std::shared_ptr<abstraction::Value>& val)
{
    std::shared_ptr<abstraction::OperationAbstraction> op = abstraction::Registry::getValuePrinterAbstraction(val->getType());

    ext::ostringstream oss;
    op->attachInput(val, 0);
    op->attachInput(std::make_shared<abstraction::ValueHolder<ext::ostream&>>(oss, false), 1);
    if (!op->eval())
        throw std::logic_error("Could not convert value to string");
    return oss.str();
}
