#pragma once
#include <memory>
#include <string>

namespace abstraction {
class Value;
}

std::string abstractValueToString(const std::shared_ptr<abstraction::Value>& val);
