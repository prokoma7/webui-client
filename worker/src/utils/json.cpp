#include <json/json.h>

Json::Value parseJson(const std::string& jsonText)
{
    Json::Value json;
    std::istringstream iss(jsonText);
    iss >> json;
    return json;
}

void writeJson(const Json::Value& json, std::ostream& os)
{
    Json::StreamWriterBuilder streamWriterBuilder;
    streamWriterBuilder["commentStyle"] = "None";
    streamWriterBuilder["indentation"] = "";

    const std::unique_ptr<Json::StreamWriter> writer(streamWriterBuilder.newStreamWriter());
    writer->write(json, &os);
}

std::string writeJson(const Json::Value& json)
{
    std::ostringstream oss;
    writeJson(json, oss);
    return oss.str();
}
