#include "log.hpp"

/** @short Extract log level from a CLI option */
spdlog::level::level_enum parseLogLevel(int option)
{
    static_assert(spdlog::level::trace < spdlog::level::off, "spdlog::level levels have changed");
    static_assert(spdlog::level::off == 6, "spdlog::level levels have changed");
    if (option < 0 || option > 5)
        throw std::runtime_error("Log level invalid or out-of-range");

    return static_cast<spdlog::level::level_enum>(5 - option);
}
