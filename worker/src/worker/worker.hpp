#pragma once
#include <chrono>
#include <cms/Connection.h>
#include <filesystem>
#include <json/value.h>
#include "utils/log.hpp"

using WorkerCallback = std::function<std::string(long, std::string, std::string)>;

/**
 * Class representing an individual worker
 */
class Worker {
public:
    /**
     * Abstract queue task processor.
     *
     * @param brokerUrl url of the broker
     * @param requestQueueName name of the queue producing jobs for us
     * @param responseQueueName name of the queue to put responses to
     */
    Worker(std::string brokerURL, const std::string& requestQueueName, const std::string& responseQueueName, WorkerCallback workerCb);

    ~Worker();

    /** @brief starts synchronously receiving messages */
    void listen();

private:
    static const int MESSAGE_RECEIVE_TIMEOUT_MS = 1000;

    std::string m_brokerURL;
    std::unique_ptr<cms::Connection> m_connection;
    std::unique_ptr<cms::Session> m_requestSession, m_responseSession;
    std::unique_ptr<cms::Queue> m_requestQueue, m_responseQueue;
    std::unique_ptr<cms::MessageConsumer> m_requestConsumer;
    std::unique_ptr<cms::MessageProducer> m_responseProducer;
    WorkerCallback m_workerCb;
};
