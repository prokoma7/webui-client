#include <chrono>
#include <csignal>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <sys/wait.h>
#include <unistd.h>
#include "runner.hpp"
#include "utils/log.hpp"

#define PIPE_RD 0
#define PIPE_WR 1

#define FD_STDOUT 1
#define FD_STDERR 2

using namespace std::chrono_literals;

/* Communication between signal handler and the rest of the program */
int g_Wakeup[2]; // pipe for wakeup
int g_RecvSignal; // signalled flag

static int waitSignalTimeout(const std::chrono::microseconds& duration)
{
    struct timeval tv;
    fd_set rd;

    auto duration_secs = std::chrono::duration_cast<std::chrono::seconds>(duration);
    auto duration_usecs = std::chrono::duration_cast<std::chrono::microseconds>(duration - duration_secs);
    tv.tv_sec = duration_secs.count();
    tv.tv_usec = duration_usecs.count();
    FD_ZERO(&rd);
    FD_SET(g_Wakeup[PIPE_RD], &rd);

    select(g_Wakeup[PIPE_RD] + 1, &rd, nullptr, nullptr, &tv);
    return g_RecvSignal;
}

static std::string readFromFD(int fd)
{
    static const size_t BUFSIZE = 64;

    std::string res;
    int rd;
    char buf[BUFSIZE];

    while ((rd = read(fd, &buf, BUFSIZE - 1)) > 0) {
        res.append(buf, rd);
    }

    return res;
}

static void writeToFD(int fd, const std::string& message, const std::string& errorDesc)
{
    if (write(fd, message.c_str(), message.length()) != (ssize_t)message.length())
        throw std::runtime_error("TimeoutAqlTest: write() to pipe failed (" + errorDesc + ")");
}

static void newSigChild(int)
{
    g_RecvSignal = 1;

    // write into the pipe so select can read something, this effectively means that SIGCHILD was raised
    writeToFD(g_Wakeup[PIPE_WR], " ", "wakeup signalling");
}

std::tuple<ResultType, std::string> _evaluate(const std::chrono::microseconds& timeout, const std::string& text, const std::filesystem::path& evaluatorPath)
{
    /* Register SIGCHLD handler */
    struct sigaction act;
    memset(&act, 0, sizeof(act));
    act.sa_handler = newSigChild;
    sigaction(SIGCHLD, &act, nullptr);

    int pipeToChild[2];
    int pipeFromChild[2];

    if (pipe(pipeToChild) != 0 || pipe(pipeFromChild) != 0) {
        throw std::runtime_error("Runner: Failed to initialize communication pipes");
    }

    /* SIGCHLD was not yet raised, initialize communication pipe */
    g_RecvSignal = 0;
    if (pipe(g_Wakeup)) {
        throw std::runtime_error("Runner: Failed to initialize wakeup signalling pipe");
    }

    /* do the forking */
    pid_t pid = fork();
    if (pid < 0) {
        throw std::runtime_error("Runner: Failed to fork");
    }

    if (pid == 0) { /* child, run the evaluator here */
        act.sa_handler = SIG_DFL;
        sigaction(SIGCHLD, &act, nullptr);

        close(g_Wakeup[PIPE_RD]);
        close(g_Wakeup[PIPE_WR]);

        if (dup2(pipeFromChild[PIPE_WR], STDOUT_FILENO) < 0 || dup2(pipeToChild[PIPE_RD], STDIN_FILENO) < 0) {
            exit(1);
        }

        close(pipeFromChild[PIPE_WR]);
        close(pipeFromChild[PIPE_RD]);
        close(pipeToChild[PIPE_RD]);
        close(pipeToChild[PIPE_WR]);

        execl(std::string(evaluatorPath).c_str(), std::string(evaluatorPath).c_str(), nullptr);
        exit(1);
    }
    close(pipeFromChild[PIPE_WR]);
    close(pipeToChild[PIPE_RD]);

    writeToFD(pipeToChild[PIPE_WR], text, "failed writing to child's stdin");
    close(pipeToChild[PIPE_WR]);

    /* lets wait the specified time of microseconds, maybe the child will terminate on its own */
    spdlog::debug("Waiting for child. Timeout: {} us", timeout.count());
    if (!waitSignalTimeout(timeout)) {
        kill(pid, SIGTERM);
        while (!waitSignalTimeout(250ms)) /* ... and in case it did not ..., SIGKILL it every 1/4 second */
            kill(pid, SIGKILL);
    }

    /* child termination confirmed */
    int status;
    waitpid(pid, &status, 0);
    close(g_Wakeup[PIPE_RD]);
    close(g_Wakeup[PIPE_WR]);

    /* read child outputs and communication is done */
    std::string childOutput = readFromFD(pipeFromChild[PIPE_RD]);
    close(pipeFromChild[PIPE_RD]);

    if (WIFSIGNALED(status) && (WTERMSIG(status) == SIGTERM || WTERMSIG(status) == SIGKILL)) {
        spdlog::error("Child process signalled with {}: {} (timeout)", WTERMSIG(status), strsignal(WTERMSIG(status)));
        return std::make_tuple(ResultType::TIMEOUT, childOutput);
    } else if (WIFSIGNALED(status) && WTERMSIG(status) != SIGTERM && WTERMSIG(status) != SIGKILL) {
        spdlog::error("Child process signalled with {}: {}", WTERMSIG(status), strsignal(WTERMSIG(status)));
        return std::make_tuple(ResultType::SIGNAL, childOutput);
    } else if (WIFEXITED(status)) {
        auto code = WEXITSTATUS(status);

        if (code == EXIT_SUCCESS) {
            spdlog::debug("Child process finished, code {}", WEXITSTATUS(status));
            return std::make_tuple(ResultType::EXIT_OK, childOutput);
        } else {
            spdlog::error("Child process finished, code {}", WEXITSTATUS(status));
            return std::make_tuple(ResultType::EXIT_FAIL, childOutput);
        }
    }

    __builtin_unreachable();
}
