#include <iostream>
#include <json/json.h>
#include <json/writer.h>
#include <registry/Registry.h>
#include <version.hpp>
#include "introspect.hpp"
#include "utils/json.hpp"

Json::Value getAlgorithmOverloadJSON(const ext::tuple<abstraction::AlgorithmFullInfo, std::optional<std::string>>& overload)
{
    Json::Value overloadJSON(Json::objectValue);
    Json::Value paramsJSON(Json::arrayValue);

    const auto& info = std::get<0>(overload);
    const auto& params = info.getParams();
    const auto& paramsNames = info.getParamNames();

    auto paramsIterator = params.cbegin();
    auto paramsNamesIterator = paramsNames.cbegin();

    while (paramsIterator != params.cend() || paramsNamesIterator != paramsNames.cend()) {
        Json::Value paramJSON(Json::objectValue);
        ;

        paramJSON["name"] = *paramsNamesIterator;
        paramJSON["type"] = std::get<0>(*paramsIterator);

        paramsJSON.append(paramJSON);

        ++paramsIterator;
        ++paramsNamesIterator;
    }

    overloadJSON["resultType"] = info.getResult().first;
    overloadJSON["params"] = paramsJSON;
    if (auto docs = std::get<1>(overload)) {
        overloadJSON["docs"] = *docs;
    }

    return overloadJSON;
}

Json::Value getAlgorithmOverloadsJSON(const ext::pair<std::string, ext::vector<std::string>>& algorithm)
{
    Json::Value overloadsJSON(Json::arrayValue);

    for (const auto& overload : abstraction::Registry::listOverloads(algorithm.first, algorithm.second))
        overloadsJSON.append(getAlgorithmOverloadJSON(overload));

    return overloadsJSON;
}


Json::Value getTypeParamsJSON(const ext::vector<std::string>& typeParams)
{
    Json::Value typeParamsJSON(Json::arrayValue);

    for (const auto& typeParam : typeParams)
        typeParamsJSON.append(typeParam);

    return typeParamsJSON;
}

Json::Value getAlgorithmJSON(const ext::pair<std::string, ext::vector<std::string>>& algorithm)
{
    Json::Value algorithmJSON(Json::objectValue);

    algorithmJSON["name"] = algorithm.first;
    algorithmJSON["templateParams"] = getTypeParamsJSON(algorithm.second);
    algorithmJSON["overloads"] = getAlgorithmOverloadsJSON(algorithm);

    return algorithmJSON;
}

Json::Value getAlgorithmsJSON()
{
    Json::Value algorithmsJSON(Json::arrayValue);
    for (const auto& algorithm : abstraction::Registry::listAlgorithms())
        algorithmsJSON.append(getAlgorithmJSON(algorithm));

    return algorithmsJSON;
}

Json::Value getCastsJSON()
{
    Json::Value castsJSON(Json::objectValue);

    for (const ext::tuple<std::string, std::string, bool>& castPair : abstraction::Registry::listCasts()) {
        const auto& isExplicit = std::get<2>(castPair);
        if (isExplicit)
            continue;

        const auto& from = std::get<1>(castPair);
        const auto& to = std::get<0>(castPair);

        if (castsJSON[from].isNull())
            castsJSON[from] = Json::Value(Json::arrayValue);

        castsJSON[from].append(to);
    }

    return castsJSON;
}

std::string getVersion()
{
    return std::string(ALIB_VERSION);
}

std::string introspect()
{
    Json::Value root(Json::objectValue);

    root["algorithms"] = getAlgorithmsJSON();
    root["casts"] = getCastsJSON();
    root["version"] = getVersion();

    return writeJson(root);
}
