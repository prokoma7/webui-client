#include <activemq/library/ActiveMQCPP.h>
#include <argparse/argparse.hpp>
#include <chrono>
#include <filesystem>
#include <iostream>
#include <json/json.h>
#include <json/value.h>
#include <thread>
#include "introspection/introspect.hpp"
#include "runner.hpp"
#include "utils/json.hpp"
#include "utils/log-sinks.hpp"
#include "utils/log.hpp"
#include "worker.hpp"

void validateEvaluator(const std::filesystem::path& pathToEvaluator)
{
    if (!std::filesystem::exists(pathToEvaluator))
        throw std::runtime_error("Path to evaluator is not a valid path");
}

int main(int argc, char* argv[])
{
    argparse::ArgumentParser program("alt-webui-worker");

    program.add_argument("brokerURL")
        .help("URL of the broker to connect to.");

    program.add_argument("evaluator")
        .help("Path to evaluator");

    program.add_argument("timeout")
        .help("Timeout (in milliseconds) of a single job.")
        .action([](const std::string& val) { return std::stoul(val); });

    program.add_argument("-v", "--log-level")
        .help("Log level. Values: 0 (off), 1 (error), ..., 5 (trace).")
        .default_value(3)
        .action([](const std::string& val) { return std::stoi(val); });

    program.add_argument("--log-stderr")
        .help("Log warnings and errors to stderr instead of stdout.")
        .default_value(false)
        .implicit_value(true);

    int returnCode = EXIT_SUCCESS;
    std::thread thrAlgorithmListWorker;
    activemq::library::ActiveMQCPP::initializeLibrary();

    try {
        program.parse_args(argc, argv);

        if (program.get<bool>("--log-stderr")) {
            auto sink = std::make_shared<loglevel_dist_sink_st>();
            sink->add_sink(std::make_shared<spdlog::sinks::stdout_sink_st>(), spdlog::level::trace, spdlog::level::info);
            sink->add_sink(std::make_shared<spdlog::sinks::stderr_sink_st>(), spdlog::level::warn, spdlog::level::n_levels);

            // set default logger's sink
            spdlog::default_logger()->sinks().clear();
            spdlog::default_logger()->sinks().push_back(sink);
        }

        std::string brokerURL = program.get<std::string>("brokerURL");
        std::filesystem::path evaluatorPath = program.get<std::string>("evaluator");
        auto timeout = program.get<long unsigned int>("timeout");

        spdlog::set_level(parseLogLevel(program.get<int>("--log-level")));
        validateEvaluator(evaluatorPath);

        spdlog::info("Connecting to broker {}", brokerURL);
        spdlog::info("Timeout set to {} milliseconds", timeout);
        spdlog::info("Evaluator is: {}", std::string(evaluatorPath));

        auto evaluateGraphWorker = std::make_unique<Worker>(brokerURL, "EvaluateGraph", "response", [&timeout, &evaluatorPath](const long& id, const std::string& type, const std::string& message) {
            const auto [resultType, resultJsonText] = evaluate(std::chrono::milliseconds(timeout), message, evaluatorPath);

            Json::Value reply;
            if (resultType == ResultType::TIMEOUT) {
                reply = Json::objectValue;
                reply["error"][0] = "Time limit exceeded.";

                spdlog::warn("{}: Evaluator timeouted", id);
                spdlog::warn("{}: {}", id, message);
            } else if (resultType == ResultType::SIGNAL) {
                reply = Json::objectValue;
                reply["error"][0] = "Internal error. Something went bad on our side.";

                spdlog::error("{}: Evaluator was terminated with a non-timeout signal", id);
                spdlog::error("{}: {}", id, message);
            } else if (resultType == ResultType::EXIT_OK) {
                reply = parseJson(resultJsonText);
            } else if (resultType == ResultType::EXIT_FAIL) {
                reply = Json::objectValue;
                reply["error"][0] = "Internal error. Something went bad on our side.";

                spdlog::error("{}: Evaluator did not exited successfully", id);
                spdlog::error("{}: {}", id, message);
            }

            return writeJson(reply);
        });

        auto introspectCache = introspect();
        auto algorithmListWorker = std::make_unique<Worker>(brokerURL, "WorkerDefinitions", "response", [&introspectCache](auto, auto, auto) {
            return introspectCache;
        });

        thrAlgorithmListWorker = std::thread([&]() { algorithmListWorker->listen(); });
        evaluateGraphWorker->listen();
        thrAlgorithmListWorker.join();
    } catch (std::exception& e) {
        spdlog::error("{}", e.what());
        returnCode = EXIT_FAILURE;
    }

    spdlog::info("Shutting down");
    activemq::library::ActiveMQCPP::shutdownLibrary();

    return returnCode;
}
