#pragma once
#include <chrono>
#include <filesystem>
#include <string>

enum class ResultType {
    TIMEOUT,
    SIGNAL,
    EXIT_OK,
    EXIT_FAIL,
};

std::tuple<ResultType, std::string> _evaluate(const std::chrono::microseconds& timeout, const std::string& text, const std::filesystem::path& evaluatorPath);

template <class IntervalType>
std::tuple<ResultType, std::string> evaluate(const IntervalType& timeout, const std::string& text, const std::filesystem::path& evaluatorPath)
{
    return _evaluate(std::chrono::duration_cast<std::chrono::microseconds>(timeout), text, evaluatorPath);
}
