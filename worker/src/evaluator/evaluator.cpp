#include <ext/exception>
#include "evaluator.hpp"
#include "graph/AlgorithmGraph.hpp"
#include "graph/NodesFromJSONBuilder.hpp"
#include "utils/json.hpp"

namespace impl {

Json::Value replyError(const std::exception& exception)
{
    alib::ExceptionHandler::NestedExceptionContainer exceptions;
    alib::ExceptionHandler::handle(exceptions);
    Json::Value errorJson;
    for (size_t i = 0; i < exceptions.getExceptions().size(); i++) {
        errorJson["error"][static_cast<int>(i)] = exceptions.getExceptions()[i].desc.value();
    }

    return errorJson;
}


Json::Value replyOutputs(const std::map<std::string, std::string>& outputMap)
{
    Json::Value outputsJson(Json::objectValue);
    for (const auto& outputEntry : outputMap) {
        outputsJson[outputEntry.first] = outputEntry.second;
    }

    Json::Value responseJson;
    responseJson["outputs"] = outputsJson;

    return responseJson;
}

} /* namespace impl */

std::string evaluate(const std::string& jsonString)
{
    auto json = parseJson(jsonString);

    NodesFromJSONBuilder builder;
    auto graph = builder.withRoot(json).build();

    try {
        auto resultOutputMap = graph.evaluate();
        return writeJson(impl::replyOutputs(resultOutputMap));
    } catch (std::exception& exception) {
        return writeJson(impl::replyError(exception));
    }
}
