#include "graph/Edge.hpp"
#include "graph/nodes/AbstractNode.hpp"

std::shared_ptr<AbstractNode> Edge::getNode() const
{
    return node;
}

int Edge::getParamIndex() const
{
    return paramIndex;
}

bool Edge::operator<(const Edge& other) const
{
    return paramIndex == other.paramIndex
        ? node->getId() < other.getNode()->getId()
        : paramIndex < other.paramIndex;
}
