#include <environment/Environment.h>
#include "AlgorithmGraph.hpp"
#include "graph/nodes/AbstractNode.hpp"
#include "graph/topSort.hpp"

AlgorithmGraph::AlgorithmGraph(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes)
    : m_nodes(nodes)
{
}

std::map<std::string, std::shared_ptr<AbstractNode>> AlgorithmGraph::nodes() const
{
    return m_nodes;
}

std::map<std::string, std::string> AlgorithmGraph::evaluate()
{
    auto sortedNodes = topSort(m_nodes);

    std::map<std::string, std::string> result;
    cli::Environment environment;

    while (!sortedNodes.empty()) {
        auto node = std::move(sortedNodes.front());
        sortedNodes.pop_front();

        node->evaluate(environment);
        if (auto nodeRes = node->getResult()) {
            result[node->getId()] = std::move(*nodeRes);
        }
    }

    return result;
}
