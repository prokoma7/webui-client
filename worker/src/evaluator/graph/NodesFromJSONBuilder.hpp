#pragma once
#include <json/json.h>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <utility>
#include "graph/nodes/AbstractNode.hpp"

class AlgorithmGraph;

class NodesFromJSONBuilderException : public std::logic_error {
public:
    explicit NodesFromJSONBuilderException(const std::string& msg);
};

/**
 * Builder class which converts evaluation request JSON to a node graph
 */
class NodesFromJSONBuilder {
public:
    /**
     * Converts the whole request JSON to a node graph
     * @param root root object of the request JSON
     * @return reference to this
     */
    NodesFromJSONBuilder& withRoot(const Json::Value& root);

    NodesFromJSONBuilder& withInts(const Json::Value& intNodesJson);

    NodesFromJSONBuilder& withDoubles(const Json::Value& doubleNodesJson);

    NodesFromJSONBuilder& withBools(const Json::Value& boolNodesJson);

    NodesFromJSONBuilder& withStrings(const Json::Value& stringNodesJson);

    NodesFromJSONBuilder& withAlgorithmNodes(const Json::Value& algorithmNodesJson);

    NodesFromJSONBuilder& withOutputs(const Json::Value& outputsJson);

    NodesFromJSONBuilder& withEdges(const Json::Value& edgesJson);

    /** @brief Retrieves the built graph */
    AlgorithmGraph build();

private:
    std::map<std::string, std::shared_ptr<AbstractNode>> nodes;

    template <class NodeType>
    void createNodes(const Json::Value& nodesJson);

    template <class OutputNodeType>
    void createOutputNodes(const Json::Value& nodesJson);

    void linkNodes(const Json::Value& edgesJson);
};
