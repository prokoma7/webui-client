#pragma once
#include <map>
#include <memory>
#include <string>

class AbstractNode;

class AlgorithmGraph {
    std::map<std::string, std::shared_ptr<AbstractNode>> m_nodes;

public:
    explicit AlgorithmGraph(const std::map<std::string, std::shared_ptr<AbstractNode>>& nodes);
    std::map<std::string, std::shared_ptr<AbstractNode>> nodes() const;
    std::map<std::string, std::string> evaluate();
};
