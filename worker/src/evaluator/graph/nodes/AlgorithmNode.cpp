#include <common/AlgorithmCategories.hpp>
#include <common/EvalHelper.h>
#include <ext/functional>
#include <ext/ostream>
#include <global/GlobalData.h>
#include "AlgorithmNode.hpp"

AlgorithmNode::AlgorithmNode(const std::string& id, std::string algorithmName, const ext::vector<std::string>& templateParams)
    : AbstractNode(id, templateParams)
    , algorithmName(std::move(algorithmName))
{
}

AlgorithmNode::AlgorithmNode(const std::string& id, const Json::Value& jsonObject)
    : AbstractNode(id, jsonObject)
    , algorithmName(jsonObject["name"].asString())
{
}

bool AlgorithmNode::isOutput() const
{
    return false;
}

bool AlgorithmNode::isInput() const
{
    return false;
}

void AlgorithmNode::evaluate(abstraction::TemporariesHolder& environment)
{
    common::GlobalData::verbose = true;
    ext::ostringstream logStream;
    common::Streams::log = ext::reference_wrapper<ext::ostream>(logStream);

    auto result = abstraction::EvalHelper::evalAlgorithm(environment, algorithmName, templateParams, getParamsVec(), abstraction::AlgorithmCategories::AlgorithmCategory::NONE);
    passResultToChilds(result);

    if (auto log = logStream.str(); !log.empty()) {
        m_log = log;
    }
}

ext::vector<std::shared_ptr<abstraction::Value>> AlgorithmNode::getParamsVec()
{
    ext::vector<std::shared_ptr<abstraction::Value>> paramsVec;
    paramsVec.reserve(params.size());

    int prevParamIndex = -1;
    for (const auto& param : params) {
        if (prevParamIndex != param.first - 1)
            throw NodeException(std::string("Algorithm node ") + id + " is missing parameter");

        prevParamIndex = param.first;
        paramsVec.push_back(param.second);
    }

    return paramsVec;
}

std::optional<std::string> AlgorithmNode::getResult()
{
    return m_log;
}
