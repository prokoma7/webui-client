#include "graph/Edge.hpp"
#include "graph/nodes/AbstractNode.hpp"

AbstractNode::AbstractNode(const std::string& id, const ext::vector<std::string>& templateParams)
    : id(id)
    , inDegree(0)
    , templateParams(templateParams)
{
}

AbstractNode::AbstractNode(const std::string& id, const Json::Value& json)
    : AbstractNode(id, ext::vector<std::string>())
{
    const Json::Value& templateParamsJSON = json["templateParams"];

    if (!templateParamsJSON.isArray() && !templateParamsJSON.isNull())
        throw std::invalid_argument("templateParams in JSON are not of JSON type array or null");

    for (const Json::Value& templateParam : templateParamsJSON) {
        if (!templateParam.isString())
            throw std::invalid_argument("templateParam is not of type string");

        this->templateParams.push_back(templateParam.asString());
    }
}

bool AbstractNode::removeEdge(const Edge& edge)
{
    return edges.erase(edge);
}

const std::string& AbstractNode::getId() const
{
    return id;
}

int AbstractNode::getInDegree() const
{
    return inDegree;
}

const std::set<Edge>& AbstractNode::getEdges() const
{
    return edges;
}

const std::map<int, std::shared_ptr<abstraction::Value>>& AbstractNode::getParams() const
{
    return params;
}

void AbstractNode::createEdge(const std::shared_ptr<AbstractNode>& to, int paramIndex)
{
    edges.insert(Edge(to, paramIndex));
    ++(to->inDegree);
}

void AbstractNode::passResultToChilds(const std::shared_ptr<abstraction::Value>& result)
{
    for (const auto& edge : edges)
        edge.getNode()->setParam(result, edge.getParamIndex());
}
