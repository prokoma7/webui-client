#pragma once
#include <abstraction/TemporariesHolder.h>
#include <ext/vector>
#include <string>
#include "graph/nodes/AbstractNode.hpp"
#include "utils/alt.hpp"

/**
 * class representing an output
 */
class OutputNode : public AbstractNode {
public:
    explicit OutputNode(const std::string& id, const ext::vector<std::string>& templateParams = {});
    explicit OutputNode(const std::string& id, const Json::Value& jsonObject);

    bool isOutput() const final;
    bool isInput() const final;

    /**
     * @return Output in the form of a string
     */
    std::optional<std::string> getResult() override;

    /**
     * Retrieves the parameter on index 0 and converts it to a string value
     */
    void evaluate(abstraction::TemporariesHolder& environment) override;

protected:
    std::string result;
};
