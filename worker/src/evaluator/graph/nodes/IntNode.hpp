#pragma once
#include <ext/vector>
#include <json/json.h>
#include <string>
#include "graph/nodes/InputNode.hpp"

/**
 * InputNode representing int value
 */
class IntNode : public InputNode<int> {
public:
    IntNode(const std::string& id, int value, const ext::vector<std::string>& templateParams);
    IntNode(const std::string& id, const Json::Value& json);
};
