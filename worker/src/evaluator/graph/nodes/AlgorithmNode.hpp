#pragma once
#include <abstraction/TemporariesHolder.h>
#include <json/value.h>
#include <string>
#include "graph/nodes/AbstractNode.hpp"

class AlgorithmNode : public AbstractNode {
public:
    AlgorithmNode(const std::string& id, std::string algorithmName, const ext::vector<std::string>& templateParams);
    AlgorithmNode(const std::string& id, const Json::Value& jsonObject);

    bool isOutput() const override;
    bool isInput() const override;

    /**
     * Evaluates the alib algorithm of given name with given parameters and template parameters
     * @param environment Environment to evaluate the algorithm in
     */
    void evaluate(abstraction::TemporariesHolder& environment) override;

    /**
     * @brief Returns result of an algorithm name, which is, verbose output (contents of common::Streams::log)
     */
    std::optional<std::string> getResult() override;

private:
    const std::string algorithmName;

    /**
     * Converts the parameter map to a vector used to pass the parameters to alib
     * @return vector of parameters
     */
    ext::vector<std::shared_ptr<abstraction::Value>> getParamsVec();

    std::optional<std::string> m_log;
};
