#pragma once
#include <abstraction/ValueHolder.hpp>
#include <json/json.h>
#include "AbstractNode.hpp"


template <typename T>
class InputNode : public AbstractNode {
public:
    InputNode(const std::string& id, const T& value, const ext::vector<std::string>& templateParams)
        : AbstractNode(id, templateParams)
    {
        setParam(std::make_shared<abstraction::ValueHolder<T>>(T(value), true), 0);
    }

    bool isOutput() const final
    {
        return false;
    }

    bool isInput() const final
    {
        return true;
    }

    void evaluate(abstraction::TemporariesHolder& environment) override
    {
        passResultToChilds(params[0]);
    }
};
