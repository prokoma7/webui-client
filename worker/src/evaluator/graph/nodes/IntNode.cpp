#include <string>
#include "IntNode.hpp"

IntNode::IntNode(const std::string& id, int value, const ext::vector<std::string>& templateParams)
    : InputNode(id, value, templateParams)
{
}

IntNode::IntNode(const std::string& id, const Json::Value& json)
    : InputNode(id, json["value"].asInt(), {})
{
}
