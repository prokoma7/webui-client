#pragma once
#include <ext/vector>
#include <json/json.h>
#include <string>
#include "graph/nodes/InputNode.hpp"

/**
 * InputNode representing double value
 */
class DoubleNode : public InputNode<double> {
public:
    DoubleNode(const std::string& id, double value, const ext::vector<std::string>& templateParams);
    DoubleNode(const std::string& id, const Json::Value& json);
};
