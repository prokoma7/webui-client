#include "OutputNode.hpp"

OutputNode::OutputNode(const std::string& id, const ext::vector<std::string>& templateParams)
    : AbstractNode(id, templateParams)
{
}

OutputNode::OutputNode(const std::string& id, const Json::Value& jsonObject)
    : AbstractNode(id, jsonObject)
{
}

bool OutputNode::isOutput() const
{
    return true;
}

bool OutputNode::isInput() const
{
    return false;
}

std::optional<std::string> OutputNode::getResult()
{
    return result;
}

void OutputNode::evaluate(abstraction::TemporariesHolder& environment)
{
    auto str = abstractValueToString(params[0]);

    // remove newline
    result = str.substr(0, str.length() > 0 ? str.length() - 1 : 0);
}
