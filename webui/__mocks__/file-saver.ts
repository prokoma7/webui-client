const fileSaver = jest.genMockFromModule<{ saveAs: (data: string) => void }>(
	'file-saver'
)

fileSaver.saveAs = () => {}

module.exports = fileSaver
