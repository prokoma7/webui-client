const uuid = jest.genMockFromModule<{ v4: () => string }>('uuid')

let id = 1
function getid() {
	const curr = id++
	return curr.toString()
}

exports.v4 = getid
