import { createAction, ActionsUnion } from '../utils/redux'
import AlgorithmNode from '../interfaces/AlgorithmNode'
import AlgorithmEdge from '../interfaces/AlgorithmEdge'
import { AlgorithmDataActions } from './algorithmData'

export interface AlgorithmUIState {
	/** Open/close Statemaker dialog */
	statemakerOpen: boolean
	/** Show/hide Statemaker apply and cancel buttons */
	showStatemakerApplyButton: boolean
	/** Open/close string edit dialog */
	stringDialogOpen: boolean
	stringDialogHeader: string
	stringDialogContent: string
	/** Open/close Dot dialog */
	dotDialogOpen: boolean
	/** Dot content of the Dot dialog */
	dotDialogContent: string
	/** Open/close evaluating dialog */
	evaluatingDialogOpen: boolean
	ghostNode: AlgorithmNode
	ghostEdge: AlgorithmEdge
	toolbarOpen: boolean
	inputDialog: {
		open: boolean
		initialValue: any
		editedNodeId: string
	}
	showHint: boolean
}

export const initialAlgorithmUIState: AlgorithmUIState = {
	statemakerOpen: false,
	showStatemakerApplyButton: false,
	stringDialogContent: '',
	stringDialogHeader: '',
	stringDialogOpen: false,
	dotDialogOpen: false,
	dotDialogContent: '',
	evaluatingDialogOpen: false,
	ghostNode: {
		id: 'ghostNode',
		height: 0,
		width: 0,
		name: '',
		params: [],
		templateParams: [],
		position: { x: 0, y: 0 },
		resultType: '',
		nodeType: 'algorithm'
	},
	ghostEdge: {
		id: 'ghostEdge',
		startNodeId: '',
		endNodeId: '',
		endParamIndex: -1
	},
	toolbarOpen: true,
	inputDialog: {
		open: false,
		initialValue: '',
		editedNodeId: ''
	},
	showHint: true
}

export const algorithmUIActions = {
	/** Opens/closes statemaker */
	setStatemakerOpen: (value: boolean, showButtons: boolean = false) =>
		createAction('setStatemakerOpen', { value, showButtons }),
	/** Opens/closes string edit dialog */
	setStringDialogOpen: (value: boolean) =>
		createAction('setStringDialogOpen', value),

	/** Opens/closes Dot dialog */
	setDotDialogOpen: (value: boolean) =>
		createAction('setDotDialogOpen', value),
	/** Sets the content of the Dot dialog */
	setDotDialogContent: (value: string) =>
		createAction('setDotDialogContent', value),
	/** Opens/closes evaluating dialog */
	setEvaluatingDialogOpen: (value: boolean) =>
		createAction('setEvaluatingDialogOpen', value),

	updateGhostNode: (node: Partial<AlgorithmNode>) =>
		createAction('updateGhostNode', node),

	updateGhostEdge: (edge: Partial<AlgorithmEdge>) =>
		createAction('updateGhostEdge', edge),

	setAlgorithmToolbarOpen: (value: boolean) =>
		createAction('setAlgorithmToolbarOpen', value),

	openStringDialog: (header: string, content: string) =>
		createAction('openStringDialog', { header, content }),

	openInputDialog: (editedNodeId: string, initialValue: any = '') =>
		createAction('openInputDialog', { editedNodeId, initialValue }),

	closeInputDialog: () => createAction('closeInputDialog'),

	toggleHint: () => createAction('toggleHint')
}

export type AlgorithmUIActions = ActionsUnion<
	typeof algorithmUIActions
>

export const algorithmUIReducer = (
	state: AlgorithmUIState = initialAlgorithmUIState,
	action: AlgorithmUIActions | AlgorithmDataActions
) => {
	switch (action.type) {
		case 'setStatemakerOpen': {
			return {
				...state,
				statemakerOpen: action.payload.value,
				showStatemakerApplyButton: action.payload.showButtons
			}
		}
		case 'setStringDialogOpen': {
			return {
				...state,
				stringDialogOpen: action.payload
			}
		}
		case 'setDotDialogOpen': {
			return {
				...state,
				dotDialogOpen: action.payload
			}
		}
		case 'setDotDialogContent': {
			return {
				...state,
				dotDialogContent: action.payload
			}
		}
		case 'setEvaluatingDialogOpen': {
			return {
				...state,
				evaluatingDialogOpen: action.payload
			}
		}
		case 'updateGhostNode': {
			return {
				...state,
				ghostNode: {
					...state.ghostNode,
					...action.payload
				}
			}
		}
		case 'updateGhostEdge': {
			return {
				...state,
				ghostEdge: {
					...state.ghostEdge,
					...action.payload
				}
			}
		}
		case 'setAlgorithmToolbarOpen': {
			return {
				...state,
				toolbarOpen: action.payload
			}
		}
		case 'openStringDialog': {
			const { header, content } = action.payload
			return {
				...state,
				stringDialogOpen: true,
				stringDialogHeader: header,
				stringDialogContent: content,
			}
		}
		case 'openInputDialog': {
			const { editedNodeId, initialValue } = action.payload
			return {
				...state,
				inputDialog: {
					open: true,
					editedNodeId,
					initialValue
				}
			}
		}
		case 'closeInputDialog': {
			return {
				...state,
				inputDialog: {
					...state.inputDialog,
					open: false
				}
			}
		}
		case 'addNode': {
			return {
				...state,
				toolbarOpen: true
			}
		}
		case 'toggleHint': {
			return {
				...state,
				showHint: !state.showHint
			}
		}
		default:
			return state
	}
}
