import IState, { StateType } from '../interfaces/State'
import { ActionsUnion, createAction } from '../utils/redux'
import ITransition from '../interfaces/Transition'

type PositioningAlgorithm = 'force' | 'layer'
export type MouseMode = 'vertical' | 'horizontal' | 'default'

export const stateUIActions = {
	/** Sets input to text */
	setInput: (text: string) => createAction('setInput', text),
	/** Sets positioning alg type */
	setAlgorithm: (value: PositioningAlgorithm) =>
		createAction('setAlgorithm', value),
	/** Sets input to '' */
	resetInput: () => createAction('resetInput'),
	/** Add error to errors */
	addError: (error: string) => createAction('addError', error),
	/** Assign error array */
	setErrors: (errors: string[]) => createAction('setErrors', errors),
	/** Deletes errors to empty arr */
	resetErrors: () => createAction('resetErrors'),
	/** Toggle build state mode */
	toggleBuildState: (stateType?: StateType) =>
		createAction('toggleBuildState', stateType),
	/** Toggle build transition mode */
	toggleBuildTransition: () => createAction('toggleBuildTransition'),
	/** Sets build state mode to value */
	setBuildState: (value: boolean) => createAction('setBuildState', value),
	/** Sets build transition mode to value */
	setBuildTransition: (value: boolean) =>
		createAction('setBuildTransition', value),
	/** Toggle hint to show */
	toggleShowHint: () => createAction('toggleShowHint'),
	/** Toggle explore to show */
	setShowExplore: (value: boolean) => createAction('setShowExplore', value),
	/** Resets input and build modes */
	reset: () => createAction('reset'),
	/** Set info snackbar text */
	setConfirmText: (value: string) => createAction('setConfirmText', value),
	/** Sets vertical or horizontal only mouse move mode */
	setMouseMode: (value: MouseMode) => createAction('setMouseMode', value),
	/** Updates values of ghost state */
	updateGhostState: (state: Partial<IState>) =>
		createAction('updateGhostState', state),
	/** Updates values of ghost transition */
	updateGhostTransition: (transition: Partial<ITransition>) =>
		createAction('updateGhostTransition', transition),
	/** Increases state name id by 1 */
	incrementStateNameId: () => createAction('incrementStateNameId'),
	/** Increases transition name id by 1 */
	incrementTransitionNameId: () => createAction('incrementTransitionNameId'),
}

export type StateUIActions = ActionsUnion<typeof stateUIActions>

export interface State {
	/** Text input value */
	input: string
	/** Displayed errors */
	errors: string[]
	/** Build state mode toggle */
	buildState: boolean
	/** Build state mode state type */
	buildStateType?: StateType
	/** Build transition mode toggle */
	buildTransition: boolean
	/** Show/hide hint */
	showHint: boolean
	/** Show/hide explore */
	showExplore: boolean
	/** Chosen positioning alg */
	algorithm: PositioningAlgorithm
	/** Info snackbar text */
	confirmText: string
	/** Default/horizontal.vertical mouse mode */
	mouseMode: MouseMode
	/** Ghost state used when building state */
	ghostState: IState
	/** Ghost transition used when building transition */
	ghostTransition: ITransition
	/** Number of next auto state name number */
	stateNameId: number
	/** Number of next auto transition name number */
	transitionNameId: number
}

export const initialStateUIState: State = {
	input: '',
	errors: [],
	buildState: false,
	buildTransition: false,
	showHint: true,
	showExplore: false,
	algorithm: 'layer',
	confirmText: '',
	mouseMode: 'default',
	ghostState: {
		id: 'state',
		name: 'State',
		position: {
			x: 0,
			y: 0
		},
		size: 29
	},
	ghostTransition: {
		id: 'transition',
		name: 'Transition',
		startState: '',
		endState: ''
	},
	stateNameId: 1,
	transitionNameId: 1
}

function reducer(state = initialStateUIState, action: StateUIActions): State {
	switch (action.type) {
		case 'setInput': {
			return {
				...state,
				input: action.payload
			}
		}
		case 'setAlgorithm': {
			return {
				...state,
				algorithm: action.payload
			}
		}
		case 'resetInput': {
			return {
				...state,
				input: ''
			}
		}
		case 'addError': {
			return {
				...state,
				errors: [...state.errors, action.payload]
			}
		}
		case 'setErrors': {
			return {
				...state,
				errors: action.payload
			}
		}
		case 'resetErrors': {
			return {
				...state,
				errors: []
			}
		}
		case 'toggleBuildState': {
			return {
				...state,
				buildState: !state.buildState,
				buildStateType: action.payload
			}
		}
		case 'toggleBuildTransition': {
			return {
				...state,
				buildTransition: !state.buildTransition
			}
		}
		case 'setBuildState': {
			return {
				...state,
				buildState: action.payload
			}
		}
		case 'setBuildTransition': {
			return {
				...state,
				buildTransition: action.payload
			}
		}
		case 'toggleShowHint': {
			return {
				...state,
				showHint: !state.showHint
			}
		}
		case 'setShowExplore': {
			return {
				...state,
				showExplore: action.payload
			}
		}
		case 'reset': {
			return {
				...state,
				buildState: false,
				buildTransition: false,
				input: '',
				errors: []
			}
		}
		case 'setConfirmText': {
			return {
				...state,
				confirmText: action.payload
			}
		}
		case 'setMouseMode': {
			return {
				...state,
				mouseMode: action.payload
			}
		}
		case 'updateGhostState': {
			return {
				...state,
				ghostState: {
					...state.ghostState,
					...action.payload
				}
			}
		}
		case 'updateGhostTransition': {
			return {
				...state,
				ghostTransition: {
					...state.ghostTransition,
					...action.payload
				}
			}
		}
		case 'incrementStateNameId': {
			return {
				...state,
				stateNameId: state.stateNameId + 1
			}
		}
		case 'incrementTransitionNameId': {
			return {
				...state,
				transitionNameId: state.transitionNameId + 1
			}
		}
		default:
			return state
	}
}

export default reducer
