import * as uuid from 'uuid'
import { Map, Set } from 'immutable'
import { ActionsUnion, createAction } from '../utils/redux'
import AlgorithmNode, { emptyNode } from '../interfaces/AlgorithmNode'
import AlgorithmEdge from '../interfaces/AlgorithmEdge'
import StateData from '../interfaces/StateData'
import {
	automanAlibTypeMap,
	classifyAutomaton
} from '../interfaces/AutomatonTypes'
import { PartialProperties } from '../interfaces/types'
import { AlgorithmGraph } from '../interfaces/AlgorithmGraph'
import { detectCycles } from '../utils/detectCycle'
import Position from '../interfaces/Position'
import { Casts } from 'interfaces/Casts'
import { isCastable } from 'utils/alt/casts'
import { AlibType } from '../interfaces/Algorithms'

export const algorithmDataActions = {
	addNode: (data: PartialProperties<AlgorithmNode, 'id'>) =>
		createAction('addNode', data),

	addEdge: (data: Omit<AlgorithmEdge, 'id'>, castData: Casts) => createAction('addEdge', { data, castData } ),

	positionNode: (id: string, position: Position) =>
		createAction('positionNode', { id, position }),

	resizeNodeWidth: (id: string, width: number) =>
		createAction('resizeNodeWidth', { id, width }),

	setInputAutomaton: (id: string, automaton: StateData) =>
		createAction('setInputAutomaton', { id, automaton }),

	setInputString: (id: string, value: string) =>
		createAction('setInputString', { id, value }),

	setInputNumber: (id: string, value: number) =>
		createAction('setInputNumber', { id, value }),

	setInputBoolean: (id: string, value: boolean) =>
		createAction('setInputBoolean', { id, value }),

	setOutputValues: (values: Map<string, string>) =>
		createAction('setOutputValues', values),

	deleteNode: (id: string) => createAction('deleteNode', id),
	deleteEdge: (id: string) => createAction('deleteEdge', id),

	importGraph: (data: Omit<AlgorithmGraph, 'outputValues'>, castData: Casts) =>
		createAction('importGraph', { data, castData }),

	clear: () => createAction('clear')
}

export type AlgorithmDataActions = ActionsUnion<typeof algorithmDataActions>

export interface AlgorithmGraphState extends AlgorithmGraph {
	cycleEdges: Set<string>
	incompatibleEdges: Set<AlgorithmEdge>
	errors: string[]
}

export const initialAlgorithmState: AlgorithmGraphState = {
	nodes: Map(),
	edges: Map(),
	outputValues: Map<string, string>(),
	cycleEdges: Set(),
	incompatibleEdges: Set(),
	errors: []
}

const incompatibleTypeError = (from: AlibType, to: AlibType) =>
	`Type ${from} is not castable to type ${to}`

const localStorageKey = 'webui_graph'

export const localStorageAlgorithmReducer = (
	state?: AlgorithmGraphState,
	action?: any
) => {
	if (state === undefined) {
		const savedGraph = window.localStorage.getItem(localStorageKey)

		if (savedGraph === null) return algorithmReducer(state, action)

		const parsedGraph = JSON.parse(savedGraph)

		const loadedState: AlgorithmGraphState = {
			nodes: Map(parsedGraph.nodes),
			edges: Map(parsedGraph.edges),
			outputValues: Map(parsedGraph.outputValues),
			cycleEdges: Set(parsedGraph.cycleEdges),
			incompatibleEdges: Set(parsedGraph.incompatibleEdges),
			errors: parsedGraph.errors
		}

		return algorithmReducer(loadedState, action)
	}

	const newState = algorithmReducer(state, action)

	if (
		newState !== state ||
		['undoAlgorithm', 'redoAlgorithm'].includes(action.type)
	) {
		try {
			window.localStorage.setItem(
				localStorageKey,
				JSON.stringify(newState)
			)
		} catch (e) {
			console.error(e)
		}
	}

	return newState
}

export const algorithmReducer = (
	state = initialAlgorithmState,
	action: AlgorithmDataActions
): AlgorithmGraphState => {
	switch (action.type) {
		case 'addNode': {
			const id = action.payload.id ?? uuid.v4()
			return {
				...state,
				nodes: state.nodes.update(id, () => ({
					...action.payload,
					id
				})),
				outputValues: Map()
			}
		}
		case 'addEdge': {
			const { data, castData } = action.payload
			const id = uuid.v4()
			const newEdge: AlgorithmEdge = {
				...data,
				id
			}
			const newEdges = state.edges.set(id, newEdge)

			const errors = []
			const cycleEdges = detectCycles(newEdges.toSet())
			if (!cycleEdges.isEmpty()) errors.push('Cycle detected')

			const fromType = state.nodes.get(data.startNodeId)?.resultType
			const toType = state.nodes.get(data.endNodeId)?.params[data.endParamIndex].type
			let incompatibleEdges = state.incompatibleEdges

			if (fromType && toType && !isCastable(castData, fromType, toType)) {
				incompatibleEdges = incompatibleEdges.add(newEdge)
				errors.push(incompatibleTypeError(fromType, toType))
			}

			return {
				...state,
				edges: newEdges,
				cycleEdges,
				outputValues: Map(),
				errors,
				incompatibleEdges
			}
		}
		case 'positionNode': {
			const { id, position } = action.payload

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (oldNode: AlgorithmNode) => ({
					...oldNode,
					position
				}))
			}
		}
		case 'resizeNodeWidth': {
			const { id, width } = action.payload

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (oldNode: AlgorithmNode) => ({
					...oldNode,
					width
				}))
			}
		}
		case 'setInputAutomaton': {
			const { id, automaton } = action.payload
			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					resultType:
						automanAlibTypeMap[classifyAutomaton(automaton)],
					value: automaton
				})),
				outputValues: Map()
			}
		}
		case 'setInputString': {
			const { id, value } = action.payload
			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: 'string',
					resultType:
						'std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >',
					nodeType: 'inputString',
					value
				})),
				outputValues: Map()
			}
		}
		case 'setInputNumber': {
			const { id, value } = action.payload

			const isInt = Number.isInteger(value)
			const nodeType = isInt ? 'inputInt' : 'inputDouble'
			const type = isInt ? 'int' : 'double'

			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: type,
					resultType: type,
					nodeType,
					value
				})),
				outputValues: Map()
			}
		}
		case 'setInputBoolean': {
			const { id, value } = action.payload
			return {
				...state,
				nodes: state.nodes.update(id, emptyNode, (node) => ({
					...node,
					name: 'bool',
					resultType: 'bool',
					nodeType: 'inputBoolean',
					value
				})),
				outputValues: Map()
			}
		}
		case 'setOutputValues': {
			return {
				...state,
				outputValues: action.payload
			}
		}
		case 'deleteNode': {
			const newEdges = state.edges.filter(
				({ startNodeId, endNodeId }) =>
					![startNodeId, endNodeId].includes(action.payload)
			)

			return {
				...state,
				nodes: state.nodes.delete(action.payload),
				edges: newEdges,
				cycleEdges: detectCycles(newEdges.toSet()),
				outputValues: Map(),
				incompatibleEdges: state.incompatibleEdges.filter(
					({ startNodeId, endNodeId }) =>
						![startNodeId, endNodeId].includes(action.payload)
				)
			}
		}
		case 'deleteEdge': {
			const newEdges = state.edges.delete(action.payload)
			return {
				...state,
				edges: newEdges,
				cycleEdges: detectCycles(newEdges.toSet()),
				outputValues: Map(),
				incompatibleEdges: state.incompatibleEdges.filter(
					({ id }) => id !== action.payload
				)
			}
		}
		case 'importGraph': {
			const { data, castData } = action.payload
			const { nodes, edges } = data
			const errors: string[] = []

			const cycleEdges = detectCycles(edges.toSet())
			if (!cycleEdges.isEmpty) errors.push('Cycle detected')

			let incompatibleEdges = Set<AlgorithmEdge>()
			const validatedEdges = edges.filter((edge) => {
				const startNode = nodes.get(edge.startNodeId)
				const endNode = nodes.get(edge.endNodeId)

				if (!startNode || !endNode || !endNode.params[edge.endParamIndex]) {
					errors.push(`Import error: edge ${edge.id} is invalid`)
					return false
				}

				const endType = endNode.params[edge.endParamIndex].type
				if (!isCastable(castData, startNode.resultType, endType)) {
					incompatibleEdges = incompatibleEdges.add(edge)
					errors.push(incompatibleTypeError(startNode.resultType, endType))
				}

				return true
			})

			return {
				...initialAlgorithmState,
				nodes,
				edges: validatedEdges,
				cycleEdges,
				errors,
				incompatibleEdges
			}
		}
		case 'clear' : {
			return initialAlgorithmState
		}
		default: {
			return state
		}
	}
}
