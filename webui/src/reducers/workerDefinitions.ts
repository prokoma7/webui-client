import { Map } from 'immutable'
import { AlgorithmCategory } from 'interfaces/Algorithms'
import { categorizeAlgorithms } from 'utils/alt/algorithmCategorization'
import { processCasts } from 'utils/alt/casts'
import { ActionsUnion, createAction } from 'utils/redux'

export interface WorkerDefinitionsState {
	algorithms: Map<string, AlgorithmCategory>
	casts: any
	version: string
}

export const initialWorkerDefinitionsState = {
	algorithms: Map<string, AlgorithmCategory>(),
	casts: {},
	version: "<unknown>",
}

export const workerDefinitionsActions = {
	setData: (algorithms: any, casts: any, version: string) =>
		createAction('setData', { algorithms: algorithms, casts: casts, version: version }),
}

export type WorkerDefinitionsActions = ActionsUnion<typeof workerDefinitionsActions>

export const workerDefinitionsReducer = (state: WorkerDefinitionsState = initialWorkerDefinitionsState, action: any) => {
	switch(action.type) {
		case 'setData':
			let algorithms = action.payload.algorithms
			let casts = action.payload.casts
			let version = action.payload.version

			return {
				...state,
				algorithms: categorizeAlgorithms(algorithms),
				casts: processCasts(algorithms, casts),
				version: version,
			}

		default:
			return state;
	}
}
