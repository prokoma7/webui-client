import Position, { zoom } from '../interfaces/Position'
import { ActionsUnion, createAction } from '../utils/redux'
import { round as roundNum } from '../utils/number'
import { CursorMode } from '../interfaces/Canvas'

const WIDTH = document.documentElement.clientWidth
const HEIGHT = document.documentElement.clientHeight

export const canvasActionType = (canvasId: string, type: string) =>
	`${canvasId}_${type}`

export const createCanvasAction = <PayloadType extends any>(
	canvasId: string,
	type: string,
	payload?: PayloadType
) => createAction(canvasActionType(canvasId, type), payload)

export const canvasActionsFactory = (canvasId: string) => ({
	/** Sets cursor to position */
	setCursor: (value: Position) =>
		createCanvasAction(canvasId, 'setCursor', value),
	/** Sets offset to position */
	setOffset: (value: Position) =>
		createCanvasAction(canvasId, 'setOffset', value),
	/** Sets cursor mode to select or move */
	setCursorMode: (value: CursorMode) =>
		createCanvasAction(canvasId, 'setCursorMode', value),
	/** Sets id of state being positioned */
	setPositioningNode: (id: string) =>
		createCanvasAction(canvasId, 'setPositioningNode', id),
	/** Sets scale and offset of new zoom factor */
	zoom: (factor: number, to?: Position) =>
		createCanvasAction(canvasId, 'zoom', { factor, to }),
	/** Deselects canvas elements */
	deselect: () => createCanvasAction(canvasId, 'deselect'),
	/** Selects state by its id */
	selectNode: (id: string) => createCanvasAction(canvasId, 'selectNode', id),
	/** Selects transition by its id */
	selectEdge: (id: string) => createCanvasAction(canvasId, 'selectEdge', id),
	/** Sets the build state mode value */
	setBuildNode: (value: boolean) =>
		createCanvasAction(canvasId, 'setBuildNode', value),
	/** Sets the build edge mode value */
	setBuildEdge: (value: boolean) =>
		createCanvasAction(canvasId, 'setBuildEdge', value),
	/** Sets the offset from the center of dragged element */
	setMouseDownOffset: (value: Position) =>
		createCanvasAction(canvasId, 'setMouseDownOffset', value),
	/** Toggles grid on/off */
	toggleGrid: () => createCanvasAction(canvasId, 'toggleGrid')
})

export type CanvasActions = ActionsUnion<
	ReturnType<typeof canvasActionsFactory>
>

export interface CanvasState {
	/** Canvas modes of mouse interaction */
	cursorMode: CursorMode
	/** Cursor position */
	cursor: Position
	/** Canvas offset position */
	offset: Position
	/** Canvas scale */
	scale: number
	/** Id of selected state */
	selectedNode: string
	/** Id of selected transition */
	selectedEdge: string
	/** Id of positioning state */
	positioningNode: string
	/** Build node state */
	buildNode: boolean
	/** Build edge state */
	buildEdge: boolean
	/** Distance of the mousedown event from the center of the selected node */
	mouseDownOffset: Position
	/** Toggles grid on/off */
	gridOn: boolean
}

export const initialState: CanvasState = {
	cursor: { x: 0, y: 0 },
	offset: { x: 0, y: 0 },
	cursorMode: 'select',
	scale: 1,
	selectedNode: '',
	selectedEdge: '',
	positioningNode: '',
	buildNode: false,
	buildEdge: false,
	mouseDownOffset: { x: 0, y: 0 },
	gridOn: false
}

export const canvasReducerFactory = (canvasId: string) => {
	const actionType = (type: string) => canvasActionType(canvasId, type)

	return (state = initialState, action: CanvasActions): CanvasState => {
		switch (action.type) {
			case actionType('setCursor'): {
				return {
					...state,
					cursor: action.payload as Position
				}
			}
			case actionType('setOffset'): {
				return {
					...state,
					offset: action.payload as Position
				}
			}
			case actionType('setCursorMode'): {
				return {
					...state,
					cursorMode: action.payload as CursorMode
				}
			}
			case actionType('setPositioningNode'): {
				return {
					...state,
					positioningNode: action.payload as string
				}
			}
			case actionType('zoom'): {
				const { factor, to } = action.payload as { factor: number, to: Position }
				const newScale = state.scale + factor
				if (newScale > 3 || newScale < 0.15) return state

				const newOffset = zoom(
					to || { x: WIDTH / 2, y: HEIGHT / 2 },
					state.offset,
					state.scale,
					factor
				)

				return {
					...state,
					scale: roundNum(newScale),
					offset: { ...newOffset }
				}
			}
			case actionType('deselect'): {
				return {
					...state,
					selectedNode: '',
					selectedEdge: ''
				}
			}
			case actionType('selectNode'): {
				return {
					...state,
					selectedNode: action.payload as string,
					selectedEdge: ''
				}
			}
			case actionType('selectEdge'): {
				return {
					...state,
					selectedNode: '',
					selectedEdge:
						state.selectedEdge === action.payload
							? ''
							: action.payload as string
				}
			}
			case actionType('setBuildNode'): {
				return {
					...state,
					buildNode: action.payload as boolean
				}
			}
			case actionType('setBuildEdge'): {
				return {
					...state,
					buildEdge: action.payload as boolean
				}
			}
			case actionType('setMouseDownOffset'): {
				return {
					...state,
					mouseDownOffset: action.payload as Position
				}
			}
			case actionType('toggleGrid'): {
				return {
					...state,
					gridOn: !state.gridOn
				}
			}
			default:
				return state
		}
	}
}
