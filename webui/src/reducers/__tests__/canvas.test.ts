import { initialState, canvasActionsFactory, CanvasState, canvasReducerFactory } from '../canvas'

describe('canvas reducer', () => {
	const canvasId = 'id'
	
	const reducer = canvasReducerFactory(canvasId)
	const actions = canvasActionsFactory(canvasId)

	it('sets cursor', () => {
		expect(
			reducer(initialState, actions.setCursor({ x: 10, y: 10 }))
		).toEqual({
			...initialState,
			cursor: { x: 10, y: 10 }
		} as CanvasState)
	})

	it('sets offset', () => {
		expect(
			reducer(initialState, actions.setOffset({ x: 10, y: 10 }))
		).toEqual({
			...initialState,
			offset: { x: 10, y: 10 }
		} as CanvasState)
	})

	it('sets cursor mode', () => {
		expect(reducer(initialState, actions.setCursorMode('select'))).toEqual({
			...initialState,
			cursorMode: 'select'
		} as CanvasState)
	})

	it('sets positioning state', () => {
		expect(reducer(initialState, actions.setPositioningNode('x'))).toEqual(
			{
				...initialState,
				positioningNode: 'x'
			} as CanvasState
		)
	})

	it('correctly zooms', () => {
		expect(
			reducer(initialState, actions.zoom(0.1, { x: 100, y: 100 }))
		).toEqual({
			...initialState,
			scale: 1.1,
			offset: { x: -10, y: -10 }
		} as CanvasState)
	})

	it('correctly zooms to center', () => {
		expect(reducer(initialState, actions.zoom(0.1))).toEqual({
			...initialState,
			scale: 1.1,
			offset: { x: 0, y: 0 }
		} as CanvasState)
	})

	it("doesn't zoom over 3", () => {
		expect(
			reducer(initialState, actions.zoom(2.1, { x: 100, y: 100 }))
		).toEqual(initialState)
	})

	it("doesn't zoom under 0.15", () => {
		expect(
			reducer(initialState, actions.zoom(-0.86, { x: 100, y: 100 }))
		).toEqual(initialState)
	})

	it('correctly deselects', () => {
		expect(reducer(initialState, actions.deselect())).toEqual({
			...initialState,
			selectedNode: '',
			selectedEdge: ''
		} as CanvasState)
	})

	it('correctly selects state', () => {
		expect(reducer(initialState, actions.selectNode('x'))).toEqual({
			...initialState,
			selectedNode: 'x'
		} as CanvasState)
	})

	it('correctly selects transition', () => {
		expect(reducer(initialState, actions.selectEdge('x'))).toEqual({
			...initialState,
			selectedEdge: 'x'
		} as CanvasState)
	})

	it('correctly deselects transition', () => {
		expect(
			reducer(
				{
					...initialState,
					selectedEdge: 'x'
				},
				actions.selectEdge('x')
			)
		).toEqual({
			...initialState,
			selectedEdge: ''
		} as CanvasState)
	})
})
