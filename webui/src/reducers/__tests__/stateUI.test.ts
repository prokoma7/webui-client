import reducer, { initialStateUIState, stateUIActions, State } from '../stateUI'

describe('toolbar reducer', () => {
	it('sets input', () => {
		expect(reducer(initialStateUIState, stateUIActions.setInput('x'))).toEqual({
			...initialStateUIState,
			input: 'x'
		} as State)
	})

	it('sets algorithm', () => {
		expect(reducer(initialStateUIState, stateUIActions.setAlgorithm('force'))).toEqual({
			...initialStateUIState,
			algorithm: 'force'
		} as State)
	})

	it('resets input', () => {
		expect(reducer(initialStateUIState, stateUIActions.resetInput())).toEqual(
			initialStateUIState
		)
	})

	it('adds error', () => {
		expect(
			reducer(
				{
					...initialStateUIState,
					errors: ['x']
				},
				stateUIActions.addError('err')
			)
		).toEqual({
			...initialStateUIState,
			errors: ['x', 'err']
		} as State)
	})

	it('sets errors', () => {
		expect(
			reducer(
				{
					...initialStateUIState,
					errors: ['x']
				},
				stateUIActions.setErrors(['err1', 'err2'])
			)
		).toEqual({
			...initialStateUIState,
			errors: ['err1', 'err2']
		} as State)
	})

	it('resets errors', () => {
		expect(
			reducer(
				{
					...initialStateUIState,
					errors: ['x']
				},
				stateUIActions.resetErrors()
			)
		).toEqual(initialStateUIState)
	})

	it('toggles build state', () => {
		expect(reducer(initialStateUIState, stateUIActions.toggleBuildState())).toEqual({
			...initialStateUIState,
			buildState: true
		} as State)
		expect(
			reducer(initialStateUIState, stateUIActions.toggleBuildState('initial'))
		).toEqual({
			...initialStateUIState,
			buildState: true,
			buildStateType: 'initial'
		} as State)
		expect(
			reducer(initialStateUIState, stateUIActions.toggleBuildState('final'))
		).toEqual({
			...initialStateUIState,
			buildState: true,
			buildStateType: 'final'
		} as State)
	})

	it('toggles build transition', () => {
		expect(reducer(initialStateUIState, stateUIActions.toggleBuildTransition())).toEqual({
			...initialStateUIState,
			buildTransition: true
		} as State)
	})

	it('sets build state', () => {
		expect(reducer(initialStateUIState, stateUIActions.setBuildState(true))).toEqual({
			...initialStateUIState,
			buildState: true
		} as State)
	})

	it('sets build transition', () => {
		expect(reducer(initialStateUIState, stateUIActions.setBuildTransition(true))).toEqual(
			{
				...initialStateUIState,
				buildTransition: true
			} as State
		)
	})

	it('toggles hint', () => {
		expect(reducer(initialStateUIState, stateUIActions.toggleShowHint())).toEqual({
			...initialStateUIState,
			showHint: false
		} as State)
	})

	it('sets explore', () => {
		expect(reducer(initialStateUIState, stateUIActions.setShowExplore(true))).toEqual({
			...initialStateUIState,
			showExplore: true
		} as State)
	})

	it('resets', () => {
		expect(reducer(initialStateUIState, stateUIActions.reset())).toEqual({
			...initialStateUIState,
			buildState: false,
			buildTransition: false,
			input: '',
			errors: []
		} as State)
	})

	it('sets confirm text', () => {
		expect(reducer(initialStateUIState, stateUIActions.setConfirmText('x'))).toEqual({
			...initialStateUIState,
			confirmText: 'x'
		} as State)
	})

	it('sets mouse mode', () => {
		expect(
			reducer(initialStateUIState, stateUIActions.setMouseMode('horizontal'))
		).toEqual({
			...initialStateUIState,
			mouseMode: 'horizontal'
		} as State)
	})
})
