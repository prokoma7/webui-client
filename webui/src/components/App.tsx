import React, { useRef, useCallback, useMemo } from 'react'
import { ThemeProvider, CssBaseline, IconButton } from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'
import './App.css'
import { WebUI } from './WebUI/WebUI'
import { theme } from '../interfaces/Theme'
import { SnackbarProvider, SnackbarKey } from 'notistack'
import { AlgorithmErrorNotifier } from './WebUI/AlgorithmErrorNotifier'
import { makeStyles } from '@material-ui/styles'

const useSnackbarStyles = makeStyles({
	root: {
		'& > div': {
			flexWrap: 'nowrap'
		}
	}
})
/**
 * App structure definition
 *
 * @returns
 */
const App = () => {
	const snackbarClasses = useSnackbarStyles()
	const snackbarProviderRef = useRef<SnackbarProvider>(null)

	const snackbarAction = useCallback(
		(key: SnackbarKey) => (
			<IconButton
				color="inherit"
				onClick={() => snackbarProviderRef.current?.closeSnackbar(key)}
			>
				<CloseIcon />
			</IconButton>
		),
		[snackbarProviderRef]
	)

	const snackbarAnchor = useMemo(
		() =>
			({
				horizontal: 'center',
				vertical: 'top'
			} as const),
		[]
	)

	return (
		<ThemeProvider theme={theme}>
			<SnackbarProvider
				ref={snackbarProviderRef}
				classes={snackbarClasses}
				maxSnack={3}
				anchorOrigin={snackbarAnchor}
				action={snackbarAction}
			>
				<CssBaseline />
				<AlgorithmErrorNotifier />
				<WebUI />
			</SnackbarProvider>
		</ThemeProvider>
	)
}

export default App
