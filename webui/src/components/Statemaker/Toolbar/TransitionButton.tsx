import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import TransitionIcon from '@material-ui/icons/ArrowRightAlt'

import { Actions, ReduxState } from '../../../reducers'
import ElementButton from '../../Toolbar/ElementButton'

const { setCursorMode, toggleBuildTransition } = Actions

/**
 * Allows building transition
 *
 * @returns
 */
function TransitionButton() {
	const dispatch = useDispatch()
	const { selected } = useSelector(
		(state: ReduxState) => ({
			selected: state.stateUI.buildTransition
		})
	)

	const handleToggleTrainsition = React.useCallback(() => {
		batch(() => {
			dispatch(setCursorMode('select'))
			dispatch(toggleBuildTransition())
		})
	}, [dispatch])

	return (
		<ElementButton
			Icon={TransitionIcon}
			label="Add Transition"
			selected={selected}
			onAction={handleToggleTrainsition}
		/>
	)
}

export default TransitionButton
