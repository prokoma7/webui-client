import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import AlgorithmIcon from '@material-ui/icons/Settings'

import { Actions, ReduxState } from '../../../reducers'
import MenuButton from './MenuButton'

const { setAlgorithm } = Actions

/**
 * Sets positioning algorithm
 *
 * @returns
 */
function SetAlgorithmButton() {
	const dispatch = useDispatch()
	const { algorithm } = useSelector(
		(state: ReduxState) => ({
			algorithm: state.stateUI.algorithm
		})
	)

	const handleChooseAlg = React.useCallback(
		(variant: 'force' | 'layer') => () => {
			dispatch(setAlgorithm(variant))
		},
		[dispatch]
	)

	return (
		<React.Fragment>
			<MenuButton
				Icon={AlgorithmIcon}
				label="Choose positioning algorithm"
				selected={false}
				menuItems={[
					{
						label: 'Force-directed',
						selected: algorithm === 'force',
						onAction: handleChooseAlg('force')
					},
					{
						label: 'Hierarchical',
						selected: algorithm === 'layer',
						onAction: handleChooseAlg('layer')
					}
				]}
			/>
		</React.Fragment>
	)
}

export default SetAlgorithmButton
