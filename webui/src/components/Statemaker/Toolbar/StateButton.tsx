import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import SvgIcon from '@material-ui/core/SvgIcon'

import { ReactComponent as StateIcon } from '../../../icons/state.svg'
import { ReactComponent as InitialStateIcon } from '../../../icons/state_initial.svg'
import { ReactComponent as FinalStateIcon } from '../../../icons/state_final.svg'
import { StateType } from '../../../interfaces/State'
import { Actions, ReduxState } from '../../../reducers'
import ElementButton from '../../Toolbar/ElementButton'

const { setCursorMode, toggleBuildState } = Actions

interface Props {
	/** State variant */
	variant: StateType
}

/**
 * Allows drag and drop of states from the toolbar
 *
 * @param {Props} props
 * @returns
 */
function StateButton(props: Props) {
	const { variant } = props
	const dispatch = useDispatch()
	const { selected } = useSelector(
		(state: ReduxState) => ({
			selected:
				state.stateUI.buildState &&
				state.stateUI.buildStateType === variant
		})
	)

	const handleToggleBuildState = React.useCallback(() => {
		batch(() => {
			dispatch(setCursorMode('select'))
			dispatch(toggleBuildState(variant))
		})
	}, [dispatch, variant])

	let Icon = StateIcon
	let label = ' Add State'
	if (variant === 'initial') {
		Icon = InitialStateIcon
		label = ' Add Initial State'
	} else if (variant === 'final') {
		Icon = FinalStateIcon
		label = ' Add Final State'
	}

	return (
		<ElementButton
			Icon={() => (
				<SvgIcon>
					<Icon />
				</SvgIcon>
			)}
			label={label}
			onAction={handleToggleBuildState}
			event="onMouseDown"
			selected={selected}
		/>
	)
}

export default StateButton
