import * as React from 'react'
import ImportIcon from '@material-ui/icons/CloudUpload'
import ElementButton from '../../Toolbar/ElementButton'

export interface ImportButtonProps {
	onImport?: (e: React.MouseEvent) => void
}

/**
 * Manages importing state machine from file
 *
 * @returns
 */
const ImportButton = (props: ImportButtonProps) => {
	const { onImport } = props

	return (
		<ElementButton
			Icon={ImportIcon}
			label="Import from file"
			onAction={onImport}
		/>
	)
}

export default ImportButton
