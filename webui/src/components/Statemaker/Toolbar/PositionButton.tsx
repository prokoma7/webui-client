import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PositionIcon from '@material-ui/icons/GpsFixed'

import { Actions, ReduxState } from '../../../reducers'
import ElementButton from '../../Toolbar/ElementButton'


const { position } = Actions

/**
 * Automatically positions state machine
 *
 * @returns
 */
function PositionButton() {
	const dispatch = useDispatch()
	const { algorithm } = useSelector(
		(state: ReduxState) => ({
			algorithm: state.stateUI.algorithm
		})
	)

	const handlePosition = React.useCallback(() => {
		dispatch(position(algorithm))
	}, [dispatch, algorithm])

	return (
		<ElementButton
			Icon={PositionIcon}
			label="Auto-position"
			onAction={handlePosition}
		/>
	)
}

export default PositionButton
