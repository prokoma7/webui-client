import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { makeStyles } from '@material-ui/styles'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

import { ReduxState, Actions } from '../../reducers'

const { setConfirmText } = Actions

const useStyles = makeStyles({
	close: {
		padding: 4
	}
})

/**
 * Shows info snackbar
 *
 * @returns
 */
function Confirm() {
	const dispatch = useDispatch()
	const [open, setOpen] = React.useState(false)
	const classes = useStyles()
	const { text } = useSelector(
		(state: ReduxState) => ({
			text: state.stateUI.confirmText
		})
	)

	// Close on close button click or when timeout
	const handleClose = React.useCallback(
		(_: React.MouseEvent<HTMLElement>, reason?: string) => {
			if (reason === 'clickaway') return
			setOpen(false)
			setTimeout(() => {
				dispatch(setConfirmText(''))
			}, 1000)
		},
		[dispatch]
	)

	React.useEffect(() => {
		if (!!text) setOpen(true)
	}, [text])

	return (
		<Snackbar
			anchorOrigin={{
				vertical: 'top',
				horizontal: 'center'
			}}
			open={open}
			onClose={handleClose as any}
			autoHideDuration={1000}
		>
			<SnackbarContent
				message={text}
				action={
					<IconButton
						color="inherit"
						className={classes.close}
						onClick={handleClose}
					>
						<CloseIcon />
					</IconButton>
				}
			/>
		</Snackbar>
	)
}

export default Confirm
