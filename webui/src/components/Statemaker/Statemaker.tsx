import React from 'react'

import Hint from './StatemakerHints'
import Confirm from './Confirm'
import Errors from './StatemakerErrors'
import ExploreDialog from './ExploreDialog'
import StatemakerOverlay from './StatemakerOverlay'
import StateCanvas from './StateCanvas'


const Statemaker = () => (
	<>
		<Hint />
		<Confirm />
		<Errors />
		<ExploreDialog />
		<StatemakerOverlay />
		<StateCanvas />
	</>
)

export default Statemaker
