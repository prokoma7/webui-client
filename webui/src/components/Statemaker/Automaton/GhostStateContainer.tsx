import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import useStatePosition from '../../../hooks/useStatePosition'
import useTextSize from '../../../hooks/useTextSize'

import { Actions, ReduxState } from '../../../reducers'
import State from './State'

const {
	addState,
	incrementStateNameId,
	setBuildState,
	updateGhostState
} = Actions

interface GhostStateProps {
	canvasRef: React.RefObject<SVGSVGElement>
}

/**
 * Ghost state shown when drawing new state on canvas
 * Uses mouse cursor for position
 *
 * @returns
 */
function GhostStateContainer(props: GhostStateProps) {
	const { canvasRef } = props
	const dispatch = useDispatch()

	const { stateNameId, show, data, type } = useSelector(
		(state: ReduxState) => ({
			stateNameId: state.stateUI.stateNameId,
			show: state.stateUI.buildState,
			data: state.stateUI.ghostState,
			type: state.stateUI.buildStateType || 'default'
		})
	)

	const position = useStatePosition(canvasRef, show)

	const handleTextChange = React.useCallback(
		(size: number) => dispatch(updateGhostState({ size: size / 2 })),
		[dispatch]
	)

	const textRef = useTextSize(data.name, handleTextChange)

	// Create new state on cursor and increase name auto generator counter
	const handleMouseUp = React.useCallback(() => {
		batch(() => {
			dispatch(
				addState(
					{
						name: data.name + stateNameId,
						position,
						size: data.size
					},
					type
				)
			)

			dispatch(incrementStateNameId())
			dispatch(setBuildState(false))
		})
	}, [dispatch, position, stateNameId, type, data.name, data.size])

	if (!show) return null

	return (
		<State
			ref={textRef}
			data={{
				...data,
				name: data.name + stateNameId,
				position
			}}
			isInitial={type === 'initial'}
			isFinal={type === 'final'}
			isGhost={true}
			onMouseUp={handleMouseUp}
		/>
	)
}

export default GhostStateContainer
