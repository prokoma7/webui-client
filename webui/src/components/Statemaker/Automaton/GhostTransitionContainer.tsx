import * as React from 'react'
import { useSelector } from 'react-redux'

import useStatePosition from '../../../hooks/useStatePosition'
import { ReduxState } from '../../../reducers'
import { getEdgePointsCursor } from '../../../utils/positioning'
import Transition from './Transition'

interface GhostTransitionProps {
	canvasRef: React.RefObject<SVGSVGElement>
}

/**
 * Ghost transition shown when drawing new transition on canvas
 * Uses mouse cursor for position
 *
 * @returns
 */
function GhostTransitionContainer(props: GhostTransitionProps) {
	const { canvasRef } = props
	const { data, startState, buildTransition } = useSelector(
		(state: ReduxState) => {
			const transition = state.stateUI.ghostTransition
			// Find the correct start state
			const startState = transition
				? state.stateData.present.states[transition.startState]
				: undefined

			return {
				data: {
					...transition,
					name: transition.name + state.stateUI.transitionNameId
				},
				startState,
				buildTransition: state.stateUI.buildTransition
			}
		}
	)

	const position = useStatePosition(canvasRef, buildTransition)
	
	if (!startState || !buildTransition) return null

	// Calculate the start and end points of the transition
	const points = getEdgePointsCursor(startState, position)

	return <Transition data={data} {...points} isGhost={true} offset={0} />
}

export default GhostTransitionContainer
