import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles, Paper } from '@material-ui/core'
import {
	Close as CloseIcon,
	Undo as UndoIcon,
	Redo as RedoIcon,
	Delete as DeleteIcon,
	HelpOutline as HelpIcon
} from '@material-ui/icons'
import { CanvasToolbar } from '../../Toolbar/CanvasToolbar'
import { ReduxState } from '../../../reducers'
import { algorithmCanvasActions } from '../../../reducers/algorithmCanvas'
import { CursorMode } from '../../../interfaces/Canvas'
import { AlgorithmImportExportToolbar } from './AlgorithmImportExportToolbar'

import { algorithmDataActions } from '../../../reducers/algorithmData'
import ElementButton from '../../Toolbar/ElementButton'
import { Actions } from '../../../reducers'
import { useAlgorithmShare } from '../../../hooks/useAlgorithmShare'
import { useAlgorithmExport } from '../../../hooks/useAlgorithmExport'
import { useAlgorithmImport } from '../../../hooks/useAlgorithmImport'
import { useAlgorithmDelete } from '../../../hooks/useAlgorithmDelete'
import { algorithmUIActions } from '../../../reducers/algorithmUI'

const { toggleGrid, setCursorMode, zoom } = algorithmCanvasActions
const { clear } = algorithmDataActions
const { undoAlgorithm, redoAlgorithm } = Actions
const { toggleHint } = algorithmUIActions

const useStyles = makeStyles((theme) => ({
	root: {
		position: 'absolute',
		top: '55px',
		right: '8px',
		'& > *': {
			marginLeft: 8,
			marginTop: 8,
			display: 'flex',
			flexDirection: 'column',
			overflow: 'hidden',
			'& > button': {
				borderRadius: 0,
				color: 'rgba(0, 0, 0, 0.54)',
				'&:hover': {
					color: theme.palette.primary.light,
					backgroundColor: 'transparent'
				},
				'&[data-selected="true"]': {
					color: theme.palette.primary.main
				}
			}
		}
	}
}))

export const AlgorithmCanvasToolbar = () => {
	const dispatch = useDispatch()
	const classes = useStyles()
	const {
		cursorMode,
		gridOn,
		elementSelected,
		pastEmpty,
		futureEmpty,
		showHint
	} = useSelector((state: ReduxState) => ({
		cursorMode: state.algorithmCanvas.cursorMode,
		gridOn: state.algorithmCanvas.gridOn,
		elementSelected:
			state.algorithmCanvas.selectedNode ||
			state.algorithmCanvas.selectedEdge,
		pastEmpty: state.algorithmData.past.length === 0,
		futureEmpty: state.algorithmData.future.length === 0,
		showHint: state.algorithmUI.showHint
	}))

	const handleDelete = useAlgorithmDelete()

	const handleSelectCursorModeFactory = useCallback(
		(cursorMode: CursorMode) => () => dispatch(setCursorMode(cursorMode)),
		[dispatch]
	)

	const handleSelectCursorMode = useCallback(
		handleSelectCursorModeFactory('select'),
		[handleSelectCursorModeFactory]
	)

	const handleMoveCursorMode = useCallback(
		handleSelectCursorModeFactory('move'),
		[handleSelectCursorModeFactory]
	)

	const handleZoomIn = useCallback(() => dispatch(zoom(0.25)), [dispatch])

	const handleZoomOut = useCallback(() => dispatch(zoom(-0.25)), [dispatch])

	const handleGridToggle = useCallback(() => dispatch(toggleGrid()), [
		dispatch
	])

	const handleShare = useAlgorithmShare()
	const handleExport = useAlgorithmExport()
	const handleImport = useAlgorithmImport()

	const handleUndo = useCallback(() => dispatch(undoAlgorithm()), [dispatch])
	const handleRedo = useCallback(() => dispatch(redoAlgorithm()), [dispatch])
	const handleClear = useCallback(() => dispatch(clear()), [dispatch])
	const handleToggleHint = useCallback(() => dispatch(toggleHint()), [
		dispatch
	])

	return (
		<div className={classes.root}>
			<CanvasToolbar
				cursorMode={cursorMode}
				gridOn={gridOn}
				onSelectCursorMode={handleSelectCursorMode}
				onMoveCursorMode={handleMoveCursorMode}
				onZoomIn={handleZoomIn}
				onZoomOut={handleZoomOut}
				onGrid={handleGridToggle}
			/>
			<Paper>
				<ElementButton
					Icon={DeleteIcon}
					label="Delete selected"
					isDisabled={!elementSelected}
					onAction={handleDelete}
				/>
				<ElementButton
					Icon={UndoIcon}
					label="Undo"
					onAction={handleUndo}
					isDisabled={pastEmpty}
				/>
				<ElementButton
					Icon={RedoIcon}
					label="Redo"
					onAction={handleRedo}
					isDisabled={futureEmpty}
				/>
				<ElementButton
					Icon={CloseIcon}
					label="Clear canvas"
					onAction={handleClear}
				/>
			</Paper>
			<AlgorithmImportExportToolbar
				onShare={handleShare}
				onExport={handleExport}
				onImport={handleImport}
			/>
			<Paper>
				<ElementButton
					Icon={HelpIcon}
					label="Toggle hint"
					onAction={handleToggleHint}
					selected={showHint}
				/>
			</Paper>
		</div>
	)
}
