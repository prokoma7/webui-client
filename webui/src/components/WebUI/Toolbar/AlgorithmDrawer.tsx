import React, { useMemo, useCallback } from 'react'
import {
	Drawer,
	makeStyles,
	List,
	ListItem,
	ListItemText,
	Divider,
} from '@material-ui/core'
import { WebUITheme } from '../../../interfaces/Theme'
import {
	automatonTypes,
	automanAlibTypeMap,
} from '../../../interfaces/AutomatonTypes'
import { batch, useDispatch, useSelector } from 'react-redux'
import AlgorithmList from './AlgorithmList/AlgorithmList'
import { algorithmCanvasActions } from '../../../reducers/algorithmCanvas'
import { algorithmUIActions } from '../../../reducers/algorithmUI'
import { ReduxState } from '../../../reducers'
import { AlgorithmNodeType, AlgorithmNodeValue } from '../../../interfaces/AlgorithmNode'
import { AlibType, AlgorithmParam } from '../../../interfaces/Algorithms'

const { setBuildNode } = algorithmCanvasActions
const { updateGhostNode, setAlgorithmToolbarOpen } = algorithmUIActions

const useStyles = makeStyles<WebUITheme>((theme) => ({
	drawer: {
		width: theme.drawerWidth,
	},
	drawerPaper: {
		width: theme.drawerWidth,
	},
}))

export const AlgorithmDrawer = () => {
	const dispatch = useDispatch()
	const classes = useStyles()

	const { toolbarOpen } = useSelector((state: ReduxState) => ({
		algorithmService: state.service.algorithmsService,
		toolbarOpen: state.algorithmUI.toolbarOpen,
	}))

	const drawerClassNames = useMemo(() => ({ paper: classes.drawerPaper }), [
		classes.drawerPaper,
	])

	const handleButtonClick = useCallback(
		(
			nodeType: AlgorithmNodeType,
			name: string,
			resultType: AlibType,
			params: AlgorithmParam[],
			value?: AlgorithmNodeValue
		) =>
			batch(() => {
				dispatch(
					updateGhostNode({
						nodeType,
						name,
						params,
						resultType,
						height: 75,
						value,
						templateParams: [],
					})
				)
				dispatch(setBuildNode(true))
				dispatch(setAlgorithmToolbarOpen(false))
			}),
		[dispatch]
	)

	const handleInputClick = useCallback(
		() =>
			handleButtonClick(
				'inputString',
				'string',
				'std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >',
				[],
				''
			),
		[handleButtonClick]
	)

	const handleAutomatonInputClick = useCallback(
		() =>
			handleButtonClick(
				'inputAutomaton',
				'Automaton',
				automanAlibTypeMap['DFA'],
				[]
			),
		[handleButtonClick]
	)

	const handleOutputButtonClick = useCallback(
		() =>
			handleButtonClick('output', 'Output', '', [
				{ name: 'input', type: 'abstraction::UnspecifiedType' },
			]),
		[handleButtonClick]
	)

	const handleAutomatonOutputClick = useCallback(
		() =>
			handleButtonClick('outputAutomaton', 'Automaton output', '', [
				{
					name: 'automaton',
					type: automatonTypes.reduce(
						(acc, type, index, arr) =>
							index === arr.length - 1
								? automanAlibTypeMap[type]
								: `${automanAlibTypeMap[type]} | ${acc}`,
						''
					),
				},
			]),
		[handleButtonClick]
	)

	const handleDotOutputClick = useCallback(
		() =>
			handleButtonClick('outputDot', 'Dot Output', '', [
				{
					name: 'input',
					type:
						'DotConvertible',
				},
			]),
		[handleButtonClick]
	)

	return (
		<Drawer
			className={classes.drawer}
			classes={drawerClassNames}
			variant={'persistent'}
      open={toolbarOpen}
		>
			<List>
				<ListItem button onClick={handleInputClick}>
					<ListItemText>Input</ListItemText>
				</ListItem>
				<ListItem button onClick={handleAutomatonInputClick}>
					<ListItemText>Automaton Input</ListItemText>
				</ListItem>
			</List>
			<Divider />
			<List>
				<ListItem button onClick={handleOutputButtonClick}>
					<ListItemText>Output</ListItemText>
				</ListItem>
				<ListItem button onClick={handleAutomatonOutputClick}>
					<ListItemText>Automaton Output</ListItemText>
				</ListItem>
				<ListItem button onClick={handleDotOutputClick}>
					<ListItemText>Dot Output</ListItemText>
				</ListItem>
			</List>
      <Divider />
			<AlgorithmList />
		</Drawer>
	)
}
