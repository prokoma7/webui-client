import React, { useCallback } from 'react'
import { Overload } from '../../../../interfaces/Algorithms'
import {
	List,
	ListItem,
	ListItemText,
	makeStyles,
	Divider,
	Paper
} from '@material-ui/core'
import { theme } from '../../../../interfaces/Theme'
import { Param } from './Param'
import { Doxygen } from './Doxygen'

interface OverloadListItemProps {
	overload: Overload
	onClick?: (overload: Overload) => void
}

const useStyles = makeStyles({
	root: {
		padding: theme.spacing(1)
	},
	paper: {
		width: '100%'
	},
	listItemText: {
		display: 'flex',
		flexDirection: 'column',
		padding: 'unset',
		'& > *': {
			marginBottom: theme.spacing(0)
		},
		'& > :last-child': {
			marginBottom: theme.spacing(0)
		}
	},
	overload: {
		padding: '0',
	},
})

function createParam(param: any, idx: number) {
	// Let's consider param names that are named 'arg<i>' as unnamed, i.e., remove 'name' key
	const ARG_REGEXP = /^arg\d+$/g
	if (param.name.match(ARG_REGEXP)) {
		const {name, ...nparam} = param;
		param = nparam;
	}

	return (
		<React.Fragment key={idx}>
			<Param {...param} order={idx} />
		</React.Fragment>
	)
}

export const OverloadListItem = (props: OverloadListItemProps) => {
	const { overload, onClick } = props
	const classes = useStyles()

	const handleClick = useCallback(() => onClick?.(overload), [
		onClick,
		overload
	])

	return (
		<Paper className={classes.paper} variant="outlined">
			<ListItem className={classes.root} button onClick={handleClick}>
				<ListItemText
					className={classes.listItemText}
					disableTypography
				>
					<List dense className={classes.overload}>
						{overload.params.map((param, paramIndex) => createParam(param, paramIndex + 1))}
						<Param
							type={overload.resultType}
							order="return"
						/>
						{overload.docs && (
							<>
								<Divider />
								<Doxygen docstring={overload.docs} />
							</>
						)}
					</List>
				</ListItemText>
			</ListItem>
		</Paper>
	)
}
