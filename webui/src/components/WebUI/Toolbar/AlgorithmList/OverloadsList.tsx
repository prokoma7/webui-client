import React, { useCallback } from 'react'
import { Algorithm, Overload } from '../../../../interfaces/Algorithms'
import { OverloadListItem } from './OverloadListItem'
import { Actions } from '../../../../reducers'
import { useDispatch, batch } from 'react-redux'
import { makeStyles, List } from '@material-ui/core'
import { WebUITheme } from '../../../../interfaces/Theme'

interface AlgorithmListItemProps {
	algorithm: Algorithm
}
const { setBuildNode, updateGhostNode, setAlgorithmToolbarOpen } = Actions

const useStyles = makeStyles<WebUITheme>((theme) => ({
	root: {
		paddingBottom: 'unset',
		paddingLeft: theme.spacing(1),
		paddingRight: theme.spacing(1),
		paddingTop: theme.spacing(1),
		borderLeft: `2px solid ${theme.palette.primary.light}`,
		'& > *': {
			marginBottom: theme.spacing(1)
		}
	}
}))

export const AlgorithmListItem = (props: AlgorithmListItemProps) => {
	const { algorithm } = props
	const dispatch = useDispatch()
	const classes = useStyles()

	const handleClick = useCallback(
		(overload: Overload) =>
			batch(() => {
				dispatch(setBuildNode(true))
				dispatch(
					updateGhostNode({
						name: algorithm.name,
						templateParams: algorithm.templateParams,
						height: Math.max(overload.params.length * 25, 75),
						nodeType: 'algorithm',
						...overload,
					})
				)
				dispatch(setAlgorithmToolbarOpen(false))
			}),
		[dispatch, algorithm]
	)

	return (
		<List className={classes.root} dense>
			{algorithm.overloads.map((overload, index) => (
				<OverloadListItem
					key={index}
					overload={overload}
					onClick={handleClick}
				/>
			))}
		</List>
	)
}
