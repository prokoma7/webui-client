import React, { useState, useCallback } from 'react'
import { Collapse, List, ListItem, ListItemText } from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import { AlgorithmCategory } from 'interfaces/Algorithms'
import { makeStyles } from '@material-ui/styles'
import { WebUITheme } from '../../../../interfaces/Theme'
import { AlgorithmListItem } from './OverloadsList'

interface AlgorithmCategoryListProps {
	algorithmCategory: AlgorithmCategory
	filter: string
}

const useStyles = makeStyles<WebUITheme>((theme) => ({
	indent: {
		borderLeft: '2px',
		borderLeftColor: theme.palette.primary.light,
		borderLeftStyle: 'solid'
	},
	nestedList: {
		padding: 'unset',
		paddingLeft: theme.spacing(1)
	},
	expandIcon: {
		transition: 'transform 300ms cubic-bezier(0.4, 0, 0.2, 1)',
		transform: 'rotate(-90deg)',
		'&[data-open=true]': {
			transform: 'rotate(-180deg)'
		}
	}
}))

const filterCategory = (category: string, filter: string) =>
	category.toLocaleUpperCase().includes(filter.toLocaleUpperCase())

const filterSubcategories = (
	algorithmCategory: AlgorithmCategory,
	filter: string
): boolean =>
	algorithmCategory.subcategories.some(
		(algorithmSubcategory) =>
			filterCategory(algorithmSubcategory.categoryName, filter) ||
			filterSubcategories(algorithmSubcategory, filter)
	)

export const AlgorithmCategoryList = (props: AlgorithmCategoryListProps) => {
	const { algorithmCategory, filter } = props
	const classes = useStyles()
	const [open, setOpen] = useState<boolean>(false)

	const handleExpand = useCallback(() => setOpen(!open), [open])

	const include = filterCategory(algorithmCategory.categoryName, filter)
	const includeSubcategories = filterSubcategories(algorithmCategory, filter)

	if (!(include || includeSubcategories)) return null

	const subcategoryFilter = !include && includeSubcategories ? filter : ''

	return (
		<div className={classes.indent}>
			<ListItem button onClick={handleExpand}>
				<ExpandMoreIcon
					className={classes.expandIcon}
					data-open={open}
				/>
				<ListItemText>{algorithmCategory.categoryName}</ListItemText>
			</ListItem>
			<Collapse in={open} unmountOnExit>
				<List className={classes.nestedList}>
					{algorithmCategory.subcategories
						.valueSeq()
						.map((subcategory, index) => (
							<AlgorithmCategoryList
								key={index}
								algorithmCategory={subcategory}
								filter={subcategoryFilter}
							/>
						))
						.toArray()}
					{algorithmCategory.algorithms.map((algorithm, index) => (
						<AlgorithmListItem
							key={`a${index}`}
							algorithm={algorithm}
						/>
					))}
				</List>
			</Collapse>
		</div>
	)
}
