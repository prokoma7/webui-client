import React from 'react'
import { AlibType } from '../../../../interfaces/Algorithms'
import {
	makeStyles,
	ListItem,
	ListItemIcon,
	Typography,
	Tooltip
	} from '@material-ui/core'
import {
	Filter1Outlined,
	Filter2Outlined,
	Filter3Outlined,
	Filter4Outlined,
	Filter5Outlined,
	Filter6Outlined,
	Filter7Outlined,
	Filter8Outlined,
	Filter9Outlined,
	Filter9PlusOutlined,
	ArrowRightAltOutlined,
} from '@material-ui/icons';
import { removeTemplate } from 'utils/alt/removeTemplate'
import { isNumber } from 'utils/typeGuards'

type ParamOrder = number | 'return'

interface ParamProps {
	name?: string
	type: AlibType
	order: ParamOrder
}

function paramOrderToIcon (order: ParamOrder) {
	switch(order) {
	case 'return':
		return (<ArrowRightAltOutlined />)
	case 1:
		return (<Filter1Outlined />)
	case 2:
		return (<Filter2Outlined />)
	case 3:
		return (<Filter3Outlined />)
	case 4:
		return (<Filter4Outlined />)
	case 5:
		return (<Filter5Outlined />)
	case 6:
		return (<Filter6Outlined />)
	case 7:
		return (<Filter7Outlined />)
	case 8:
		return (<Filter8Outlined />)
	case 9:
		return (<Filter9Outlined />)
	default:
		return (<Filter9PlusOutlined />)
	}
}

const useStyles = makeStyles({
  item: {
    padding: 0
  }
})


export const Param = (props: ParamProps) => {
	const { name, type, order } = props
	const templateLessType = removeTemplate(type)
	const classes = useStyles()

	let nameBlock = name && (
		<Typography variant="body2" component="span" display="inline">{name}</Typography>
	)

	let typeBlock = (
		<Typography variant="body2" component="span" display="inline" color="secondary">{templateLessType}</Typography>
	)

	if (templateLessType !== type) {
		typeBlock = (
			<Tooltip title={type} placement="bottom-start">
				{typeBlock}
			</Tooltip>
		)
	}

	if (nameBlock) {
		typeBlock = (
			<span>
				<Typography variant="body2" component="span" display="inline">: </Typography>
				{typeBlock}
			</span>
		)
	}

	return (
		<ListItem classes={{ root: classes.item }}>
			<Tooltip title={isNumber(order) ? ("input #" + order) : "return value"} placement="bottom-start">
				<ListItemIcon>{paramOrderToIcon(order)}</ListItemIcon>
			</Tooltip>
			<div>
				{nameBlock}
				{typeBlock}
			</div>
		</ListItem>
		)
}
