import React, { useState, useCallback } from 'react'
import { useSelector } from 'react-redux'
import { ReduxState } from 'reducers'
import { Tooltip, List, ListItem, TextField, IconButton, makeStyles } from '@material-ui/core'
import { AlgorithmCategory } from 'interfaces/Algorithms'
import { AlgorithmCategoryList } from './AlgorithmCategoryList'
import { ClearOutlined } from '@material-ui/icons';

const useStyles = makeStyles({
	textField: {
		width: '100%'
	},
	root: {
		'& > div': {
			border: 'unset'
		}
	}
})

const AlgorithmList = () => {
	const classes = useStyles()
	const [filter, setFilter] = useState<string>('')

	const { algorithms } = useSelector((state: ReduxState) => ({
		algorithms: state.workerDefinitions.algorithms
	}))

	const handleFilterChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setFilter(e.target.value),
		[]
	)

	const handleKeyDown = useCallback(
		(e: React.KeyboardEvent) => e.stopPropagation(),
		[]
	)

	return (
		<List className={classes.root}>
			<ListItem>
				<TextField
					className={classes.textField}
					label="Filter algorithms"
					value={filter}
					onChange={handleFilterChange}
					onKeyDown={handleKeyDown}
				/>
				<Tooltip title="Clear filter" aria-label="delete" placement="left">
					<IconButton aria-label="delete" color="primary" onClick={() => setFilter('') }>
						<ClearOutlined />
					</IconButton>
				</Tooltip>
			</ListItem>
			{algorithms
				.valueSeq()
				.map((category: AlgorithmCategory, index: number) => (
					<AlgorithmCategoryList
						key={index}
						algorithmCategory={category}
						filter={filter}
					/>
				))
				.toArray()}
		</List>
	)
}

export default AlgorithmList
