import React from 'react'
import InputDialog from '../Dialog/InputDialog'
import StatemakerDialog from '../../Statemaker/StatemakerDialog'
import { DotDialog } from '../Dialog/DotDialog'
import { EvaluatingDialog } from '../Dialog/EvaluatingDialog'
import { Fab, makeStyles } from '@material-ui/core'
import { useEvaluate } from '../../../hooks/useEvaluate'
import { AlgorithmDrawer } from './AlgorithmDrawer'
import { WebUITheme } from '../../../interfaces/Theme'
import { AlgorithmCanvasToolbar } from './AlgorithmCanvasToolbar'
import { StringDialog } from '../Dialog/StringDialog'
import { AlgorithmHints } from '../AlgorithmHints'

const useStyles = makeStyles<WebUITheme>((theme) => ({
	root: {
		width: '0px',
	},
	fab: {
		position: 'absolute',
		bottom: theme.spacing(2),
		right: theme.spacing(2),
		'@media (max-height: 640px)': {
			right: theme.spacing(2) + 40
		}
	},
}))

const AlgorithmToolbar = () => {
	const classes = useStyles()
	const evaluate = useEvaluate()

	return (
		<div className={classes.root}>
			<AlgorithmDrawer />
			<Fab
				className={classes.fab}
				color="primary"
				variant="extended"
				onClick={evaluate}
			>
				Evaluate
			</Fab>
			<InputDialog />
			<StringDialog />
			<StatemakerDialog />
			<DotDialog />
			<EvaluatingDialog />
			<AlgorithmCanvasToolbar />
			<AlgorithmHints />
		</div>
	)
}

export default AlgorithmToolbar
