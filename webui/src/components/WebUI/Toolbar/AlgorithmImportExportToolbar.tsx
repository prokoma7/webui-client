import React from 'react'
import { Paper } from '@material-ui/core'
import {
	CloudDownload as DownloadIcon,
	ShareRounded
} from '@material-ui/icons'
import ImportButton, { ImportButtonProps } from '../../Statemaker/Toolbar/ImportButton'
import ElementButton from '../../Toolbar/ElementButton'

interface AlgorithmImportExportToolbarProps {
	onShare?: (e: React.MouseEvent) => void
	onExport?: (e: React.MouseEvent) => void
	onImport?: ImportButtonProps['onImport']
}

export const AlgorithmImportExportToolbar = (
	props: AlgorithmImportExportToolbarProps
) => (
	<Paper>
		<ElementButton
			Icon={ShareRounded}
			label="Share link"
			onAction={props.onShare}
		/>
		<ElementButton
			Icon={DownloadIcon}
			label="Export to file"
			onAction={props.onExport}
		/>
		<ImportButton onImport={props.onImport}/>
	</Paper>
)
