import React, { useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
	Dialog,
	DialogContent,
	TextField,
	DialogTitle,
	IconButton,
	makeStyles,
	Typography,
	Divider
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import { ReduxState } from '../../../reducers'
import { algorithmUIActions } from '../../../reducers/algorithmUI'

const { setStringDialogOpen } = algorithmUIActions

const useStyles = makeStyles({
	dialogPaper: {
		width: '100%',
		overflow: 'visible'
	},
	dialogTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '4px 4px 0px 24px',
	},
	textField: {
		width: '100%',
		height: '100%',
	},
})

export const StringDialog = () => {
	const dispatch = useDispatch()
	const classes = useStyles()
	const dialogClasses = useMemo(
		() => ({
			paper: classes.dialogPaper
		}),
		[classes.dialogPaper]
	)

	const { open, content, header } = useSelector((state: ReduxState) => ({
		open: state.algorithmUI.stringDialogOpen,
		content: state.algorithmUI.stringDialogContent,
		header: state.algorithmUI.stringDialogHeader,
	}))

	const handleClose = useCallback(
		() => dispatch(setStringDialogOpen(false)),
		[dispatch]
	)

	const handleKeyDown = useCallback(
		(e: React.KeyboardEvent) => e.stopPropagation(),
		[]
	)

	return (
		<Dialog
			classes={dialogClasses}
			fullWidth
			scroll="body"
			open={open}
			onClose={handleClose}
			onKeyDown={handleKeyDown}
			maxWidth="sm"
		>
			<DialogTitle disableTypography className={classes.dialogTitle}>
				<Typography variant="h6">{header}</Typography>
				<IconButton onClick={handleClose}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<Divider />
			<DialogContent>
				<TextField
					className={classes.textField}
					variant="filled"
					multiline
					value={content}
				/>
			</DialogContent>
		</Dialog>
	)
}
