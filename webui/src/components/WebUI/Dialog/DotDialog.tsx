import React, { useCallback, useRef, memo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
	Dialog,
	DialogContent,
	DialogTitle,
	Typography,
	IconButton,
	Divider,
	DialogActions,
	Button,
	ButtonGroup
} from '@material-ui/core'
import { Graphviz } from 'components/WebUI/Dialog/Graphviz'
import { makeStyles } from '@material-ui/styles'
import { Close as CloseIcon } from '@material-ui/icons'
import { useSnackbar } from 'notistack'
import { saveAs } from 'file-saver'
import { ReduxState } from '../../../reducers'
import { algorithmUIActions } from '../../../reducers/algorithmUI'

const { setDotDialogOpen } = algorithmUIActions

const useStyles = makeStyles({
	dialogTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '4px 4px 0px 24px'
	}
})

const MemoizedDotDialogContent = memo(
	({ dotContent, onError }: { dotContent: string, onError: any }) => (
		<Graphviz dot={dotContent} onError={onError}/>
	)
)

export const DotDialog = () => {
	const classes = useStyles()
	const dispatch = useDispatch()
	const { open, dotContent } = useSelector((state: ReduxState) => ({
		open: state.algorithmUI.dotDialogOpen,
		dotContent: state.algorithmUI.dotDialogContent
	}))

	const { enqueueSnackbar } = useSnackbar()
	const contentRef = useRef<HTMLDivElement>(null)

	const handleDialogClose = useCallback(() => {
		dispatch(setDotDialogOpen(false))
	}, [dispatch])

	const handleDOTDownload = useCallback(() => {
		const blob = new Blob([dotContent], {
			type: 'text/plain'
		})

		saveAs(blob, 'output.gv')
	}, [dotContent])

	const handleSVGDownload = useCallback(() => {
		const svgElement = (
			contentRef.current?.firstChild as HTMLDivElement | undefined
		)?.firstChild as SVGSVGElement | undefined

		if (!svgElement || svgElement.nodeName !== 'svg')
			return enqueueSnackbar('DOT SVG output has not been rendered', {
				variant: 'error'
			})

		const blob = new Blob([svgElement.outerHTML], {
			type: 'image/svg+xml'
		})

		saveAs(blob, 'output.svg')
	}, [contentRef, enqueueSnackbar])

    const onGraphvizRenderError = (errorMsg: string) => {
      console.error("Graphviz render error:", errorMsg)
      enqueueSnackbar("Graphviz image could not be rendered.", { variant: 'error' })
    }
    const onGraphvizRenderErrorMemo = useCallback(onGraphvizRenderError, []); // https://stackoverflow.com/questions/54368822/react-memo-components-and-re-render-when-passing-function-as-props

	return (
		<Dialog
			maxWidth="lg"
			scroll="body"
			open={open}
			onClose={handleDialogClose}
		>
			<DialogTitle disableTypography className={classes.dialogTitle}>
				<Typography variant="h6">DOT Output</Typography>
				<IconButton onClick={handleDialogClose}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<Divider />
			<DialogContent ref={contentRef}>
				<MemoizedDotDialogContent dotContent={dotContent} onError={onGraphvizRenderErrorMemo} />
			</DialogContent>
			<Divider />
			<DialogActions>
				<ButtonGroup variant="text" color="primary" aria-label="text primary button group">
					<Button color="primary" aria-controls="saveMenu" aria-haspopup="true" onClick={handleDOTDownload}>Download DOT</Button>
					<Button color="primary" aria-controls="saveMenu" aria-haspopup="true" onClick={handleSVGDownload}>Download SVG</Button>
				</ButtonGroup>
			</DialogActions>
		</Dialog>
	)
}
