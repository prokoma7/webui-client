import React, { useCallback, useState, useMemo, useLayoutEffect } from 'react'
import { useDispatch, useSelector, batch } from 'react-redux'
import {
	Dialog,
	DialogActions,
	Button,
	DialogTitle,
	IconButton,
	makeStyles,
	Typography,
	Divider,
	TextField,
	DialogContent,
	Select,
	MenuItem,
	InputLabel,
	FormControl,
	Link,
	Grid,
} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import HelpIcon from '@material-ui/icons/Help';
import { ReduxState } from '../../../reducers'
import { algorithmUIActions } from '../../../reducers/algorithmUI'
import { algorithmDataActions } from '../../../reducers/algorithmData'

const { closeInputDialog } = algorithmUIActions
const {
	setInputString,
	setInputNumber: setInputInt,
	setInputBoolean
} = algorithmDataActions

const useStyles = makeStyles({
	dialogPaper: {
		width: '100%',
		overflow: 'visible'
	},
	dialogTitle: {
		display: 'flex',
		justifyContent: 'space-between',
		alignItems: 'center',
		padding: '4px 4px 0px 24px'
	},
	textField: {
		width: '100%',
		height: '100%'
	},
	typeSelectFormControl: {
		marginBottom: '20px'
	},
	dialogContent: {
		display: 'flex',
		flexDirection: 'column'
	},
	numberTextField: {
		width: '25%'
	},
	boolSelectFormControl: {
		width: '15%'
	}
})

const InputDialog = () => {
	const dispatch = useDispatch()
	const classes = useStyles()
	const dialogClasses = useMemo(
		() => ({
			paper: classes.dialogPaper
		}),
		[classes.dialogPaper]
	)

	const [stringValue, setStringValue] = useState<string>('')
	const [numberValue, setNumberValue] = useState<string>('')
	const [boolValue, setBoolValue] = useState<boolean>(false)
	const [type, setType] = useState<'string' | 'number' | 'boolean'>('string')

	const { open, initialValue, editedNode } = useSelector(
		(state: ReduxState) => ({
			open: state.algorithmUI.inputDialog.open,
			initialValue: state.algorithmUI.inputDialog.initialValue,
			editedNode: state.algorithmData.present.nodes.get(
				state.algorithmUI.inputDialog.editedNodeId
			)
		})
	)

	useLayoutEffect(() => {
		if (!open) return

		const valueType = typeof initialValue

		switch (valueType) {
			case 'string':
				setStringValue(initialValue)
				break
			case 'number':
				setNumberValue(initialValue.toString())
				break
			case 'boolean':
				setBoolValue(initialValue)
				break
			default:
				return
		}

		setType(valueType as any)
	}, [open, initialValue])

	const handleClose = useCallback(() => {
		dispatch(closeInputDialog())
		setStringValue('')
		setNumberValue('')
		setBoolValue(false)
		setType('string')
	}, [dispatch])

	const handleApply = useCallback(
		() =>
			batch(() => {
				handleClose()

				if (!editedNode) return

				switch (type) {
					case 'string':
						dispatch(setInputString(editedNode.id, stringValue))
						break
					case 'number':
						dispatch(
							setInputInt(editedNode.id, parseFloat(numberValue))
						)
						break
					case 'boolean':
						dispatch(setInputBoolean(editedNode.id, boolValue))
						break
				}
			}),
		[
			dispatch,
			handleClose,
			editedNode,
			stringValue,
			numberValue,
			boolValue,
			type
		]
	)

	const handleTextFieldChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) =>
			setStringValue(e.currentTarget.value),
		[]
	)

	const handleNumberTextFieldChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
			const { value } = e.currentTarget
			if (
				value.match(/^-?([0-9]*|[0-9]+\.?[0-9]*)$/) ||
				(value.length < numberValue.length &&
					numberValue.startsWith(value))
			)
				setNumberValue(value)
		},
		[numberValue]
	)

	const handleBoolSelectChange = useCallback(
		(e: React.ChangeEvent<{ value: unknown }>) =>
			setBoolValue(e.target.value === 'true'),
		[]
	)

	const handleKeyDown = useCallback(
		(e: React.KeyboardEvent) => e.stopPropagation(),
		[]
	)

	const handleTypeChange = useCallback(
		(e: React.ChangeEvent<{ value: unknown }>) =>
			setType(e.target.value as typeof type),
		[type]
	)

	if (!editedNode) return null

	return (
		<Dialog
			classes={dialogClasses}
			fullWidth
			scroll="body"
			open={open}
			onClose={handleClose}
			onKeyDown={handleKeyDown}
			maxWidth="sm"
		>
			<DialogTitle disableTypography className={classes.dialogTitle}>
				<Typography variant="h6">Input editor</Typography>
				<IconButton onClick={handleClose}>
					<CloseIcon />
				</IconButton>
			</DialogTitle>
			<Divider />
			<DialogContent className={classes.dialogContent}>
			<Grid container justify="center" spacing={2}>
				<Grid item xs={6}>
					<FormControl className={classes.typeSelectFormControl}>
						<InputLabel id="typeSelectLabel">Type</InputLabel>
						<Select
							labelId="typeSelectLabel"
							value={type}
							onChange={handleTypeChange}
						>
							<MenuItem value="string">String</MenuItem>
							<MenuItem value="number">Number</MenuItem>
							<MenuItem value="boolean">Bool</MenuItem>
						</Select>
					</FormControl>
				</Grid>
				<Grid item xs={6}>
					{type === "string" && (
						<Link component="button" variant="body2" target="_blank" href="https://alt.fit.cvut.cz/docs/parse/" onClick={()=> window.open("https://alt.fit.cvut.cz/docs/parse/", "_blank")}>
							<IconButton disabled={true}><HelpIcon /></IconButton>
							String format documentation
						</Link>
						)}
				</Grid>
			</Grid>

				{type === 'string' && (
					<TextField
						className={classes.textField}
						variant="filled"
						multiline
						value={stringValue}
						onChange={handleTextFieldChange}
					/>
				)}
				{type === 'number' && (
					<TextField
						className={classes.numberTextField}
						label="Value"
						value={numberValue}
						onChange={handleNumberTextFieldChange}
					/>
				)}
				{type === 'boolean' && (
					<FormControl className={classes.boolSelectFormControl}>
						<InputLabel id="boolValueSelect">Value</InputLabel>
						<Select
							labelId="boolValueSelect"
							value={boolValue.toString()}
							onChange={handleBoolSelectChange}
						>
							<MenuItem value="false">False</MenuItem>
							<MenuItem value="true">True</MenuItem>
						</Select>
					</FormControl>
				)}
			</DialogContent>
			<DialogActions>
				<Button onClick={handleApply} color="primary">
					Done
				</Button>
			</DialogActions>
		</Dialog>
	)
}

export default InputDialog
