import React from 'react'
import { graphviz, Graphviz as IGraphviz, GraphvizOptions } from 'd3-graphviz';

interface IGraphvizProps {
  /**
   * A string containing a graph representation using the Graphviz DOT language.
   * @see https://graphviz.org/doc/info/lang.html
   */
  dot: string;
  /**
   * Options to pass to the Graphviz renderer.
   */
  options?: GraphvizOptions;
  /**
   * The classname to attach to this component for styling purposes.
   */
  className?: string;

  onError?: (errorText: string) => void;
}

// @types/d3-graphviz are old and Graphviz::destroy does not exist. Let's hack it in.
interface IGraphvizWithDestroy extends IGraphviz<any, any, any, any> {
	destroy: () => void;
}

export class Graphviz extends React.Component<IGraphvizProps> {
  private static count = 0;

  private static defaultOptions: GraphvizOptions = {
    fit: true,
    height: 650,
    width: 750,
    zoom: false,
  };

  private id: string;

  constructor(props: IGraphvizProps) {
    super(props)
    this.id = `graphviz${Graphviz.count}`
    Graphviz.count += 1
  }

  public render = (): JSX.Element => {
    const { className } = this.props;
    return (<div className={className} id={this.id} />)
  };

  public componentDidMount = () => {
    this.renderGraph();
  };

  public componentDidUpdate = () => {
    this.renderGraph();
  };

  public componentWillUnmount = () => {
    (graphviz(`#${this.id}`) as IGraphvizWithDestroy).destroy()
  };

  private renderGraph = () => {
    const { dot, options } = this.props;
    graphviz(`#${this.id}`)
      .options({
        ...Graphviz.defaultOptions,
        ...options || {},
      })
      .renderDot(dot)
      .onerror((errorMsg) => {
        if (this.props.onError) {
          this.props.onError(errorMsg)
        }
      })
  };
}
