import React from 'react'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/styles'
import { ReduxState } from '../../reducers'
import { Hints } from '../Hints'

const hints: string[][] = [
  ['Open/close sidebar', 'A'],
  ['Evaluate', 'E'],
	['Delete selected', 'Del'],
	['Select mode', 'L'],
	['Move mode', 'M'],
	['Toggle grid', 'G'],
	['Undo', 'Ctrl + Z'],
	['Redo', 'Ctrl + Y'],
	['Clear canvas', 'Ctrl + X'],
	['Export', 'Ctrl + S'],
	['Import', 'Ctrl + O'],
	['Toggle hint', 'H']
]

const useStyles = makeStyles({
	root: {
		position: 'absolute',
		bottom: '70px',
		right: 0,
		zIndex: 8,
		pointerEvents: 'none',
		marginRight: 8,
		marginBottom: 8,
		textAlign: 'right',
		'@media (max-height: 935px)': {
				right: 48
		}
	}
})

export const AlgorithmHints = () => {
	const classes = useStyles()
	const { showHint } = useSelector((state: ReduxState) => ({
		showHint: state.algorithmUI.showHint
	}))

	if (!showHint) return null

	return (
		<div className={classes.root}>
			<Hints hints={hints} />
		</div>
	)
}
