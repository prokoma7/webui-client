import * as React from 'react'
import { useDispatch, useSelector } from 'react-redux'

import useAlgorithmPosition from '../../../hooks/useAlgorithmPosition'
import { ReduxState } from '../../../reducers'
import {
	outputParamNodePosition,
	inputParamNodePosition
} from '../../../utils/positioning'
// import { hasTransitionError } from '../../../utils/errors'
import Edge from './Edge'
import { algorithmCanvasActions } from '../../../reducers/algorithmCanvas'
import { add } from '../../../interfaces/Position'
import { paramNodeRadius } from './ParamNode'
import AlgorithmNode, { emptyNode } from '../../../interfaces/AlgorithmNode'

interface Props {
	id: string
	canvasRef: React.RefObject<SVGSVGElement>
}

const { selectEdge } = algorithmCanvasActions

/**
 * Selects attributes from state
 *
 * @param {string} id Id of the edge
 */
const edgeSelector = (id: string) => (state: ReduxState) => {
	const nodes = state.algorithmData.present.nodes
	const edge = state.algorithmData.present.edges.get(id)
	if (!edge) return null // Transition with id not found
	const { startNodeId, endNodeId } = edge

	// State is being positioned
	const positioningNode = state.algorithmCanvas.positioningNode
	const useLocalMove = [startNodeId, endNodeId].includes(positioningNode)

	return {
		data: edge,
		isSelected: state.algorithmCanvas.selectedEdge === id,
		startNode: { ...nodes.get(startNodeId, emptyNode) } as AlgorithmNode,
		endNode: { ...nodes.get(endNodeId, emptyNode) } as AlgorithmNode,
		hasError:
			state.algorithmData.present.cycleEdges.contains(id) ||
			state.algorithmData.present.incompatibleEdges.some(edge => edge.id === id),
		cursorMode: state.algorithmCanvas.cursorMode,
		positioningNode: state.algorithmCanvas.positioningNode,
		mouseDownOffset: state.algorithmCanvas.mouseDownOffset,
		useLocalMove,
		gridOn: state.algorithmCanvas.gridOn
	}
}

const EdgeContainer = (props: Props) => {
	const { id, canvasRef } = props
	const dispatch = useDispatch()
	const state = useSelector(edgeSelector(id))

	const position = useAlgorithmPosition(
		canvasRef,
		state?.useLocalMove ?? true,
	)

	const tId = state ? state.data.id : ''
	const cursorMode = state ? state.cursorMode : 'select'
	const handleClick = React.useCallback(
		(e: React.MouseEvent<SVGGElement>) => {
			e.nativeEvent.stopImmediatePropagation()
			// Disallow if mode not select
			if (cursorMode !== 'select') return
			// Select edge
			dispatch(selectEdge(tId))
		},
		[dispatch, tId, cursorMode]
	)

	if (!state) return null

	const {
		data,
		startNode,
		endNode,
		isSelected,
		hasError,
		positioningNode,
		useLocalMove,
		mouseDownOffset,
		gridOn
	} = state
	// Return if start or end states are missing
	if (!startNode || !endNode) return null

	// Use local position if start or end nodes are being positioned
	if (useLocalMove) {
		if (startNode.id === positioningNode) {
			startNode.position = gridOn ? position : add(position, mouseDownOffset)
		} else {
			endNode.position = gridOn ? position : add(position, mouseDownOffset)
		}
	}

	const fromPoint = outputParamNodePosition(startNode)
	fromPoint.x += paramNodeRadius

	const toPoint = inputParamNodePosition(endNode, data.endParamIndex)
	toPoint.x -= paramNodeRadius

	return (
		<Edge
			data={data}
			fromPoint={fromPoint}
			toPoint={toPoint}
			isSelected={isSelected}
			hasError={hasError}
			onClick={handleClick}
		/>
	)
}

export default EdgeContainer
