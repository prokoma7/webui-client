import * as React from 'react'
import { batch, useDispatch, useSelector } from 'react-redux'

import useAlgorithmPosition from '../../../hooks/useAlgorithmPosition'
import useTextSize from '../../../hooks/useTextSize'

import { Actions, ReduxState } from '../../../reducers'
import Node from './Node'
import { isOutputNode } from '../../../interfaces/AlgorithmNode'
import { v4 as uuidv4 } from 'uuid'

const {
	addNode,
	setBuildNode,
	updateGhostNode,
	set,
	setStatemakerOpen,
	selectNode,
	openInputDialog
} = Actions

interface GhostNodeProps {
	canvasRef: React.RefObject<SVGSVGElement>
}

/**
 * Ghost state shown when drawing new state on canvas
 * Uses mouse cursor for position
 *
 * @returns
 */
function GhostNodeContainer(props: GhostNodeProps) {
	const { canvasRef } = props
	const dispatch = useDispatch()

	const { show, data } = useSelector((state: ReduxState) => ({
		show: state.algorithmCanvas.buildNode,
		data: state.algorithmUI.ghostNode,
	}))

	const position = useAlgorithmPosition(canvasRef, show)

	const handleTextChange = React.useCallback(
		(size: number) => dispatch(updateGhostNode({ width: size })),
		[dispatch]
	)

	const textRef = useTextSize(data.name, handleTextChange)

	// Create new node
	const handleMouseUp = React.useCallback(() => {
		batch(() => {
			const id = uuidv4()
			dispatch(
				addNode({
					id,
					position,
					name: data.name,
					width: data.width,
					height: data.height,
					params: data.params,
					templateParams: data.templateParams,
					resultType: data.resultType,
					nodeType: data.nodeType,
					value: data.value
				})
			)
			dispatch(setBuildNode(false))
			dispatch(selectNode(id))
			switch (data.nodeType) {
				case 'inputAutomaton':
					dispatch(
						set({
							states: {},
							transitions: {},
							initialStates: [],
							finalStates: [],
						})
					)
					dispatch(setStatemakerOpen(true, true))
					break
				case 'inputString':
					dispatch(openInputDialog(id))
					break
			}
		})
	}, [dispatch, position, data])

	if (!show) return null

	return (
		<Node
			ref={textRef}
			data={{ ...data, position }}
			isGhost={true}
			onMouseUp={handleMouseUp}
			hasOutput={!isOutputNode(data.nodeType)}
		/>
	)
}

export default GhostNodeContainer
