import * as React from 'react'

import { Theme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/styles'
import { Visibility as VisibilityIcon, Edit as EditIcon } from '@material-ui/icons'
import AlgorithmNode from '../../../interfaces/AlgorithmNode'
import ParamNode from './ParamNodeContainer'
import {
	inputParamNodePosition,
	outputParamNodePosition
} from '../../../utils/positioning'
import { SVGButton } from '../SVGButton'

interface Props {
	/** State element data */
	data: AlgorithmNode
	isSelected?: boolean
	isGhost?: boolean
	hasError?: boolean
	hasValue?: boolean
	/** Some transition from or to this state is selected */
	hasTransitionSelected?: boolean
	showEditButton?: boolean
	hasOutput?: boolean
	onClick?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onMouseUp?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onMouseDown?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onMouseMove?: (e: React.MouseEvent<SVGRectElement, MouseEvent>) => void
	onEdit?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
	onView?: (e: React.MouseEvent<SVGGElement, MouseEvent>) => void
}

const noneString: 'none' = 'none'
const highlightColor = '#0e0'
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		cursor: 'pointer',
		'&[data-ghost="true"]': {
			opacity: 0.4
		},
		'&:hover > rect[data-highlight="false"]': {
			stroke: theme.palette.primary.light
		},
		'&:hover > g': {
			display: 'block'
		}
	},
	node: {
		strokeWidth: 2,
		stroke: theme.palette.grey[400],
		fill: theme.palette.common.white,
		rx: 2,
		ry: 2,
		'&[data-error="true"]': {
			stroke: theme.palette.error.main
		},
		'&[data-selected="true"]': {
			stroke: theme.palette.primary.main
		},
		'&[data-transition-selected="true"]': {
			stroke: theme.palette.secondary.main
		},
		'&[data-highlight="true"]': {
			stroke: highlightColor
		}
	},
	text: {
		fill: theme.palette.text.primary,
		fontFamily: 'Roboto',
		fontSize: 20,
		pointerEvents: noneString
	},
	editButton: {
		display: 'none',
		'&:hover rect': {
			stroke: theme.palette.primary.light
		}
	},
	editButtonRect: {
		fill: theme.palette.common.white,
		stroke: theme.palette.grey[400],
		strokeWidth: 1
	},
	viewButton: {
		'& > rect': { stroke: highlightColor },
		'&:hover > rect': { stroke: highlightColor }
	}
}))

/**
 * Represents the state element in state machine
 *
 * @param {Props} props
 * @param {React.Ref<SVGTextElement>} textRef Reference to the name of state used to calculate the state size
 * @returns
 */
function Node(props: Props, textRef: React.Ref<SVGTextElement>) {
	const {
		data,
		hasError,
		hasValue,
		hasTransitionSelected,
		isGhost,
		isSelected,
		showEditButton,
		hasOutput,
		onClick,
		onMouseDown,
		onMouseMove,
		onMouseUp,
		onEdit,
		onView
	} = props
	const classes = useStyles()

	const id = 'node_' + data.id

	const { height, width } = data
	const x = data.position.x - data.width / 2
	const y = data.position.y - data.height / 2

	const [name] = data.name.split('::').reverse()

	return (
		<g id={id} className={classes.root} data-ghost={isGhost}>
			<rect
				className={classes.node}
				height={height}
				width={width}
				x={x}
				y={y}
				onClick={onClick}
				onMouseUp={onMouseUp}
				onMouseDown={onMouseDown}
				onMouseMove={onMouseMove}
				data-error={hasError}
				data-selected={isSelected}
				data-transition-selected={hasTransitionSelected}
				data-highlight={hasValue}
			/>
			<text
				className={classes.text}
				ref={textRef}
				alignmentBaseline="middle"
				textAnchor="middle"
				transform={`translate(${data.position.x},${data.position.y})`}
			>
				<tspan x={0} y={6}>
					{name}
				</tspan>
			</text>
			{showEditButton && (
				<SVGButton
					x={data.position.x}
					y={y}
					onClick={onEdit}
					icon={EditIcon}
				/>
			)}
			{hasValue && (
				<SVGButton
					className={classes.viewButton}
					x={data.position.x}
					y={y}
					onClick={onView}
					icon={VisibilityIcon}
				/>
			)}
			{hasOutput && (
				<ParamNode
					nodeId={data.id}
					type={data.resultType}
					{...outputParamNodePosition(data)}
				/>
			)}
			{data.params.map((param, index) => (
				<ParamNode
					key={index}
					paramIndex={index}
					nodeId={data.id}
					{...param}
					{...inputParamNodePosition(data, index)}
				/>
			))}
		</g>
	)
}

export default React.memo(React.forwardRef(Node), (prevProps, nextProps) => {
	if (prevProps.data.name !== nextProps.data.name) {
		return false
	}
	if (prevProps.data.position.x !== nextProps.data.position.x) return false
	if (prevProps.data.position.y !== nextProps.data.position.y) return false
	if (prevProps.data.width !== nextProps.data.width) return false
	if (prevProps.isSelected !== nextProps.isSelected) return false
	if (prevProps.isGhost !== nextProps.isGhost) return false
	if (prevProps.hasError !== nextProps.hasError) return false
	if (prevProps.onClick !== nextProps.onClick) return false
	if (prevProps.onMouseDown !== nextProps.onMouseDown) return false
	if (prevProps.onMouseMove !== nextProps.onMouseMove) return false
	if (prevProps.onMouseUp !== nextProps.onMouseUp) return false
	if (prevProps.hasTransitionSelected !== nextProps.hasTransitionSelected)
		return false
	if (prevProps.onEdit !== nextProps.onEdit) return false
	if (prevProps.onView !== nextProps.onView) return false
	if (prevProps.showEditButton !== nextProps.showEditButton) return false
	if (prevProps.hasValue !== nextProps.hasValue) return false
	if (prevProps.hasOutput !== nextProps.hasOutput) return false
	return true
})
