import { useLayoutEffect, memo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ReduxState } from 'reducers'
import { algorithmGraphFromJSON } from '../../interfaces/AlgorithmGraph'
import { algorithmDataActions } from '../../reducers/algorithmData'
import { useSnackbar } from 'notistack'
import { Base64 } from 'js-base64';

const { importGraph } = algorithmDataActions

export const QueryImporter = memo(() => {
	const dispatch = useDispatch()
	const { enqueueSnackbar } = useSnackbar()
	const { castData } = useSelector((state: ReduxState) => { return { castData: state.workerDefinitions.casts }})

  // eslint-disable-next-line
	useLayoutEffect(() => {
		// don't load until castData is initialized
		if (Object.entries(castData).length === 0)
			return;

		const urlParams = new URLSearchParams(window.location.search)
		const importUrl = urlParams.get('importUrl')
		const hash = window.location.hash.substr(1)

		if (importUrl && hash) {
			enqueueSnackbar('Error while importing project, cannot import from multiple sources', { variant: 'error' })
			return
		}

		if (importUrl) {
			fetch(importUrl)
			.then((res) => res.text())
			.then((json) => algorithmGraphFromJSON(json))
			.then((algorithmGraph) => dispatch(importGraph(algorithmGraph, castData)))
			.catch((err) => {
				console.error(err)
				enqueueSnackbar('Error while importing project', { variant: 'error' })
				})
		}

		if (hash) {
			try {
				const decoded = Base64.decode(hash)
				const graph = algorithmGraphFromJSON(decoded);
				dispatch(importGraph(graph, castData));
			} catch (err) {
				console.log(err)
				enqueueSnackbar('Error while importing project', { variant: 'error' })
			}

		}
      // eslint-disable-next-line
	}, [castData])

	return null
})
