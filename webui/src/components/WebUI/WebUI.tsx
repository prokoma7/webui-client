import React, { useCallback, useEffect } from 'react'
import {
	makeStyles,
	AppBar,
	Toolbar,
	IconButton,
	Grid,
} from '@material-ui/core'
import useFetch from 'react-fetch-hook'
import { useSnackbar } from 'notistack'
import {
	ArrowForwardIos as ArrowRightIcon,
	ArrowBackIos as ArrowLeftIcon,
} from '@material-ui/icons'
import AlgorithmCanvas from '../AlgorithmCanvas'
import AlgorithmToolbar from './Toolbar/AlgorithmToolbar'
import { WorkerDefinitionsResponse } from 'interfaces/WorkerDefinitionsResponse'
import { useDispatch, useSelector } from 'react-redux'
import { ReduxState } from 'reducers'
import { algorithmUIActions } from 'reducers/algorithmUI'
import { workerDefinitionsActions } from 'reducers/workerDefinitions'
import { WebUITheme } from '../../interfaces/Theme'
import { useAlgorithmKeybinds } from '../../hooks/useAlgorithmKeybinds'
import { QueryImporter } from './QueryImporter'
import { WebUIAboutBox } from './WebUIAboutBox'
import { Loading } from 'components/Loading'

const { setAlgorithmToolbarOpen } = algorithmUIActions
const { setData } = workerDefinitionsActions

// styles based on https://material-ui.com/components/drawers/#PersistentDrawerLeft.tsx
const useStyles = makeStyles<WebUITheme>((theme) => ({
	root: {
		display: 'flex',
		height: '100%',
		width: '100%'
	},
	appBar: {
		transition: theme.transitions.create(['margin', 'width'], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		}),
		'&[data-shift="true"]': {
			width: `calc(100% - ${theme.drawerWidth})`,
			marginLeft: theme.drawerWidth,
			transition: theme.transitions.create(['margin', 'width'], {
				easing: theme.transitions.easing.easeOut,
				duration: theme.transitions.duration.enteringScreen
			})
		}
	},
	toolbar: {
		paddingLeft: '0px'
	},
	popover: {
      pointerEvents: 'none',
    },
}))

export const WebUI = () => {
	const classes = useStyles()
	const dispatch = useDispatch()
	const { toolbarOpen } = useSelector((state: ReduxState) => ({
		toolbarOpen: state.algorithmUI.toolbarOpen
	}))
	const { enqueueSnackbar } = useSnackbar();
	const { isLoading, error, data } = useFetch(process.env.REACT_APP_API_PREFIX + "/definitions");
	const fetchDone = data !== undefined && !isLoading

	if (error) {
		console.error(error);
		enqueueSnackbar("Error fetching algorithm list", { variant: 'error' })
	}

	useEffect(() => {
		if (fetchDone) {
			let d = data as WorkerDefinitionsResponse
			dispatch(setData(d.algorithms, d.casts, d.version))
		}
	}, [dispatch, fetchDone, data])

	const algorithmKeyEventsHandler = useAlgorithmKeybinds()

	const handleToolbarButtonClick = useCallback(() => {
		dispatch(setAlgorithmToolbarOpen(!toolbarOpen))
	}, [dispatch, toolbarOpen])

	return (
		<div
			className={classes.root}
			tabIndex={0}
			{...algorithmKeyEventsHandler}
		>
			<AppBar className={classes.appBar} data-shift={toolbarOpen}>
				<Toolbar className={classes.toolbar} variant="dense">
					<Grid container justify="space-between">
						<IconButton
							color="inherit"
							onClick={handleToolbarButtonClick}
						>
							{toolbarOpen ? <ArrowLeftIcon /> : <ArrowRightIcon />}
						</IconButton>
						<WebUIAboutBox />
					</Grid>
				</Toolbar>
			</AppBar>
			<AlgorithmToolbar />
			<AlgorithmCanvas />
			<QueryImporter />
			<Loading open={!fetchDone} />
		</div>
	)
}
