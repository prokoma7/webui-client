import React from 'react'
import { makeStyles } from '@material-ui/core'

interface HintsProps {
	hints: string[][]
}

const useStyles = makeStyles({
	root: {
		display: 'grid',
		gridTemplateColumns: 'auto minmax(0, 1fr)',
		justifyItems: 'start',
		gridGap: '4px 32px',
		fontSize: '1rem',
		lineHeight: '1rem',
		marginBottom: 8,
		'&:last-child': {
			marginBottom: 0
		}
	},
	name: {
		color: 'rgba(0,0,0,0.2)',
		whiteSpace: 'pre-line',
		alignSelf: 'center'
	},
	code: {
		justifySelf: 'right',
		'& > div': {
			display: 'inline-block',
			minWidth: 40,
			textAlign: 'center',
			color: 'rgba(255,255,255)',
			backgroundColor: 'rgba(0,0,0,0.2)',
			padding: '2px 8px',
			borderRadius: 4
		}
	}
})

export const Hints = ({ hints }: HintsProps) => {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			{hints.map((row: string[], index: number) => {
				const [name, code] = row

				return (
					<React.Fragment key={index}>
						<div className={classes.name}>{name}</div>
						{code && (
							<div className={classes.code}>
								<div>{code}</div>
							</div>
						)}
					</React.Fragment>
				)
			})}
		</div>
	)
}
