import React from 'react'
import { List, ListItem, ListItemText, ListItemIcon } from '@material-ui/core'
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import { EvaluateResponseError } from '../interfaces/EvaluateResponseError'

export const createEvalErrorMessage = ( e: EvaluateResponseError) => {
	return (
			<List dense={true}>
			{e.errorStack.map((errorMsg, idx) => (
						<ListItem>
						{(idx > 0) && (
								<ListItemIcon>
								<SubdirectoryArrowRightIcon />
								</ListItemIcon>
								)
						}
						<ListItemText primary={errorMsg} />
						</ListItem>
						))}
			</List>
		   )
}
