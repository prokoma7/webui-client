import * as React from 'react'
import { makeStyles } from '@material-ui/styles'

import Position, { divideByX, round } from '../interfaces/Position'

export type CanvasNode = {
	id: string
}

export type CanvasEdge = {
	id: string
}

interface CanvasProps extends React.HTMLProps<SVGSVGElement> {
	id?: string
	showGrid: boolean
	offset: Position
	scale: number
	onMouseUp?: (e: React.MouseEvent<SVGSVGElement>) => any
}

const useStyles = makeStyles({
	root: {
		height: '100%',
		width: '100%',
		userSelect: 'none'
	},
	grid: {
		display: 'none',
		'&[data-show="true"]': {
			display: 'block'
		}
	},
	gridPattern: {
		'& path': {
			stroke: '#bdbdbd',
			fill: 'none'
		},
		'& #smallGridPattern path': {
			strokeWidth: 0.5
		},
		'& #gridPattern path': {
			strokeWidth: 1
		}
	}
})

/**
 * SVG canvas where the state machine is being built and showed
 *
 * @returns
 */
const Canvas = React.forwardRef<SVGSVGElement, CanvasProps>(
	(props, ref) => {
		const { id, showGrid, offset, scale, onMouseUp } = props

		const classes = useStyles()

		const gridWidth = document.body.clientWidth * 100 * -0.5
		const gridHeight = document.body.clientHeight * 100 * -0.5
		const gridTransform = `translate(${gridWidth}, ${gridHeight})`

		// Transform the svg using the zoom scale and offset
		const scaledOffset = round(divideByX(offset, scale))
		const translate = `translate(${scaledOffset.x}, ${scaledOffset.y})`
		const transform = `scale(${scale}), ${translate}`

		return (
			<svg id={id} ref={ref} className={classes.root} onMouseUp={onMouseUp}>
				<g transform={transform}>
					<defs className={classes.gridPattern}>
						<pattern
							id="smallGridPattern"
							width="25"
							height="25"
							patternUnits="userSpaceOnUse"
						>
							<path d="M 25 0 L 0 0 0 25" />
						</pattern>
						<pattern
							id="gridPattern"
							width="250"
							height="250"
							patternUnits="userSpaceOnUse"
						>
							<rect
								width="250"
								height="250"
								fill="url(#smallGridPattern)"
							/>
							<path d="M 250 0 L 0 0 0 250" />
						</pattern>
					</defs>
					<rect
						className={classes.grid}
						data-show={showGrid}
						transform={gridTransform}
						width="10000%"
						height="10000%"
						fill="url(#gridPattern)"
					/>
					{props.children}
				</g>
			</svg>
		)
	}
)

export default Canvas
