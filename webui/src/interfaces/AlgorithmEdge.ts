import { isString, isNumber } from '../utils/typeGuards'

export default interface AlgorithmEdge {
	id: string
	startNodeId: string
	endNodeId: string
	/** Index of end param in the end node */
	endParamIndex: number
}

export const isAlgorithmEdge = (edge: any): edge is AlgorithmEdge => {
	if (!isString(edge.id)) return false
	if (!isString(edge.startNodeId)) return false
	if (!isString(edge.endNodeId)) return false
	if (!isNumber(edge.endParamIndex)) return false

	return true
}
