import { Map } from 'immutable'
import AlgorithmNode, { isAlgorithmNode } from './AlgorithmNode'
import AlgorithmEdge, { isAlgorithmEdge } from './AlgorithmEdge'
import { isObject } from '../utils/typeGuards'
import { emptyAutomaton } from './StateData'
import exportData from '../utils/export/txt-fit'
import { v4 as uuidv4 } from 'uuid'

export interface AlgorithmGraph {
	nodes: Map<string, AlgorithmNode>
	edges: Map<string, AlgorithmEdge>
	outputValues: Map<string, string>
}

export const algorithmGraphFromJSON = (jsonString: string): AlgorithmGraph => {
	const json = JSON.parse(jsonString)

	if (
		!json.nodes ||
		!json.edges ||
		!isObject(json.nodes) ||
		!isObject(json.edges)
	)
		throw new Error('Invalid JSON structure')

	if (!Object.values(json.nodes).every((node: any) => isAlgorithmNode(node)))
		throw new Error('Invalid node structure')

	if (!Object.values(json.edges).every((edge: any) => isAlgorithmEdge(edge)))
		throw new Error('Invalid edge structure')

	return {
		nodes: Map<string, AlgorithmNode>(json.nodes),
		edges: Map<string, AlgorithmEdge>(json.edges),
		outputValues: Map<string, string>()
	}
}

const createEdge = (
	from: string,
	to: string,
	edges: Map<string, AlgorithmEdge>
) => {
	const edgeId = uuidv4()

	return edges.set(edgeId, {
		id: edgeId,
		startNodeId: from,
		endNodeId: to,
		endParamIndex: 0
	})
}

const inputMaps = (nodes: AlgorithmGraph['nodes']) => ({
	strings: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputString' &&
				typeof node.value === 'string'
		)
		.map((node) => ({ value: node.value })),

	ints: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputInt' &&
				typeof node.value === 'number' &&
				Number.isInteger(node.value)
		)
		.map((node) => ({ value: node.value })),

	doubles: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputDouble' &&
				typeof node.value === 'number'
		)
		.map((node) => ({ value: node.value })),

	bools: nodes
		.filter(
			(node) =>
				node.nodeType === 'inputBoolean' &&
				typeof node.value === 'boolean'
		)
		.map((node) => ({ value: node.value }))
})

const reassignEdgeEnds = (
	edges: AlgorithmGraph['edges'],
	from: string,
	to: string
) =>
	edges.map((edge) =>
		edge.endNodeId === from ? { ...edge, endNodeId: to } : edge
	)

export const serialize = (graph: AlgorithmGraph) => {
	let edges = graph.edges

	const nodesSeq = graph.nodes.toSeq()

	let nodes = graph.nodes
	const inputs = inputMaps(graph.nodes)
	let algorithmNodes = nodes.filter((node) => node.nodeType === 'algorithm')
	const outputs: string[] = nodes
		.valueSeq()
		.filter((node) => node.nodeType === 'output')
		.map((node) => node.id)
		.toArray()

	nodesSeq
		.filter((node) => node.nodeType === 'inputAutomaton')
		.forEach((node) => {
			algorithmNodes = algorithmNodes.set(node.id, {
				...node,
				name: 'string::Parse',
				templateParams: ['automaton::Automaton']
			})

			const inputId = uuidv4()
			inputs.strings = inputs.strings.set(inputId, {
				value: exportData(
					typeof node.value === 'object' ? node.value : emptyAutomaton
				)
			})

			edges = createEdge(inputId, node.id, edges)
		})

	nodesSeq
		.filter((node) => node.nodeType === 'outputAutomaton')
		.forEach((node) => {
			const algorithmId = uuidv4()
			algorithmNodes = algorithmNodes.set(algorithmId, {
				...node,
				id: algorithmId,
				name: 'string::Compose'
			})

			outputs.push(node.id)
			edges = reassignEdgeEnds(edges, node.id, algorithmId)
			edges = createEdge(algorithmId, node.id, edges)
		})

	nodesSeq
		.filter((node) => node.nodeType === 'outputDot')
		.forEach((node) => {
			const algorithmId = uuidv4()
			algorithmNodes = algorithmNodes.set(algorithmId, {
				...node,
				id: algorithmId,
				name: 'convert::DotConverter'
			})

			outputs.push(node.id)
			edges = reassignEdgeEnds(edges, node.id, algorithmId)
			edges = createEdge(algorithmId, node.id, edges)
		})

	return {
		inputs: {
			bools: inputs.bools.toJS(),
			ints: inputs.ints.toJS(),
			doubles: inputs.doubles.toJS(),
			strings: inputs.strings.toJS()
		},
		algorithms: algorithmNodes
			.map(({ name, templateParams }) => ({ name, templateParams }))
			.toJS(),
		outputs,
		pipes: edges
			.valueSeq()
			.map((edge) => ({
				from: edge.startNodeId,
				to: edge.endNodeId,
				paramIndex: edge.endParamIndex
			}))
			.toArray()
	}
}
