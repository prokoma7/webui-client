import { classifyAutomaton } from '../AutomatonTypes'
import StateData from '../StateData'

const DFA: StateData = {
	states: {
		1: {
			id: '1',
			name: '1',
			position: { x: 0, y: 0 },
			size: 0
		},
		2: {
			id: '2',
			name: '2',
			position: { x: 0, y: 0 },
			size: 0
		}
	},
	transitions: {
		'1': {
			id: '1',
			startState: '1',
			endState: '2',
			name: 'a'
		}
	},
	initialStates: ['1'],
	finalStates: ['2']
}

test('Classify DFA', () => {
	expect(classifyAutomaton(DFA)).toBe('DFA')
})

const NFA: StateData = JSON.parse(JSON.stringify(DFA))
NFA.transitions = {
	...NFA.transitions,
	2: {
		id: '2',
		startState: '1',
		endState: '1',
		name: 'a'
	}
}

test('Classify NFA', () => {
	expect(classifyAutomaton(NFA)).toBe('NFA')
})

const ENFA: StateData = JSON.parse(JSON.stringify(DFA))
ENFA.transitions['3'] = { id: '3', startState: '1', endState: '2', name: '#E' }

test('Classify ENFA', () => {
	expect(classifyAutomaton(ENFA)).toBe('ENFA')
})

const MISNFA: StateData = JSON.parse(JSON.stringify(NFA))
MISNFA.initialStates.push('2')

test('Classify MISNFA', () => {
	expect(classifyAutomaton(MISNFA)).toBe('MISNFA')
})

const MISENFA: StateData = JSON.parse(JSON.stringify(MISNFA))
MISENFA.transitions[3] = {
	id: '3',
	startState: '1',
	endState: '2',
	name: '#E'
}

test('Classify MISENFA', () => {
	expect(classifyAutomaton(MISENFA)).toBe('MISENFA')
})
