import StateData from './StateData'

/** Import interface */
export type ImportFn = (data: string) => StateData
/** Export interface */
export type ExportFn = (data: StateData) => string
/** Positioning interface */
export type PositionFn = (data: StateData) => StateData

type ValidateArg<T> = T extends StateData ? StateData : any
/** Validating interface */
export type ValidateFn<T> = (data: ValidateArg<T>) => string[]
