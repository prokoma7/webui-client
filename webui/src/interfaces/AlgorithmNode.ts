import { AlgorithmParam, AlibType, AlibTemplateType } from './Algorithms'
import Position from './Position'
import StateData from './StateData'
import { isString, isNumber } from '../utils/typeGuards'

export const inputNodeTypes = [
	'inputString',
	'inputAutomaton',
	'inputInt',
	'inputBoolean',
	'inputDouble'
] as const

export type InputNodeType = typeof inputNodeTypes[number]

export const outputNodeTypes = [
	'output',
	'outputDot',
	'outputAutomaton'
] as const

export const algorithmNodeTypes = [
	'algorithm',
	...inputNodeTypes,
	...outputNodeTypes
] as const

export type OutputNodeType = typeof outputNodeTypes[number]

export type AlgorithmNodeType = typeof algorithmNodeTypes[number]

export const isInputNode = (
	nodeType: AlgorithmNodeType
): nodeType is InputNodeType =>
	inputNodeTypes.includes(nodeType as InputNodeType)

export const isOutputNode = (
	nodeType: AlgorithmNodeType
): nodeType is OutputNodeType =>
	outputNodeTypes.includes(nodeType as OutputNodeType)

export type AlgorithmNodeValue = string | number | boolean | StateData

export default interface AlgorithmNode {
	id: string
	nodeType: AlgorithmNodeType
	name: string
	position: Position
	height: number
	width: number
	params: AlgorithmParam[]
	templateParams: AlibTemplateType[]
	resultType: AlibType
	value?: AlgorithmNodeValue
}

export const emptyNode : AlgorithmNode = {
	id: '',
	nodeType: 'algorithm',
	name: '',
	position: {x: 0, y: 0},
	height: 0,
	width: 0,
	params: [],
	templateParams: [],
	resultType: ''
}

export const isAlgorithmNode = (node: any): node is AlgorithmNode => {
	if (!isString(node.id)) return false
	if (!algorithmNodeTypes.includes(node.nodeType)) return false
	if (!isString(node.name)) return false
	if (!isNumber(node?.position?.x)) return false
	if (!isNumber(node?.position?.y)) return false
	if (!isNumber(node?.height)) return false
	if (!isNumber(node?.width)) return false
	if (
		!Array.isArray(node.templateParams) ||
		!node.templateParams.every((template: any) => isString(template))
	)
		return false
	if (!isString(node.resultType)) return false
	if (Array.isArray(node.value)) return false

	return true
}
