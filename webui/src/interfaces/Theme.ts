import { createMuiTheme, Theme } from '@material-ui/core'

export interface WebUITheme extends Theme {
	drawerWidth: string
}

export const theme: WebUITheme = { ...createMuiTheme(), drawerWidth: '500px' }