/** All allowed file variants for import and export */
type FileVariant = 'json' | 'json-xstate' | 'txt' | 'txt-fit'
export default FileVariant
