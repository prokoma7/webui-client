export interface EvaluateResponse {
  error?: string[],
  outputs?: {
    [outputId: string]: string
  }
}
