import { Map } from 'immutable'

export type AlibType = string
export type AlibTemplateType = string

export interface AlgorithmParam {
  name: string
  type: AlibType
}

export interface Overload {
  params: AlgorithmParam[]
  resultType: AlibType,
  docs?: string
}

export interface Algorithm {
  name: string
  templateParams: AlibTemplateType[]
  overloads: Overload[]
}

export interface AlgorithmCategory {
	categoryName: string
	subcategories: Map<string, AlgorithmCategory>
	algorithms: Algorithm[]
	tags: string[]
}
