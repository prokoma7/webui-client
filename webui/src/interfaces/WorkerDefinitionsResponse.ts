import { Algorithm } from 'interfaces/Algorithms'
import { Casts } from 'interfaces/Casts'

export interface WorkerDefinitionsResponse {
	algorithms: Algorithm[]
	casts: Casts
	version: string
}
