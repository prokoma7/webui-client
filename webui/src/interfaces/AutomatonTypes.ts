import { Set } from 'immutable'
import { AlibType } from './Algorithms'
import StateData from './StateData'

export const automatonTypes = [
	'DFA',
	'NFA',
	'ENFA',
	'MISNFA',
	'MISENFA'
] as const
export type AutomatonType = typeof automatonTypes[number]

type AutomatonAlibTypeMap = {
	[key: string]: AlibType
}

export const automanAlibTypeMap: AutomatonAlibTypeMap = {
	DFA: 'automaton::DFA<object::Object, object::Object>',
	NFA: 'automaton::NFA<object::Object, object::Object>',
	ENFA: 'automaton::EpsilonNFA<object::Object, object::Object>',
	MISNFA: 'automaton::MultiInitialStateNFA<object::Object, object::Object>',
	MISENFA: 'automaton::MultiInitialStateEpsilonNFA<object::Object, object::Object>'
} as const

export const classifyAutomaton = (data: StateData): AutomatonType => {
	// check if there is more than one initial state
	if (
		data.initialStates.length > 1 &&
		data.initialStates.every((stateId) => stateId in data.states)
	)
		return Object.values(data.transitions).some(
			(transition) => transition.name === '#E'
		)
			? 'MISENFA'
			: 'MISNFA'
	// check if some transition has epsilon value
	if (
		Object.values(data.transitions).some(
			(transition) => transition.name === '#E'
		)
	)
		return 'ENFA'

	if (
		// check if each state has only max one outgoing transition of each value
		Object.values(data.states).some((state) => {
			const stateTransitions = Object.values(data.transitions).filter(
				(transition) => transition.startState === state.id
			)

			// construct a set with transition values and check if it's the same size as the map of the outgoing transitions
			const transitionSet = stateTransitions.reduce(
				(acc, transition) => acc.add(transition.name),
				Set<string>()
			)

			return transitionSet.size !== stateTransitions.length
		})
	)
		return 'NFA'

	return 'DFA'
}
