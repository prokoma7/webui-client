import { Map } from 'immutable'
import { Algorithm } from 'interfaces/Algorithms'
import { AlgorithmCategory } from 'interfaces/Algorithms'

const createCategory = (
	categories: string[],
	algorithms: Algorithm[]
): AlgorithmCategory => {
	const [category, ...subcategories] = categories

	if (subcategories.length === 0)
		return {
			categoryName: category,
			subcategories: Map(),
			tags: [],
			algorithms,
		}

	return {
		categoryName: category,
		subcategories: Map<string, AlgorithmCategory>().set(
			subcategories[0],
			createCategory(subcategories, algorithms)
		),
		algorithms: [],
		tags: [],
	}
}

const addAlgorithm = (
	algorithmCategories: Map<string, AlgorithmCategory>,
	categories: string[],
	algorithm: Algorithm
): Map<string, AlgorithmCategory> => {
	const [category, ...subcategories] = categories
	const algorithmCategory = algorithmCategories.get(category)

	if (!algorithmCategory)
		// create new category
		return algorithmCategories.set(
			category,
			createCategory(categories, [algorithm])
		).sort((a, b) => a.categoryName.localeCompare(b.categoryName))

	if (subcategories.length === 0) {
		// no more categories to delve into, add algorithm here
		algorithmCategory.algorithms.push(algorithm)
		algorithmCategory.algorithms.sort((a, b) => a.name.localeCompare(b.name))
		return algorithmCategories
	}

	algorithmCategory.subcategories = addAlgorithm(
		algorithmCategory.subcategories,
		subcategories,
		algorithm
	)

	return algorithmCategories
}

export const categorizeAlgorithms = (algorithms: Algorithm[]) =>
	algorithms.reduce((acc, algorithm) => {
		const categories = algorithm.name.split('::')

		if (algorithm.templateParams.length > 0)
			categories.push(
				algorithm.templateParams
					.reduce((acc, typeParam) => `${acc}, @${typeParam}`, '')
					.slice(1)
			)

		return addAlgorithm(acc, categories, algorithm)
	}, Map<string, AlgorithmCategory>())

