import { AlibType, Algorithm, Overload } from 'interfaces/Algorithms'
import { Casts } from 'interfaces/Casts'
import { removeTemplate } from 'utils/alt/removeTemplate'

export const processCasts = (algorithms: Algorithm[], casts: Casts) => {
	let ret : Casts

	ret = Object.fromEntries(Object.entries(casts).map(([typeFrom, typesTo]) => [
		removeTemplate(typeFrom),
		typesTo.map((typeTo: any) => removeTemplate(typeTo))
	]))

	let dotConverterOverloads: Overload[] | undefined = algorithms.find((algorithm) => algorithm.name === 'convert::DotConverter')?.overloads
	dotConverterOverloads
		?.map((overload) => removeTemplate(overload.params[0].type))
		.forEach((dotConvertible) => {
			if (!ret[dotConvertible])
				ret[dotConvertible] = []

			ret[dotConvertible].push('DotConvertible')
		})

	return ret
}

export const isCastable = (casts: Casts, from: AlibType, to: AlibType) => {
	return [from, to].includes('abstraction::UnspecifiedType') ||
	removeTemplate(from) === removeTemplate(to) ||
	casts[removeTemplate(from)]?.includes(removeTemplate(to))
}
