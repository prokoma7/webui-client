declare global {
	interface Window {
		clientX: number
		clientY: number
	}
}

window.clientX = 0
window.clientY = 0

/**
 * Create global cursor param and update it
 *
 * @export
 */
export default function globalCursor() {
	const onMove = (event: MouseEvent) => {
		window.clientX = event.clientX
		window.clientY = event.clientY
	}

	document.addEventListener('mousemove', onMove)
}
