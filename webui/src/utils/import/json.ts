import { v4 as uuidv4 } from 'uuid'

import StateData, { StateMap, TransitionMap } from '../../interfaces/StateData'
import { ImportFn } from '../../interfaces/Transformations'

interface ExportData {
	initial: string[]
	final: string[]
	states: {
		[key: string]: {
			[key: string]: string
		}
	}
}

/**
 * Convert json data string to data object
 *
 * @param {string} dataString
 * @returns
 */
const importData: ImportFn = (dataString: string) => {
	const importData = JSON.parse(dataString) as ExportData
	const data: StateData = {
		initialStates: [],
		finalStates: [],
		// Create states
		states: Object.keys(importData.states).reduce(
			(acc, curr) => {
				const id = uuidv4()
				return {
					...acc,
					[id]: {
						id,
						name: curr,
						position: { x: 0, y: 0 },
						size: 0
					}
				}
			},
			{} as StateMap
		),
		transitions: {}
	}

	// Find initial states
	data.initialStates = importData.initial.reduce(
		(acc, curr) => {
			const id = Object.values(data.states).find(s => s.name === curr)
			return id ? [...acc, id.id] : acc
		},
		[] as string[]
	)

	// Find final states
	data.finalStates = importData.final.reduce(
		(acc, curr) => {
			const id = Object.values(data.states).find(s => s.name === curr)
			return id ? [...acc, id.id] : acc
		},
		[] as string[]
	)

	// Create transitions
	data.transitions = Object.keys(importData.states).reduce(
		(acc, curr) => ({
			...acc,
			...Object.keys(importData.states[curr]).reduce(
				(a, c) => {
					const id = uuidv4()
					const startState = Object.values(data.states).find(
						s => s.name === curr
					)
					const endState = Object.values(data.states).find(
						s => s.name === importData.states[curr][c]
					)

					if (!startState || !endState) {
						return a
					}

					return {
						...a,
						[id]: {
							id,
							name: c,
							startState: startState.id,
							endState: endState.id
						}
					}
				},
				{} as TransitionMap
			)
		}),
		{} as TransitionMap
	)

	return data
}

export default importData
