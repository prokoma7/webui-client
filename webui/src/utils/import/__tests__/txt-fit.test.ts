jest.mock('uuid', () => {
	let id = 0
	return { v4: () => (++id).toString(), reset: () => (id = 0) }
})

import uuid from 'uuid'
import importData from '../txt-fit'

import StateData from '../../../interfaces/StateData'
import Map from '../../../interfaces/Map'
import State from '../../../interfaces/State'
import Transition from '../../../interfaces/Transition'

type StateMap = Map<State>
type TransitionMap = Map<Transition>

const stateMapFactory = (...args: { id: string; name: string }[]): StateMap =>
	args.reduce(
		(acc, val) => ({
			...acc,
			[val.id]: { ...val, position: { x: 0, y: 0 }, size: 0 }
		}),
		{}
	)

const stateMap = stateMapFactory(
	{ id: '1', name: 's1' },
	{ id: '2', name: 's2' },
	{ id: '3', name: 's3' },
	{ id: '4', name: 's4' }
)

const transition1: Transition = {
	id: '5',
	name: 't1',
	startState: '1',
	endState: '2'
}
const transition2: Transition = {
	id: '6',
	name: 't2',
	startState: '1',
	endState: '1'
}
const transition3: Transition = {
	id: '7',
	name: 't3',
	startState: '2',
	endState: '3'
}
const transition4: Transition = {
	id: '9',
	name: 't4',
	startState: '4',
	endState: '1'
}
const transition5: Transition = {
	id: '9',
	name: 't5',
	startState: '4',
	endState: '1a'
}
const transition6: Transition = {
	id: '10',
	name: 't4',
	startState: '4',
	endState: '2'
}
const transitionMap: TransitionMap = {
	5: transition1,
	6: transition2,
	7: transition3,
	9: transition4
}

export const data: StateData = {
	states: stateMap,
	transitions: transitionMap,
	initialStates: ['1'],
	finalStates: ['1', '2']
}

export const file: string = `DFA	t1	t2	t3	t4
><s1	s2	s1	-	-
<s2	-	-	s3	-
s3	-	-	-	-
s4	-	-	s5	s1`

const NFA: StateData = {
	states: stateMapFactory({ id: '1', name: 'a' }, { id: '2', name: 'b' }),
	transitions: {
		3: {
			id: '3',
			name: '1',
			startState: '1',
			endState: '2'
		},
		4: {
			id: '4',
			name: '1',
			startState: '1',
			endState: '1'
		},
		5: {
			id: '5',
			name: '0',
			startState: '1',
			endState: '1'
		}
	},
	initialStates: ['1'],
	finalStates: ['2']
}

const NFAFile = `NFA	1	0
>a	b|a	a
<b	-	-`

const setDFA: StateData = {
	states: stateMapFactory(
		{ id: '1', name: '{a}' },
		{ id: '2', name: '{a, b}' }
	),
	transitions: {
		3: {
			id: '3',
			name: '0',
			startState: '1',
			endState: '1'
		},
		4: {
			id: '4',
			name: '1',
			startState: '1',
			endState: '2'
		},
		5: {
			id: '5',
			name: '0',
			startState: '2',
			endState: '1'
		},
		6: {
			id: '6',
			name: '1',
			startState: '2',
			endState: '2'
		}
	},
	initialStates: ['1'],
	finalStates: ['2']
}

const DFASetFile = `DFA 0 1
>{a} {a} {a, b}
<{a, b} {a} {a, b}\n\n`

describe('import txt-fit', () => {
	beforeEach(() => (uuid as any).reset())

	it('converts DFA', () => {
		expect(importData(file)).toEqual(data)
	})

	it('converts NFA', () => {
		expect(importData(NFAFile)).toEqual(NFA)
	})

	it('converts DFA with set syntax', () => {
		expect(importData(DFASetFile)).toEqual(setDFA)
	})

	afterAll(() => jest.resetModules())
})
