import { v4 as uuidv4 } from 'uuid'

import StateData from '../../interfaces/StateData'
import { ImportFn } from '../../interfaces/Transformations'

interface ImportTransition {
	endState: string
	name: string
}

interface ImportState {
	id: string
	name: string
	initial: boolean
	final: boolean
	transitions: ImportTransition[]
}

interface ParseResult {
	parsed: string
	remaining: string
}

/** Parses string to index
 * 
 * @param str String to parse
 * @param index index to parse to
 */
const parseToIndex = (str: string, index: number): ParseResult => ({
	parsed: str.substring(0, index + 1),
	remaining: str.substring(index + 1)
})

const closingSymbolMap: { [key: string]: string } = {
	'(': ')',
	'[': ']',
	'{': '}'
}

/** Parses state name that is enclosed in parentheses, for example when the state name is a set
 * 
 * @param stateString String to parse from
 */
const parseParenStateName = (stateString: string): ParseResult => {
	const endIndex = stateString.search(closingSymbolMap[stateString[0]])
	return parseToIndex(stateString, endIndex)
}

const parseStateName = (stateString: string) => {
	if (['(', '[', '{'].includes(stateString[0]))
		return parseParenStateName(stateString)

	// closing symbol of the state name
	const closingSymbol = stateString.match(/^[^\s]+\|/) ? '|' : /\s/
	const endIndex = stateString.search(closingSymbol)

	const parseResult = parseToIndex(
		stateString,
		endIndex !== -1 ? endIndex : stateString.length - 1
	)

	// trim whitespace
	parseResult.parsed = parseResult.parsed.trim()

	return parseResult
}

const _parseTransitions = (
	stateString: string,
	transitionNames: string[],
	acc: ImportTransition[] = []
): ImportTransition[] => {
	const trimmedStateString = stateString.trimStart()
	const [transitionName, ...remainingTransitionNames] = transitionNames

	// line is empty and there is no more transition entries to parse
	if (trimmedStateString === '' && transitionNames.length === 0) return acc
	// line is empty but there are undefined transitions
	if (trimmedStateString === '')
		throw new Error(
			`Missing transition entry for transition value ${transitionName}`
		)

	// line has extra transition entries
	if (transitionNames.length === 0)
		throw new Error(`Transition entry too long`)

	// parse the name of the state
	const { parsed, remaining } = parseStateName(trimmedStateString)

	const trimmedRemaining = remaining.trimStart()

	// if transition is defined, add it to the new accumulator
	const newAcc =
		parsed !== '-'
			? [...acc, { endState: parsed, name: transitionName }]
			: acc

	return trimmedRemaining.startsWith('|')
		? _parseTransitions(trimmedRemaining.slice(1), transitionNames, newAcc)
		: _parseTransitions(trimmedRemaining, remainingTransitionNames, newAcc)
}

const parseTransitions = (
	stateString: string,
	transitionNames: string[]
): ImportTransition[] => _parseTransitions(stateString, transitionNames)

/**
 * Convert txt-fit data string to data object
 *
 * @param {string} dataString
 * @returns
 */
const importData: ImportFn = (dataString: string) => {
	const importData = dataString.trim().split('\n').map(str => str.trim())
	const data: StateData = {
		initialStates: [],
		finalStates: [],
		states: {},
		transitions: {}
	}
	// Ignore Automaton type id and get transitions
	const transitionNames = importData[0].split(/\s/).slice(1)

	//              t1  t2                  t3
	// [>][<]state 	- 	tostate1 | tostat2 	-
	const states: ImportState[] = importData.slice(1).map(stateString => {
		const id = uuidv4()
		let initial: boolean = false
		let final: boolean = false

		// Initial id > is first char
		if (stateString[0] === '>') {
			initial = true
			stateString = stateString.slice(1)
		}
		// Final id < is next first char
		if (stateString[0] === '<') {
			final = true
			stateString = stateString.slice(1)
		}

		const { parsed: name, remaining } = parseStateName(stateString)

		// Get transitions
		const transitions = parseTransitions(remaining, transitionNames)

		return {
			id,
			name,
			initial,
			final,
			transitions
		}
	})

	// Create objects
	data.states = states.reduce(
		(acc, curr) => ({
			...acc,
			[curr.id]: {
				id: curr.id,
				name: curr.name,
				position: { x: 0, y: 0 },
				size: 0
			}
		}),
		{}
	)

	data.initialStates = states.reduce(
		(acc, curr) => (curr.initial ? [...acc, curr.id] : acc),
		[] as string[]
	)

	data.finalStates = states.reduce(
		(acc, curr) => (curr.final ? [...acc, curr.id] : acc),
		[] as string[]
	)

	data.transitions = states.reduce(
		(acc, curr) => ({
			...acc,
			...curr.transitions.reduce((a, c) => {
				const id = uuidv4()
				const endState = Object.values(data.states).find(
					s => s.name === c.endState
				)

				if (!endState) {
					return a
				}

				return {
					...a,
					[id]: {
						id,
						name: c.name,
						startState: curr.id,
						endState: endState.id
					}
				}
			}, {})
		}),
		{}
	)

	return data
}

export default importData
