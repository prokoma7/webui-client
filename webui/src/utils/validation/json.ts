import {
	hasProperties,
	unidentKeys,
	validateTypesName,
	validateTypesData,
	validateProp
} from '../validation'
import { ValidateFn } from '../../interfaces/Transformations'

/**
 * Validate json file
 *
 * @param {*} data
 * @returns
 */
const validate: ValidateFn<any> = data => {
	const keys = ['initial', 'final', 'states']
	let errors = [...hasProperties(keys)(data)]
	if (errors.length > 0) return errors

	const states = Object.values(data.states)
	errors = [
		...errors,
		...unidentKeys(keys)(data),
		...validateTypesName(data.initial).map(
			e => 'Initial state error: ' + e
		),
		...validateTypesName(data.final).map(e => 'Final state error: ' + e),
		...validateTypesData(states).map(e => 'State error: ' + e),
		...validateProp(states, Object.values, validateTypesName).map(
			e => 'Transition error: ' + e
		)
	]

	return errors
}

export default validate
