import { validateTypesName, validateStringArr } from '../validation'
import { ValidateFn } from '../../interfaces/Transformations'

/**
 * Validate txt file
 *
 * @param {*} data
 * @returns
 */
const validate: ValidateFn<any> = data => {
	let errors: string[] = []

	if (data.length < 3) {
		errors = [
			...errors,
			'Missing data lines (1 - states, 2 - initial, 3 - final)'
		]
	} else {
		const STATE_ROW = 0
		const INITIAL_STATE_ROW = 1
		const FINAL_STATE_ROW = 2
		const transitions = data.slice(3)

		errors = [
			...errors,
			...validateTypesName(data[STATE_ROW]),
			...validateTypesName(data[INITIAL_STATE_ROW]),
			...validateTypesName(data[FINAL_STATE_ROW]),
			...validateStringArr('Transition')(transitions, 3)
		]
	}

	return errors
}

export default validate
