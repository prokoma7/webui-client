import {
	hasProperties,
	unidentKeys,
	validateType,
	validateTypesName,
	validateTypesData,
	validateProp
} from '../validation'
import { ValidateFn } from '../../interfaces/Transformations'

/**
 * Validate json-xstate file
 *
 * @param {*} data
 * @returns
 */
const validate: ValidateFn<any> = data => {
	const keys = ['id', 'initial', 'states']
	let errors = [...hasProperties(keys)(data)]
	if (errors.length > 0) return errors

	const states = Object.values(data.states)
	errors = [
		...errors,
		...hasProperties(keys)(data),
		...unidentKeys(keys)(data),
		...validateType(data.id, ['string']).map(e => 'Id name error: ' + e),
		...validateType(data.initial, ['string']).map(
			e => 'Initial state name error: ' + e
		),
		...validateTypesData(states).map(e => 'State error: ' + e),
		...validateProp(states, v => v, hasProperties(['on'])).map(
			e => 'State error: ' + e
		),
		...validateProp(states, v => v, unidentKeys(['on'])).map(
			e => 'State error: ' + e
		),
		...validateProp(states, Object.values, validateTypesData).map(
			e => 'Transitions error: ' + e
		),
		...validateProp(
			states,
			prop =>
				Object.values(prop).reduce(
					(acc, v) => [...acc, ...Object.values(v)],
					[]
				),
			validateTypesName
		).map(e => 'Transition error: ' + e)
	]

	return errors
}

export default validate
