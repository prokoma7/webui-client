import validate from '../json'

const data: any = {
	initial: ['a', {}],
	final: ['b', {}],
	states: {
		a: {},
		b: {
			'1': 'a'
		},
		c: {
			'1': {}
		},
		x: 'k'
	},
	test: {}
}

test('expect error with file wrong props', () => {
	expect(validate({})).toEqual([
		'Missing property: initial',
		'Missing property: final',
		'Missing property: states'
	])
})

test('expect get correct errors', () => {
	expect(validate(data)).toEqual([
		'Unidentified key: test',
		'Initial state error: [object Object] is not string or number',
		'Final state error: [object Object] is not string or number',
		'State error: k is not object',
		'Transition error: [object Object] is not string or number'
	])
})
