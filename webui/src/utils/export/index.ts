import { saveAs } from 'file-saver'

import StateData from '../../interfaces/StateData'
import { ExportFn } from '../../interfaces/Transformations'
import JSON from './json'
import JSONXSTATE from './json-xstate'
import TXT from './txt'
import TXTFIT from './txt-fit'

export type Variant = 'json' | 'json-xstate' | 'txt' | 'txt-fit'

/**
 * Converts data into file and exports them to file variant
 *
 * @export
 * @param {StateData} data
 * @param {Variant} variant
 */
export function downloadData(data: StateData, variant: Variant) {
	let exportType: ExportFn
	let metaType: 'application/json' | 'text/plain'
	let fileType: 'json' | 'txt'
	switch (variant) {
		case 'json-xstate':
			exportType = JSONXSTATE
			metaType = 'application/json'
			fileType = 'json'
			break
		case 'txt':
			exportType = TXT
			metaType = 'text/plain'
			fileType = 'txt'
			break
		case 'txt-fit':
			exportType = TXTFIT
			metaType = 'text/plain'
			fileType = 'txt'
			break
		case 'json':
		default:
			exportType = JSON
			metaType = 'application/json'
			fileType = 'json'
			break
	}

	const exportString = exportType(data)

	const exportBlob = new Blob([exportString], {
		type: metaType + ';charset=utf-8'
	})
	saveAs(exportBlob, 'output.' + fileType)
}
