import TGraph, {
	Vertex as TVertex,
	VertexMap as TVertexMap,
	Edge,
	toGraph,
	VertexReducer as TVertexReducer,
	EdgeReducer as TEdgeReducer,
	baseVertex,
	baseEdge,
	updateElement,
	toArray,
	mapMap,
	toData
} from '../../interfaces/Graph'
import Position, * as PosOp from '../../interfaces/Position'
import { PositionFn } from '../../interfaces/Transformations'

const WIDTH = (document.documentElement.clientWidth || 800) * 0.4
const HEIGHT = (document.documentElement.clientHeight || 800) * 0.4
const COOLING = 0.97
const ITERATIONS = 200
const INITIAL_TEMP = Math.min(WIDTH, HEIGHT) / 4

interface Vertex extends TVertex {
	displacement: Position
}
const DEFAULT_V: Vertex = {
	...baseVertex,
	displacement: PosOp.toPosition(0, 0)
}
const DEFAULT_E: Edge = baseEdge

type VertexMap = TVertexMap<Vertex>
type Graph = TGraph<Vertex, Edge>
type VertexReducer<T> = TVertexReducer<T, Vertex>
type EdgeReducer<T> = TEdgeReducer<T, Edge>

const getArea = (): number => WIDTH * HEIGHT
export const fk = (area: number, verticeCount: number): number =>
	Math.sqrt(verticeCount === 0 ? 0 : area / verticeCount)
export const fr = (x: number, k: number): number =>
	x === 0 ? 0 : Math.pow(k, 2) / x
export const fa = (x: number, k: number): number =>
	k === 0 ? 0 : Math.pow(x, 2) / k

const repulse = (diff: Position, dist: number, k: number): Position =>
	PosOp.multiplyByX(PosOp.divideByX(diff, dist), fr(dist, k))
const attract = (diff: Position, dist: number, k: number): Position =>
	PosOp.multiplyByX(PosOp.divideByX(diff, dist), fa(dist, k))
const displace = (disp: Position, temp: number): Position =>
	PosOp.multiply(
		PosOp.divide(disp, PosOp.absolute(disp)),
		PosOp.minX(PosOp.absolute(disp), temp)
	)
const cool = (x: number): number => x * COOLING

const setPosition = (position: Position) => updateElement<Vertex>({ position })
const setDisplacement = (displacement: Position) =>
	updateElement<Vertex>({ displacement })

const repulseFromOthers = (k: number, position: Position) => (
	acc: Position,
	curr: Position
): Position => {
	const difference = PosOp.subtract(position, curr)
	const distance = PosOp.distance(difference)
	return PosOp.add(acc, repulse(difference, distance, k))
}

type RepulseFn = (
	k: number
) => (vertices: Vertex[]) => (vertex: Vertex) => Vertex
const calculateRepulsiveForces: RepulseFn = k => vertices => vertex =>
	setDisplacement(
		vertices
			.filter(v => v.id !== vertex.id)
			.map(v => v.position)
			.reduce<Position>(
				repulseFromOthers(k, vertex.position),
				PosOp.toPosition(0, 0)
			)
	)(vertex)

type AttractFn = (k: number) => EdgeReducer<VertexMap>
const calculateAttractingForces: AttractFn = k => (acc, curr) => {
	const from = acc[curr.from]
	const to = acc[curr.to]

	if (!from || !to) return acc

	const difference = PosOp.subtract(from.position, to.position)
	const distance = PosOp.distance(difference)
	const attraction = attract(difference, distance, k)

	return {
		...acc,
		[curr.from]: setDisplacement(
			PosOp.subtract(from.displacement, attraction)
		)(from),
		[curr.to]: setDisplacement(PosOp.add(to.displacement, attraction))(to)
	}
}

const limitDisplacement = (temperature: number) => (vertex: Vertex) =>
	setPosition(
		PosOp.round(
			PosOp.add(
				vertex.position,
				displace(vertex.displacement, temperature)
			)
		)
	)(vertex)

const position: PositionFn = data => {
	let graph = toGraph<Vertex, Edge>(DEFAULT_V, DEFAULT_E)(data)
	const k = fk(getArea(), Object.keys(graph.vertices).length)

	let vertices = mapMap<Vertex>(v =>
		setPosition(PosOp.random(PosOp.toPosition(WIDTH, HEIGHT)))(v)
	)(graph.vertices)

	let temperature = INITIAL_TEMP
	for (let i = 0; i < ITERATIONS; i++) {
		vertices = mapMap(calculateRepulsiveForces(k)(toArray(vertices)))(
			vertices
		)
		vertices = graph.edges.reduce(calculateAttractingForces(k), vertices)
		vertices = mapMap(limitDisplacement(temperature))(vertices)

		temperature = cool(temperature)
	}

	graph.vertices = vertices
	return toData(data, graph)
}

export default position
