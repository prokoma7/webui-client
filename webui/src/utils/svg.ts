import Position from '../interfaces/Position'
import { middlePoint, linePoint, curvePoint } from './positioning'

/**
 * Creates line path from start to end
 *
 * @param {Position} start
 * @param {Position} end
 */
export const line = (start: Position, end: Position) =>
	`M ${start.x} ${start.y} L ${end.x} ${end.y}`

/**
 * Creates bezier curve between points start and end
 * Curve can have offset from center, which makes it more round outwards
 *
 * @param {Position} startPoint
 * @param {Position} endPoint
 * @param {number} offset
 * @returns
 */
export const curve = (
	startPoint: Position,
	endPoint: Position,
	offset: number
) => {
	// Find points on the 1/3 and 2/3 of the line
	const p1Line = linePoint(startPoint, endPoint, 1 / 3)
	const p2Line = linePoint(startPoint, endPoint, 2 / 3)
	// Get points on the perpendicular of current line using offset to calculate distance
	const p1Curve = curvePoint(startPoint, endPoint, p1Line, offset)
	const p2Curve = curvePoint(startPoint, endPoint, p2Line, offset)

	const start = `${startPoint.x} ${startPoint.y}`
	const end = `${endPoint.x} ${endPoint.y}`
	const p1 = `${p1Curve.x} ${p1Curve.y}`
	const p2 = `${p2Curve.x} ${p2Curve.y}`

	// SVG path
	return `M ${start} C ${p1} ${p2} ${end}`
}

/**
 * Creates bezier curve between points start and end
 * Curve can have offset from center, which makes it more round outwards
 * Curve is rounded around itself
 *
 * @param {Position} startPoint
 * @param {Position} endPoint
 * @param {number} offset
 * @returns
 */
export const identityCurve = (
	startPoint: Position,
	endPoint: Position,
	offset: number
) => {
	const size = Math.abs(startPoint.x - endPoint.x)
	// Middle point in center of points above them
	const middleP = middlePoint(startPoint, endPoint)
	middleP.y = middleP.y + offset * 16
	const start = `${startPoint.x} ${startPoint.y}`
	const middle = `${middleP.x} ${middleP.y - size}`
	const end = `${endPoint.x} ${endPoint.y}`
	// Top left
	const left = middleP.x + (5 * size) / 6 - offset * 8
	// Top right
	const right = middleP.x - (5 * size) / 6 + offset * 8
	const p1 = `${left} ${middleP.y - size / 6}`
	const p2 = `${left} ${middleP.y - size}`
	const p3 = `${right} ${middleP.y - size}`
	const p4 = `${right} ${middleP.y - size / 6}`

	// SVG curve
	return `M ${end} C ${p4} ${p3} ${middle} C ${p2} ${p1} ${start}`
}

const mid = (a: number, b: number) => a + (b - a) / 2

export const bezierSCurve = (from: Position, to: Position) => {
	const middleX = mid(from.x, to.x)
	return `M${from.x} ${from.y} C${middleX} ${from.y} ${middleX} ${to.y} ${to.x} ${to.y}`
}

const bezierZCurveSpacing = 50
// M 100,100 C 175,100 175,150 100, 150 C 25,150 25,200 100,200
export const bezierZCurve = (from: Position, to: Position): string => {
	const midPoint = middlePoint(from, to)

	return `M${from.x} ${from.y} C${from.x + bezierZCurveSpacing} ${from.y} ${
		from.x + bezierZCurveSpacing
	} ${midPoint.y} ${midPoint.x} ${midPoint.y} C${
		to.x - bezierZCurveSpacing
	} ${midPoint.y} ${to.x - bezierZCurveSpacing} ${to.y} ${to.x} ${to.y}`
}
