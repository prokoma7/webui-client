import { Set } from 'immutable'
import { detectCycles } from "../detectCycle";
import AlgorithmEdge from '../../interfaces/AlgorithmEdge';

const edges = Set<Omit<AlgorithmEdge, 'endParamIndex'>>([
  { id: '1', startNodeId: '1', endNodeId: '2' },
  { id: '2', startNodeId: '2', endNodeId: '3' },
  { id: '3', startNodeId: '3', endNodeId: '4' },
  { id: '4', startNodeId: '4', endNodeId: '2' }
])

describe('detectCycle', () => {
  it('detects cycle', () => {
    const result = detectCycles(edges).toArray();
    ['2', '3', '4'].forEach(edge => expect(result).toContain(edge));
    expect(result).toHaveLength(3)
  })

  it("doesn't detect cycle", () => {
    expect(detectCycles(edges.remove(edges.last())).toArray()).toHaveLength(0)
  })
})