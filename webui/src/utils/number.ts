/**
 * Update number x to prec precision
 *
 * @param {number} x
 * @param {number} [prec=2]
 */
export const round = (x: number, prec: number = 2) =>
	x === 0 ? 0 : Math.round(Math.pow(10, prec) * x) / Math.pow(10, prec)
