interface UploadResult {
    filename: string,
    content: string
}

export const upload = () => new Promise<UploadResult>((resolve) => {
	const input = document.createElement('input')
	input.type = 'file'
	input.accept = 'application/json, text/plain, .json, .txt'

	input.addEventListener('change', (e: Event) => {
		const files = (e as any)?.target?.files

		// Allow anly loading 1 file at once
		if (!files || files.length !== 1) return

		const reader = new FileReader()

		reader.addEventListener('loadend', () =>
			resolve({ filename: file.name, content: reader.result as string })
		)

		const [file] = files
		reader.readAsText(file, 'utf-8')
	})

	input.click()
})
