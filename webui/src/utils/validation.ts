/**
 * Checks if object has p property
 *
 * @param {Object} obj
 * @param {string} p
 */
export const hasProperty = (obj: Object, p: string) =>
	obj.hasOwnProperty(p) ? [] : ['Missing property: ' + p]

/**
 * Check if object has all properties
 *
 * @param {string[]} props
 */
export const hasProperties = (props: string[]) => (obj: Object) =>
	props.reduce<string[]>((acc, p) => [...acc, ...hasProperty(obj, p)], [])

/**
 * Check if object has more keys
 *
 * @param {string[]} keys
 */
export const unidentKeys = (keys: string[]) => (obj: Object) =>
	Object.keys(obj)
		.filter(k => !keys.includes(k))
		.map(k => 'Unidentified key: ' + k)

/**
 * Check if value is of type from types
 *
 * @template T
 * @param {T} value
 * @param {string[]} types
 */
export const validateType = <T>(value: T, types: string[]) =>
	types.reduce<boolean>((a, t) => a || typeof value === t, false)
		? []
		: [value + ' is not ' + types.join(' or ')]

/**
 * Check if all values are of types
 *
 * @param {string[]} types
 */
export const validateTypes = (types: string[]) => <T>(values: T[]) =>
	values.reduce<string[]>((acc, v) => [...acc, ...validateType(v, types)], [])

/** Check if values are of types string or number */
export const validateTypesName = validateTypes(['string', 'number'])
/** Check if values are of type object */
export const validateTypesData = validateTypes(['object'])

type Getter = (prop: Object) => Object | string[]
type Validate = (data: any) => string[]
/**
 * Apply validation of each item in array of object identified by getter
 *
 * @param {any[]} data
 * @param {Getter} getter
 * @param {Validate} validate
 */
export const validateProp = (data: any[], getter: Getter, validate: Validate) =>
	data.reduce<string[]>(
		(acc, prop) => [...acc, ...validate(getter(prop))],
		[]
	)

/**
 * Check if array has at least length
 *
 * @param {any[]} arr
 * @param {number} length
 * @param {string} err
 */
const hasLength = (arr: any[], length: number, err: string) =>
	arr.length === length ? [] : [`${err}: length error (not ${length})`]
/**
 * Check if array is of strings
 *
 * @param {any[]} arr
 * @param {string} err
 */
const isChildrenString = (arr: any[], err: string) =>
	arr.reduce((acc, curr) => acc && typeof curr === 'string', true)
		? []
		: [`${err}: type error (not string)`]

/**
 * Check if array has length and is of strings
 *
 * @param {string} err
 */
export const validateStringArr = (err: string) => (
	arr: string[][],
	length: number
) =>
	arr.reduce<string[]>(
		(acc, curr, id) => [
			...acc,
			...hasLength(curr, length, err + ' ' + id),
			...isChildrenString(curr, err + ' ' + id)
		],
		[]
	)

/**
 * Check if all values in array are in array of data
 *
 * @param {string[]} arr
 * @param {string} err
 */
export const existsInArr = (arr: string[], err: string) => (data: string[]) =>
	arr.reduce<string[]>(
		(acc, curr) =>
			data.includes(curr)
				? acc
				: [...acc, err + " doesn't exist - " + curr],
		[]
	)

/**
 * Check if array has empty string
 *
 * @param {string[]} arr
 * @param {string} err
 */
export const hasEmptyString = (arr: string[], err: string) =>
	arr.reduce<string[]>(
		(acc, curr) =>
			curr === '' ? [...acc, err + " can't be empty string"] : acc,
		[]
	)
