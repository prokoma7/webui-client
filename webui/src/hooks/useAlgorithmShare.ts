import {Base64} from 'js-base64'
import {useSnackbar} from 'notistack'
import {useCallback} from 'react'
import {useSelector} from 'react-redux'

import {AlgorithmGraph} from '../interfaces/AlgorithmGraph'
import {ReduxState} from '../reducers'


export const useAlgorithmShare = () => {
  const {nodes, edges} = useSelector((state: ReduxState) => ({
                                       nodes: state.algorithmData.present.nodes,
                                       edges: state.algorithmData.present.edges
                                     }))

  const {enqueueSnackbar} = useSnackbar()

  return useCallback(() => {
    const exportData: Omit<AlgorithmGraph, 'outputValues'> = {
      nodes,
      edges
    }

    const json = JSON.stringify(exportData);

    const url: string = window.location.protocol + '//' +  window.location.host + window.location.pathname + '?#' + Base64.encode(json)

    navigator.clipboard.writeText(url)

    enqueueSnackbar(`Link copied to cliboard`, {variant: 'info'})
  }, [nodes, edges, enqueueSnackbar])
}
