import * as React from 'react'

import Position, { snap, add, modX } from '../interfaces/Position'
import { MouseMode } from '../reducers/stateUI'
import { cursorPositionOnCanvas } from '../utils/positioning'

const GRID_SIZE = 25

const defaultPosition: Position = { x: 0, y: 0 }

const cursorPositionFromCanvasRef = (
	canvasRef: React.RefObject<SVGSVGElement>,
	clientX: number,
	clientY: number
) =>
	canvasRef.current
		? cursorPositionOnCanvas(canvasRef.current, clientX, clientY)
		: defaultPosition

/**
 * Current cursor position on document
 * Snappes to grid and uses mouse modes
 *
 * @param {boolean} shouldMove
 * @returns {Position}
 */
const useCursor = (
	canvasRef: React.RefObject<SVGSVGElement>,
	shouldMove: boolean,
	mouseMode: MouseMode,
	gridOn: boolean,
	scale: number,
	offset: Position,
	mouseDownOffset: Position
): Position => {
	const [position, setPosition] = React.useState<Position>(defaultPosition)

	// Calculates current mouse position
	const onSetPosition = React.useCallback(
		(e: MouseEvent) => {
			if (!shouldMove) return

			let current = cursorPositionFromCanvasRef(
				canvasRef,
				e.clientX,
				e.clientY
			)
			// Can move only horizontally or vertically
			if (mouseMode === 'horizontal') current.x = e.clientX
			else if (mouseMode === 'vertical') current.y = e.clientY

			// Snap to grid
			if (gridOn) {
				current = add(
					snap(add(current, mouseDownOffset), GRID_SIZE * scale),
					modX(offset, GRID_SIZE * scale)
				)
			}

			
			setPosition(current)
		},
		[canvasRef, mouseMode, gridOn, scale, offset, shouldMove, mouseDownOffset]
	)

	React.useEffect(() => {
		canvasRef?.current?.addEventListener('mousemove', onSetPosition)

		return () =>
			canvasRef?.current?.removeEventListener('mousemove', onSetPosition)
	}, [canvasRef, onSetPosition])

	React.useLayoutEffect(() => {
		// Update only when should move
		if (!shouldMove) return
		// Initial position
		const current = cursorPositionFromCanvasRef(
			canvasRef,
			window.clientX,
			window.clientY
		)

		setPosition(gridOn ? snap(add(current, mouseDownOffset), GRID_SIZE) : current)
	}, [canvasRef, shouldMove, gridOn, mouseDownOffset])

	return position
}

export default useCursor
