import React, { useCallback } from 'react'
import { batch, useSelector, useDispatch } from 'react-redux'

import { saveState, loadState } from '../utils/persist'
import { Actions, ReduxState } from '../reducers'
import { stateCanvasActions } from '../reducers/stateCanvas'

const { deselect, toggleGrid, setCursorMode } = stateCanvasActions

const {
	removeState,
	removeTransition,
	reset,
	toggleBuildTransition,
	toggleBuildState,
	set,
	setConfirmText,
	toggleInitialState,
	toggleFinalState,
	toggleShowHint,
	position,
	setMouseMode,
	undoStatemaker,
	redoStatemaker,
} = Actions

/**
 * Manages all global keyboard events and mouse events
 *
 */
const useKeyBindings = () => {
	const dispatch = useDispatch()

	const state = useSelector((state: ReduxState) => ({
		algorithm: state.stateUI.algorithm,
		mouseMode: state.stateUI.mouseMode,
		selectedState: state.stateCanvas.selectedNode,
		selectedTransition: state.stateCanvas.selectedEdge,
		buildState: state.stateUI.buildState,
		buildTransition: state.stateUI.buildTransition,
		data: state.stateData.present,
	}))

	const {
		algorithm,
		mouseMode,
		selectedState,
		selectedTransition,
		buildState,
		buildTransition,
		data,
	} = state

	// Delete selected elements
	const handleDelete = useCallback(() => {
		if (selectedState) {
			batch(() => {
				dispatch(deselect())
				dispatch(removeState(selectedState))
			})
		} else if (selectedTransition) {
			batch(() => {
				dispatch(deselect())
				dispatch(removeTransition(selectedTransition))
			})
		}
	}, [dispatch, selectedState, selectedTransition])

	// Reset selection
	const onClick = useCallback(
		(e?: React.MouseEvent) => {
			e?.stopPropagation()
			batch(() => {
				dispatch(deselect())
				dispatch(reset())
			})
		},
		[dispatch]
	)

	// Identifier if there is ongoing action on canvas
	const ongoingAction =
		selectedState || selectedTransition || buildState || buildTransition
	// Toogle build modes when there's no ongoing action
	const handleBuild = useCallback(
		(
			variant: 'default' | 'initial' | 'final' | 'transition',
			e: React.KeyboardEvent
		) => {
			if (ongoingAction) return
			e.preventDefault()

			if (variant === 'transition') {
				dispatch(toggleBuildTransition())
			} else {
				dispatch(toggleBuildState(variant))
			}
		},
		[dispatch, ongoingAction]
	)

	// Loads data from local storage
	const handleLoad = useCallback(() => {
		const data = loadState()
		if (data) {
			batch(() => {
				dispatch(set(data))
				dispatch(setConfirmText('Automata loaded'))
			})
		}
	}, [dispatch])

	// Saves data to local storage
	const handleSave = useCallback(() => {
		saveState(data)
		dispatch(setConfirmText('Automata saved'))
	}, [dispatch, data])

	// Handle key events
	const onKeyDown = useCallback(
		(e: React.KeyboardEvent) => {
			e.stopPropagation()
			switch (e.key) {
				case 'Delete':
					if (e.ctrlKey) handleDelete()
					break
				case 'Escape':
				case 'Enter':
					onClick()
					break
				case 's':
					if (e.ctrlKey) {
						e.preventDefault()
						handleSave()
					} else {
						handleBuild('default', e)
					}
					break
				case 'i':
					// Sets slected state as initial
					if (e.ctrlKey && selectedState)
						dispatch(toggleInitialState(selectedState))
					else handleBuild('initial', e)
					break
				case 'f':
					// Sets slected state as final
					if (e.ctrlKey && selectedState) {
						e.preventDefault()
						dispatch(toggleFinalState(selectedState))
					} else {
						handleBuild('final', e)
					}
					break
				case 't':
					handleBuild('transition', e)
					break
				case 'h':
					e.preventDefault()
					dispatch(toggleShowHint())
					break
				case 'm':
					e.preventDefault()
					batch(() => {
						dispatch(deselect())
						dispatch(setCursorMode('move'))
					})
					break
				case 'l':
					e.preventDefault()
					dispatch(setCursorMode('select'))
					break
				case 'p':
					e.preventDefault()
					dispatch(position(algorithm))
					break
				case 'g':
					e.preventDefault()
					dispatch(toggleGrid())
					break
				case 'Shift':
					e.preventDefault()
					dispatch(setMouseMode('horizontal'))
					break
				case 'Alt':
					e.preventDefault()
					dispatch(setMouseMode('vertical'))
					break
				case 'z':
					if (e.ctrlKey) dispatch(undoStatemaker())
					break
				case 'y':
					if (e.ctrlKey) dispatch(redoStatemaker())
					break
				case 'o':
					if (e.ctrlKey) {
						e.preventDefault()
						handleLoad()
					}
					break
				default:
			}
		},
		[
			dispatch,
			algorithm,
			handleDelete,
			onClick,
			handleBuild,
			handleLoad,
			handleSave,
			selectedState,
		]
	)

	// Disable vertical and horizontal mouse modes
	const onKeyUp = useCallback(
		(e: React.KeyboardEvent) => {
			if (mouseMode !== 'default') {
				dispatch(setMouseMode('default'))
			}
		},
		[dispatch, mouseMode]
	)

	// Disallow right click
	const onContextMenu = useCallback(
		(e: React.MouseEvent) => {
			e.preventDefault()
			e.stopPropagation()
			onClick()
		},
		[onClick]
	)

	// Disallow browser zoom
	const onWheel = useCallback((e: React.MouseEvent) => {
		if (e.ctrlKey) {
			e.preventDefault()
		}
	}, [])

	return { onKeyDown, onKeyUp, onContextMenu, onWheel, onClick }
}

export default useKeyBindings
