import * as React from 'react'

import useCursor from '../hooks/useCursor'
import Position, { round, normalize } from '../interfaces/Position'
import { MouseMode } from '../reducers/stateUI'

/**
 * Cursor position relative to application scale and offset
 *
 * @param {boolean} allowChange
 * @returns {Position}
 */

const zeroMouseDownOffset = { x: 0, y: 0 } as const

const usePosition = (
	canvasRef: React.RefObject<SVGSVGElement>,
	allowChange: boolean,
	mouseMode: MouseMode,
	gridOn: boolean,
	scale: number,
	offset: Position,
	mouseDownOffset: Position = zeroMouseDownOffset
): Position => {
	const update = allowChange
	// Get cursor position if the indicator is true
	const cursor = useCursor(
		canvasRef,
		update,
		mouseMode,
		gridOn,
		scale,
		offset,
		mouseDownOffset
	)
	const [position, setPosition] = React.useState<Position>(cursor)

	React.useEffect(() => {
		if (!canvasRef.current) return

		const canvas = canvasRef.current
		const handleCanvasMouseEnter = () => {
			if (allowChange) setPosition(normalize(cursor, offset, scale))
		}

		canvas.addEventListener('mouseenter', handleCanvasMouseEnter)
		return () =>
			canvas.removeEventListener('mouseenter', handleCanvasMouseEnter)
	}, [
		canvasRef,
		allowChange,
		setPosition,
		cursor,
		offset,
		scale
	])

	React.useLayoutEffect(() => {
		// Normalize the cursor position
		setPosition(normalize(cursor, offset, scale))
	}, [update, cursor, scale, offset])

	return round(position)
}

export default usePosition
