import { useCallback } from 'react'
import { useDispatch, useSelector, batch } from 'react-redux'
import { ReduxState } from '../reducers'
import { algorithmDataActions } from '../reducers/algorithmData'

const { deleteNode, deleteEdge } = algorithmDataActions

export const useAlgorithmDelete = () => {
	const dispatch = useDispatch()
	const { selectedNode, selectedEdge } = useSelector((state: ReduxState) => ({
		selectedNode: state.algorithmCanvas.selectedNode,
		selectedEdge: state.algorithmCanvas.selectedEdge
	}))

	return useCallback(
		() =>
			batch(() => {
				if (selectedNode) dispatch(deleteNode(selectedNode))
				else if (selectedEdge) dispatch(deleteEdge(selectedEdge))
			}),
		[dispatch, selectedNode, selectedEdge]
	)
}
