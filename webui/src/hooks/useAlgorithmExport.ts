import { useCallback } from 'react'
import { useSelector } from 'react-redux'
import { saveAs } from 'file-saver'
import { AlgorithmGraph } from '../interfaces/AlgorithmGraph'
import { ReduxState } from '../reducers'


export const useAlgorithmExport = () => {
  const { nodes, edges } = useSelector((state: ReduxState) => ({
    nodes: state.algorithmData.present.nodes,
    edges: state.algorithmData.present.edges
  }))

  return useCallback(() => {
    const exportData: Omit<AlgorithmGraph, 'outputValues'> = {
			nodes,
			edges
		}

		const blob = new Blob([JSON.stringify(exportData)], {
			type: 'application/json;charset=utf-8'
		})

		saveAs(blob, 'webui_export.json')
  }, [nodes, edges])
}