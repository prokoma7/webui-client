import { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { algorithmUIActions } from '../reducers/algorithmUI'

const { toggleHint } = algorithmUIActions

export const useAlgorithmToggleHint = () => {
	const dispatch = useDispatch()

	return useCallback(() => dispatch(toggleHint()), [dispatch])
}
