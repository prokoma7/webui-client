import { Map } from 'immutable'
import { AlgorithmGraph, serialize } from '../interfaces/AlgorithmGraph'
import { EvaluateResponse } from '../interfaces/EvaluateResponse'
import { EvaluateResponseError } from '../interfaces/EvaluateResponseError'

export interface IAlgorithmService {
	evaluate(data: AlgorithmGraph): Promise<Map<string, string>>
}

class AlgorithmService implements IAlgorithmService {
	evaluate = async (data: AlgorithmGraph): Promise<Map<string, string>> => {
		const body = serialize(data)
		let response: EvaluateResponse

		try {
			response = await fetch(process.env.REACT_APP_API_PREFIX + "/evaluate", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(body)
			}).then((res) => res.json())
		} catch (e) {
			console.error(e)
			throw new EvaluateResponseError(['Error while communicating with the server'])
		}

		if (response.error) throw new EvaluateResponseError(response.error)

		return Map<string, string>((response.outputs as any) ?? {})
	}
}

export default AlgorithmService
