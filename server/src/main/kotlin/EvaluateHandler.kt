import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.*
import org.apache.activemq.ActiveMQConnectionFactory
import org.apache.activemq.ActiveMQSession
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.concurrent.ConcurrentHashMap
import javax.jms.TextMessage


class EvaluateHandler(brokerUrl: String) {
    private val connection = ActiveMQConnectionFactory(brokerUrl).createConnection()
    private val session = connection.createSession(false, ActiveMQSession.AUTO_ACKNOWLEDGE)

    private val callbackMap = ConcurrentHashMap<Long, (String) -> Unit>()
    private var nextId: Long = 0
    private var listen: Boolean = true

    private val responseQueue = session.createQueue("response")
    private val responseConsumer = session.createConsumer(responseQueue)

    private val requestQueues = HashMap<TaskType, javax.jms.Queue>();
    private val requestProducers = HashMap<TaskType, javax.jms.MessageProducer>();

    init {
        for (taskType in TaskType.values()) {
            requestQueues[taskType] = session.createQueue(taskType.name)
            requestProducers[taskType] = session.createProducer(requestQueues[taskType])
        }
    }

    suspend fun evaluate(request: String, taskType: TaskType): String = callbackFlow {
        val callback: (String) -> Unit = { offer(it) }
        val taskId = generateId()

        callbackMap[taskId] = callback

        val msg = session.createTextMessage(request)
        msg.setLongProperty("TaskID", taskId)
        requestProducers[taskType]?.send(msg)

        println("${timestamp()} Task:${taskType.name} $taskId created and sent to Queue:${requestQueues[taskType]?.getQueueName()}")

        awaitClose {
            println("${timestamp()} $taskId received response")
            callbackMap.remove(taskId)
        }
    }.first()

    fun stop() {
        listen = false
        connection.close()
    }

    @Synchronized
    private fun generateId(): Long {
        if (nextId == Long.MAX_VALUE) {
            nextId = Long.MIN_VALUE
        }

        return nextId++
    }

    private fun timestamp() = DateTimeFormatter.ISO_INSTANT.format(Instant.now())

    init {
        connection.start()
        GlobalScope.launch {
            while (listen) {
                val msg = responseConsumer.receive() as TextMessage
                val taskId = msg.getLongProperty("TaskID")

                val callback = callbackMap[taskId] ?: continue
                callback(msg.text)
            }
        }
    }
}
