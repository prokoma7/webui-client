import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.Compression
import io.ktor.features.CORS
import io.ktor.features.deflate
import io.ktor.features.gzip
import io.ktor.features.minimumSize
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.apache.activemq.broker.BrokerService

enum class TaskType {
    EvaluateGraph,
    WorkerDefinitions,
}

fun main() {
    val broker = BrokerService()
    broker.brokerName = "broker"
    broker.addConnector("tcp://0.0.0.0:61616")
    broker.start()

    val evaluateHandler = EvaluateHandler(brokerUrl = "vm://${broker.brokerName}")

    embeddedServer(Netty, 3001) {
        install(CORS) {
            header(HttpHeaders.ContentType)
            anyHost()
        }

        install(Compression) {
            gzip()
            deflate()
            minimumSize(1024)
        }

        routing {
            post("/evaluate") {
                val result = evaluateHandler.evaluate(call.receiveText(), TaskType.EvaluateGraph)
                call.respond(result)
            }
            get("/definitions") {
                val result = evaluateHandler.evaluate("", TaskType.WorkerDefinitions)
                call.respond(result)
            }
        }
    }.start()
}
