# API Server

API Server accepts evaluate requests on several endpoints and passes them to broker.

The API Server is built using the [Ktor](https://ktor.io/) framework.
Requests are sent to the embedded [ActiveMQ](https://activemq.apache.org/) brokers queues.
Responses are consumed form the `response` queue.

The server is listening for HTTP requests on port `3001`.
The broker is listening for TCP connections on port `61616`.

## Building

To build the API Server `jar`, run `./gradlew build jar` from the server directory. The `jar` is then located in `./build/libs`.

## Docker
This subproject contains a `Dockerfile` for building a docker image (pass repository root as the context when building).
The Docker image exposes TCP ports `3001` and `61616`.
