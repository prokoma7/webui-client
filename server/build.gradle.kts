plugins {
    kotlin("jvm") version "1.5.31"
}

group = "org.example"
version = if (System.getenv("DOCKER_ENVIRONMENT") != null) "" else "0.2.1-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}


val ktor_version = "1.6.7"
dependencies {
    implementation(kotlin("stdlib"))
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation(group = "org.apache.activemq", name = "activemq-all", version = "5.15.2")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "16"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "16"
    }
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "MainKt"
    }

	from({
		duplicatesStrategy = DuplicatesStrategy.EXCLUDE
		configurations.runtimeClasspath.get().map({ if (it.isDirectory) it else zipTree(it) })
	})
}
