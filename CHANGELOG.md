# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2021-02-25
### Added
- server: Enable HTTP responses compression

### Fixed
- worker: Fix worker crash when processing empty request

## [0.2.0] - 2021-02-19
### Added
- Reworked to dynamically load definitions from server. This decouples WebUI from [core].
- WebUI: Show running [core] version as well as WebUI.
- WebUI: Introduce production and development environment files.

### Changed
- WebUI: Bump all dependencies.
- WebUI: Remove react-graphviz dependency in favour of directly using d3-graphviz.
- WebUI: Graphviz dialog now uses two buttons for download instead of the menu and default dimensions are a bit larger.
- deployment: Deploy on push to staging or tags.

## [0.1.0] - 2021-11-25
### Added
- Initial release.
- Bump supported [core] version to ref [9808385fd97d465959275fe007acce3c9b11e29d](https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library/commit/9808385fd97d465959275fe007acce3c9b11e29d)

[core]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/automata-library
[Unreleased]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.2.0...master
[0.2.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.fit.cvut.cz/algorithms-library-toolkit/webui-client/tags/v0.1.0
